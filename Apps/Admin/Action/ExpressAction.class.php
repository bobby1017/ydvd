<?php

namespace Admin\Action;

class ExpressAction extends BaseAction {

    /**
     * 跳到新增/编辑页面
     */
    public function toEdit() {
        $this->isLogin();
        $m = D('Admin/Express');
        $object = array();
        if (I('id', 0) > 0) {
            $this->checkPrivelege('yhgl_02');
            $object = $m->get();
        } else {
            $this->checkPrivelege('yhgl_01');
            $object = $m->getModel();
        }
        $this->assign('object', $object);
        $this->view->display('/express/edit');
    }

    /**
     * 新增/修改操作
     */
    public function edit() {
        $this->isAjaxLogin();
        $m = D('Admin/Express');
        $rs = array();
        if (I('id', 0) > 0) {
            $this->checkAjaxPrivelege('yhgl_02');
            $rs = $m->edit();
        } else {
            $this->checkAjaxPrivelege('yhgl_01');
            $rs = $m->insert();
        }
        $this->ajaxReturn($rs);
    }

    /**
     * 删除操作
     */
    public function del() {
        $this->isAjaxLogin();
        $this->checkAjaxPrivelege('yhgl_03');
        $m = D('Admin/Express');
        $rs = $m->del();
        $this->ajaxReturn($rs);
    }

    /**
     * 分页查询
     */
    public function index() {
        $this->isLogin();
        $this->checkAjaxPrivelege('yhgl_00');
        $m = D('Admin/Express');
        $page = $m->queryByPage();
        $pager = new \Think\Page($page['total'], $page['pageSize']);
        $page['pager'] = $pager->show();
        $this->assign('Page', $page);
        $this->display("/express/list");
    }

    /**
     * 列表查询
     */
    public function queryByList() {
        $this->isAjaxLogin();
        $m = D('Admin/Express');
        $list = $m->queryByList();
        $rs = array();
        $rs['status'] = 1;
        $rs['list'] = $list;
        $this->ajaxReturn($rs);
    }

}

;
?>