<?php

namespace Admin\Action;
;

/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 * 商品分类控制器
 */
class GoodsClassAction extends BaseAction {

    /**
     * 跳到新增/编辑页面
     */
    public function toEdit() {
        $this->isLogin();
        $m = D('Admin/GoodsClass');
        $object = array();
        if (I('id', 0) > 0) {
            $this->checkPrivelege('spfl_02');
            $object = $m->get(I('id', 0));
        } else {
            $this->checkPrivelege('spfl_01');
            if (I('parentId', 0) > 0) {
                $object = $m->get(I('parentId', 0));
                $object['parentId'] = $object['classId'];
                $object['className'] = '';
                $object['classSort'] = 0;
                $object['classId'] = 0;
            } else {
                $object = $m->getModel();
            }
        }
        $this->assign('object', $object);
        $this->view->display('/goodsclass/edit');
    }

    /**
     * 新增/修改操作
     */
    public function edit() {
        $this->isAjaxLogin();
        $m = D('Admin/GoodsClass');
        $rs = array();
        if (I('id', 0) > 0) {
            $this->checkAjaxPrivelege('spfl_02');
            $rs = $m->edit();
        } else {
            $this->checkAjaxPrivelege('spfl_01');
            $rs = $m->insert();
        }
        $this->ajaxReturn($rs);
    }

    /**
     * 修改名称
     */
    public function editName() {
        $this->isAjaxLogin();
        $m = D('Admin/GoodsClass');
        $rs = array('status' => -1);
        if (I('id', 0) > 0) {
            $this->checkAjaxPrivelege('spfl_02');
            $rs = $m->editName();
        }
        $this->ajaxReturn($rs);
    }

    /**
     * 删除操作
     */
    public function del() {
        $this->isAjaxLogin();
        $this->checkAjaxPrivelege('spfl_03');
        $m = D('Admin/GoodsClass');
        $rs = $m->del();
        $this->ajaxReturn($rs);
    }

    /**
     * 分页查询
     */
    public function index() {
        $this->isLogin();
        $this->checkPrivelege('spfl_00');
        $m = D('Admin/GoodsClass');
        $list = $m->getClassAndChild();
        $this->assign('List', $list);
        $this->display("/goodsclass/list");
    }

    /**
     * 列表查询
     */
    public function queryByList() {
        $this->isAjaxLogin();
        $m = D('Admin/GoodsClass');
        $list = $m->queryByList(I('id'));
        $rs = array();
        $rs['status'] = 1;
        $rs['list'] = $list;
        $this->ajaxReturn($rs);
    }

    /**
     * 显示商品是否显示/隐藏
     */
    public function editiIsShow() {
        $this->isAjaxLogin();
        $this->checkAjaxPrivelege('spfl_02');
        $m = D('Admin/GoodsClass');
        $rs = $m->editiIsShow();
        $this->ajaxReturn($rs);
    }

}

;
?>