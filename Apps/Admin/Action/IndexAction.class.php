<?php

namespace Admin\Action;

/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 * 首页（默认）控制器
 */
class IndexAction extends BaseAction {

    /**
     * 跳到商城首页
     */
    public function index() {
        $this->isLogin();
        $this->display("/index");
    }

    /**
     * 跳去后台主页面
     */
    public function toMain() {
        $this->isLogin();
        $m = D('Index');
        $weekInfo = $m->getWeekInfo(); //一周动态
        $this->assign('weekInfo', $weekInfo);
        $sumInfo = $m->getSumInfo(); //一周动态
        $this->assign('sumInfo', $sumInfo);
        $this->display("/main");
    }

    /**
     * 跳去商城配置界面
     */
    public function toMallConfig() {
        $this->isLogin();
        $this->checkPrivelege('scxx_00');
        $m = D('Admin/Index');
        $this->assign('configs', $m->loadConfigsForParent());
        
        //获取地区信息
        $m = D('Admin/Areas');
        $this->assign('areaList', $m->queryShowByList(0));
        $areaId2 = intval($GLOBALS['CONFIG']['defaultCity']) > 0 ? $GLOBALS['CONFIG']['defaultCity'] : (int) C('DEFAULT_CITY');
        if ($areaId2 > 0) {
            $area = $m->get($areaId2);
            $this->assign('areaId1', $area['parentId']);
        }
        $this->display("/mall_config");
    }

    /**
     * 保存商城配置信息
     */
    public function saveMallConfig() {
        $this->isAjaxLogin();
        $this->checkAjaxPrivelege('scxx_02');
        $m = D('Admin/Index');
        $rs = $m->saveConfigsForCode();
        $this->ajaxReturn($rs);
    }

    /**
     * 跳去登录页面
     */
    public function toLogin() {
        $this->display("/login");
    }

    /**
     * 职员登录
     */
    public function login() {
        $m = D('Admin/Staffs');
        $rs = $m->login();
        if ($rs['status'] == 1) {
            session('MXC_STAFF', $rs['staff']);
            unset($rs['staff']);
        }
        $this->ajaxReturn($rs);
    }

    /**
     * 离开系统
     */
    public function logout() {
        session('MXC_STAFF', null);
        $this->redirect("Index/toLogin");
    }

    /**
     * 获取定时任务
     */
    public function getTask() {
        $this->isAjaxLogin();
        //获取待审核商品
        $m = D('Admin/Goods');
        $grs = $m->queryPenddingGoodsNum();
        //获取待审核店铺
        $m = D('Admin/Shops');
        $srs = $m->queryPenddingShopsNum();
        $crs = M('user_cash')->where('status = 0')->getField('count(*)');
        $rd = array('status' => 1);
        $rd['goodsNum'] = $grs['num'];
        $rd['shopsNum'] = $srs['num'];
        $rd['cashNum'] = $crs;
        $this->ajaxReturn($rd);
    }

    /**
     * 清除缓存
     */
    public function cleanAllCache() {
        $rs = $this->isLogin();
        $rv = array('status' => -1);
        $rv['status'] = MXCDelDir(C('MXC_RUNTIME_PATH'));
        $this->ajaxReturn($rv);
    }

    public function upSoft() {
        $rs = $this->isLogin();

        $m = M('upsoft');
        if (IS_POST) {
            set_time_limit(300);
            $upfile = $this->uploadSoft('downloadUrl');
            if(!empty($upfile)) {
                $data['downloadUrl'] = C('WEB_URL') . '/' . $upfile;
            }
            $data['version'] = I('version', '');
            $data['upUrl'] = I('upUrl', '');
            $data['content'] = I('content', '');
            $data['createTime'] = date('Y-m-d H:i:s');

            $rs = $m->where('id=1')->save($data);
//            dump($data);
//            exit;
            if ($rs !== FALSE) {
                $this->success('Success!');
            } else {
                $this->error('Error!');
            }
        } else {
            $data = $m->where('id=1')->find();
            $this->assign('data', $data);
            $this->display("/shops/up_soft");
        }
    }

    public function tsecho() {
        $thumb = array();
        $thumb['picPath'] = 'picPath';
        $thumb['picThumbPath'] = 'picThumbPath';
        echo json_encode($thumb);
    }

}
