<?php

namespace Admin\Action;
/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 * 订单控制器
 */
class OrdersAction extends BaseAction {

    /**
     * 分页查询
     */
    public function index() {
        $this->isLogin();
        $this->checkAjaxPrivelege('ddlb_00');
        //获取地区信息
        $m = D('Admin/Areas');
        $this->assign('areaList', $m->queryShowByList(0));

        $m = D('Admin/Orders');
        $page = $m->queryByPage();
        $pager = new \Think\Page($page['total'], $page['pageSize']);
        $page['pager'] = $pager->show();
        $this->assign('Page', $page);
        $this->assign('shopName', I('shopName'));
        $this->assign('orderNo', I('orderNo'));
        $this->assign('areaId1', I('areaId1', 0));
        $this->assign('areaId2', I('areaId2', 0));
        $this->assign('areaId3', I('areaId3', 0));
        $this->assign('buyway', I('buyway', 0));
        $this->assign('orderStatus', I('orderStatus', -9999));
        $this->display("/orders/list");
    }

    /**
     *  售后服务列表
     */
    public function queryServerPage() {
        $this->isLogin();
        $this->checkAjaxPrivelege('ddlb_00');
        //获取地区信息
        $m = D('Admin/Areas');
        $this->assign('areaList', $m->queryShowByList(0));

        $m = D('Admin/Orders');
        $page = $m->queryServerPage();
        $pager = new \Think\Page($page['total'], $page['pageSize']);
        $page['pager'] = $pager->show();
        $this->assign('Page', $page);
        $this->assign('shopName', I('shopName'));
        $this->assign('orderNo', I('orderNo'));
        $this->assign('areaId1', I('areaId1', 0));
        $this->assign('areaId2', I('areaId2', 0));
        $this->assign('areaId3', I('areaId3', 0));
        $this->assign('orderStatus', I('orderStatus', -9999));
        $this->display("/orders/list_server");
    }

    /**
     * 退款分页查询
     */
    public function queryRefundByPage() {
        $this->isLogin();
        $this->checkAjaxPrivelege('ddlb_00');
        //获取地区信息
        $m = D('Admin/Areas');
        $this->assign('areaList', $m->queryShowByList(0));

        $m = D('Admin/Orders');
        $page = $m->queryRefundByPage();
        $pager = new \Think\Page($page['total'], $page['pageSize']);
        $page['pager'] = $pager->show();
        $this->assign('Page', $page);
        $this->assign('shopName', I('shopName'));
        $this->assign('orderNo', I('orderNo'));
        $this->assign('areaId1', I('areaId1', 0));
        $this->assign('areaId2', I('areaId2', 0));
        $this->assign('areaId3', I('areaId3', 0));
        $this->assign('orderStatus', I('orderStatus', -9999));
        $this->display("/orders/list_refund");
    }

    /**
     * 查看订单详情
     */
    public function toView() {
        $this->isLogin();
        $this->checkPrivelege('ddlb_00');
        $m = D('Admin/Orders');
        if (I('id') > 0) {
            $object = $m->getDetail();
            $this->assign('object', $object);
        }
        $express = M('express')->select();
        $this->assign('express', $express);
        $this->assign('referer', $_SERVER['HTTP_REFERER']);
        $this->display("/orders/view");
    }

    /**
     * 查看订单详情
     */
    public function toServer() {
        $this->isLogin();
        $this->checkPrivelege('ddlb_00');
        $m = D('Admin/Orders');
        if (I('id') > 0) {
            $object = $m->getDetail();
            $this->assign('object', $object);
        }
        $serverInfo = M('order_refund')->where('orderId = ' . I('id', 0))->order('id desc')->find();
        $shopId = M('orders')->where('orderNo = ' . $object['orderNo'] . ' and isSource = 1')->getField('shopId');
        $shopInfo = M('shops')->where('shopId = ' . $shopId)->find();

        if (!empty($serverInfo))
            $serverInfo['pics'] = json_decode(htmlspecialchars_decode($serverInfo['pics']), true);

        $express = M('express')->select();
        $this->assign('express', $express);
        $this->assign('referer', $_SERVER['HTTP_REFERER']);
        $this->assign('serverInfo', $serverInfo);
        $this->assign('shopInfo', $shopInfo);
        $this->display("/orders/view_server");
    }

    /**
     * 查看订单详情
     */
    public function toRefundView() {
        $this->isLogin();
        $this->checkPrivelege('tk_00');
        $m = D('Admin/Orders');
        if (I('id') > 0) {
            $object = $m->getDetail();
            $this->assign('object', $object);
        }
        $this->assign('referer', $_SERVER['HTTP_REFERER']);
        $this->display("/orders/view");
    }

    /**
     * 跳到退款页面
     */
    public function toRefund() {
        $this->isLogin();
        $this->checkPrivelege('ddlb_00');
        $m = D('Admin/Orders');
        if (I('id') > 0) {
            $object = $m->getDetail();
            $this->assign('object', $object);
        }
        $express = M('express')->select();
        $this->assign('express', $express);
        $this->assign('referer', $_SERVER['HTTP_REFERER']);
        $this->display("/orders/view_refund");
    }

    /**
     * 退款
     */
    public function refund() {
        $this->isLogin();
        $this->checkAjaxPrivelege('tk_04');
        $m = D('Admin/Orders');
        $rs = $m->refund();
        $this->ajaxReturn($rs);
    }

    /**
     * 平台发货填写单号
     * @return type
     */
    public function deliverGoods() {
        $s = session('MXC_STAFF');
        $d = D('Admin/Orders');
        $m = M('orders');
        $orderId = I('orderId', 0);
        $expressId = I('expressId', 0);
        $expressNo = I('expressNo', '');
        $orderStatus = I('orderStatus', 0);
        $remark = I('remark', '');

        if ($orderId == 0 || $expressId == 0 || $expressNo == '')
            $this->ajaxReturn(array('code' => 0));

        if ($orderStatus == 0) {
            $m->orderStatus = 1;
        }

        $m->expressId = $expressId;
        $m->expressNo = $expressNo;
        $rs = $m->where('orderId=' . $orderId)->save();
        $d->log_orders($orderId, $s['staffId'], $remark);


        $this->ajaxReturn(array('code' => 1));
    }

    /**
     * 平台售后处理
     * @return type
     */
    /*public function doServer() {
        $s = session('MXC_STAFF');
        $d = D('Admin/Orders');
        $m = M('orders');
		$u = M('users');
        $orderId = I('orderId', 0);
        $orderStatus = I('orderStatus', 0);
        $orderNo = I('orderNo', '');
        $remark = I('remark', '');
        $refundtime = time();
		$requireTime = date("Y:m:d H:i:s",$refundtime);
        if ($orderId == 0 || $orderStatus == 0 || $remark == '')
            $this->ajaxReturn(array('code' => 0));

        $m->orderStatus = $orderStatus;
		$m->requireTime =$requireTime;
        $rs = $m->where('orderId=' . $orderId)->save();
        $d->log_orders($orderId, $s['staffId'], $remark);
		$orderInfo = $d->where('orderId =' .$orderId)->find();
		$userId =$orderInfo['userId'] ;
		$new_Freeze = $orderInfo['totalMoney'];
		$u_freeze = $u->where('userId=' . $userId)->field('userFreeze')->find();
		$old_Freeze = $u_freeze['userFreeze'];
		$u->userFreeze = $new_Freeze +$old_Freeze;
		$us = $u->where('userId=' . $userId)->save();

        $this->ajaxReturn(array('code' => 1));
    }*/
	public function doServer() {
        $s = session('MXC_STAFF');
        $d = D('Admin/Orders');
        $m = M('orders');
        $orderId = I('orderId', 0);
        $orderStatus = I('orderStatus', 0);
        $remark = I('remark', '');

        if ($orderId == 0 || $orderStatus == 0 || $remark == '')
            $this->ajaxReturn(array('code' => 0));

        $m->orderStatus = $orderStatus;
        $rs = $m->where('orderId=' . $orderId)->save();
        if(!empty($orderNo) && $orderStatus == -3) {
            M('log_user_profit')->where('orderNo = "' . $orderNo . '"')->save(array('orderStatus' => -3));
        }
        $d->log_orders($orderId, $s['staffId'], $remark);


        $this->ajaxReturn(array('code' => 1));
    }

    /**
     * 平台退款处理
     * @return type
     */
    public function doRufund() {
        $s = session('MXC_STAFF');
        $d = D('Admin/Orders');
        $m = M('orders');
        $orderId = I('orderId', 0);
        $remark = I('remark', '');
        $orderInfo = $m->where('orderId = ' . $orderId)->find();
        if ($orderId == 0 || $remark == '' || $orderInfo['orderStatus'] != 4)
            $this->ajaxReturn(array('code' => 0));

        $m->orderStatus = 5;

        $rs = $m->where('orderId=' . $orderId)->save();
        $d->log_orders($orderId, $s['staffId'], $remark);
        $totalPrice = $orderInfo['totalMoney'] + $orderInfo['postage'] + $orderInfo['supPrice'] + $orderInfo['supPostage'];
        M('users')->where('userId = ' . $orderInfo['userId'])->setInc('userBalance', $totalPrice);
        $d->MlogBalance($orderInfo['userId'], $totalPrice, 4, $logContent = '售后退款');

        $this->ajaxReturn(array('code' => 1));
    }

}
;
?>