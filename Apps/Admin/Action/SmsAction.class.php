<?php

namespace Admin\Action;

/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 * 银行控制器
 */
class SmsAction extends BaseAction {

    /**
     * 跳到编辑页面
     */
    public function toEdit() {
//        $this->isLogin();
//        $this->checkPrivelege('smsgl_01');
        $m = D('Admin/Sms');
        $object = array();
        $object = $m->getConfig();

        $this->assign('object', $object);
        $this->view->display('/sms/sms');
    }

    /**
     * 新增/修改操作
     */
    public function edit() {
        $this->isAjaxLogin();
        $m = D('Admin/Sms');
        log_txt('asdsf', 'tx.txt');
        $rs = array();
//        $this->checkPrivelege('zfgl_02');
        $rs = $m->edit();
        
        $this->ajaxReturn($rs);
    }

}
