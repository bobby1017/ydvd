<?php

namespace Admin\Action;
/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 * 会员控制器
 */
class UsersAction extends BaseAction {

    /**
     * 跳到新增/编辑页面
     */
    public function toEdit() {
        $this->isLogin();
        $m = D('Admin/Users');
        $object = array();
        if (I('id', 0) > 0) {
            $this->checkPrivelege('hylb_02');
            $object = $m->get();
            $depositMoney = 0;
            $profit_sum = 0;
            $shop = M('shops')->where('userId=' . $object['userId'])->find();
            if (!empty($shop)) {
                $object['userFreezeAll'] = $object['userFreeze'] + $shop['depositMoney']; //余额 userFreeze + 保证金：{$depositMoney}  + 冻结利润：{$profit_sum}
                $depositMoney = $shop['depositMoney'];
                $orderIds = M('orders')->where('shopId = ' . $shop['shopId'] . ' and orderFlag = 1 and orderStatus in (-3,0,1,2,3)')->select();
                $ids = '';
//echo "<pre>";                
//var_dump($orderIds);                
                if(!empty($orderIds)) {
                    foreach($orderIds as $v) {
                        $ids .= $v['orderNo'] . ',';
                    }
                }
                $ids = '(' . trim($ids, ',') . ')';
                $profit_sum = M('log_user_profit')->where('profitStatus = 0 and shopId = ' . $shop['shopId'] . ' and orderNo in ' . $ids)->getField("sum(`profit`)");
            }
//var_dump(M('log_user_profit')->getLastSql());     die;
            $this->assign('depositMoney', $depositMoney);
            $this->assign('profit_sum', $profit_sum);
            $this->assign('object', $object);
            $this->view->display('users/edit');
        } else {
            $this->checkPrivelege('hylb_01');
            $object = $m->getModel();
            $object['userStatus'] = 1;

            $this->assign('object', $object);
            $this->view->display('/users/add');
        }
    }

    /**
     * 新增/修改操作
     */
    public function edit() {
        $this->isAjaxLogin();
        $m = D('Admin/Users');
        $rs = array();
        if (I('id', 0) > 0) {
            $this->checkAjaxPrivelege('hylb_02');
            $rs = $m->edit();
        } else {
            $this->checkAjaxPrivelege('hylb_01');
            $rs = $m->insert();
        }
        $this->ajaxReturn($rs);
    }

    /**
     * 删除操作
     */
    public function del() {
        $this->isAjaxLogin();
        $this->checkAjaxPrivelege('hylb_03');
        $m = D('Admin/Users');
        $rs = $m->del();
        $this->ajaxReturn($rs);
    }

    /**
     * 查看
     */
    public function toView() {
        $this->isLogin();
        $this->checkPrivelege('hylb_00');
        $m = D('Admin/Users');
        if (I('id') > 0) {
            $object = $m->get();
            $this->assign('object', $object);
        }
        $this->view->display('/users/view');
    }

    /**
     * 分页查询
     */
    public function index() {
        $this->isLogin();
        $this->checkPrivelege('hylb_00');
        $m = D('Admin/Users');
        $page = $m->queryByPage();
        $pager = new \Think\Page($page['total'], $page['pageSize']);
        $page['pager'] = $pager->show();
        $this->assign('loginName', I('loginName'));
        $this->assign('userPhone', I('userPhone'));
        $this->assign('userEmail', I('userEmail'));
        $this->assign('userType', I('userType', -1));
        $this->assign('Page', $page);
        $this->display("/users/list");
    }

    /**
     * 列表查询
     */
    public function queryByList() {
        $this->isAjaxLogin();
        $m = D('Admin/Users');
        $list = $m->queryByList();
        $rs = array();
        $rs['status'] = 1;
        $rs['list'] = $list;
        $this->ajaxReturn($rs);
    }

    /**
     * 查询用户账号
     */
    public function checkLoginKey() {
        $this->isAjaxLogin();
        $m = D('Admin/Users');
        $key = I('clientid');
        $id = I('id', 0);
        $rs = $m->checkLoginKey(I($key), $id);
        $this->ajaxReturn($rs);
    }

    /*     * ********************************************************************************************
     *                                             账号管理                                                                                                                              *
     * ******************************************************************************************** */

    /**
     * 获取账号分页列表
     */
    public function queryAccountByPage() {
        $this->isLogin();
        $this->checkPrivelege('hyzh_00');
        $m = D('Admin/Users');
        $page = $m->queryAccountByPage();
        $pager = new \Think\Page($page['total'], $page['pageSize']);
        $page['pager'] = $pager->show();
        $this->assign('loginName', I('loginName'));
        $this->assign('userStatus', I('userStatus', -1));
        $this->assign('userType', I('userType', -1));
        $this->assign('Page', $page);
        $this->display("/users/account_list");
    }

    /**
     * 编辑账号状态
     */
    public function editUserStatus() {
        $this->isAjaxLogin();
        $this->checkAjaxPrivelege('hyzh_04');
        $m = D('Admin/Users');
        $rs = $m->editUserStatus();
        $this->ajaxReturn($rs);
    }

    /**
     * 跳到账号编辑状态
     */
    public function toEditAccount() {
        $this->isLogin();
        $this->checkPrivelege('hyzh_04');
        $m = D('Admin/Users');
        $object = $m->getAccountById();
        $this->assign('object', $object);
        $this->display("/users/edit_account");
    }

    /**
     * 编辑账号信息
     */
    public function editAccount() {
        $this->isAjaxLogin();
        $this->checkAjaxPrivelege('hyzh_04');
        $m = D('Admin/Users');
        $rs = $m->editAccount();
        $this->ajaxReturn($rs);
    }

    /**
     * 消息推送
     */
    public function sendSysMsg() {
        $this->isLogin();
//        $this->checkPrivelege('hyzh_04');  //13611112220 Shunqi Lam-百分珠宝玉器  userId=2521
        if (IS_POST) {
            $m = D('Api/Public');
            $userIds = I('userIds');
            $msg = I('msg');
            if (I('isAll', 0) == 0) {
                if ($userIds != '' && $msg != '') {
                    $userIds = explode(',', $userIds);
                    $rs = $m->sendSysMsg($userIds, $msg);
                    if ($rs == FALSE)
                        $this->error('消息推送失败');
                    $this->success('消息推送成功');
                }
                $this->error('消息推送失败');
            } else {
                if ($msg != '') {
                    $userIds = M('users')->field('userId')->select();
                    $rs = $m->sendSysMsg($userIds, $msg, 0, 1);
                    if ($rs == FALSE)
                        $this->error('消息推送失败');
                    $this->success('消息推送成功');
                }
                $this->error('消息推送失败');
            }
        } else {
            $m = D('Admin/Users');

            $user = $m->getMemList();
            $this->assign('user', $user);
            $this->assign('userName', I('userName'));
            $this->assign('userPhone', I('userPhone'));
            $this->assign('userType', I('userType', -1));
            $this->display("/users/send_sys_msg");
        }
    }

    /**
     * 分页查询
     */
    public function feedback() {
        $this->isLogin();
        $this->checkPrivelege('hylb_00');
        $m = D('Admin/Users');
        $page = $m->feedback();
        $pager = new \Think\Page($page['total'], $page['pageSize']);
        $page['pager'] = $pager->show();
        $this->assign('content', I('content'));
        $this->assign('phone', I('phone'));
        $this->assign('Page', $page);
        $this->display("/users/feedback");
    }

    /**
     * 保证金支付/充值记录
     */
    public function userBankroll() {
        $this->isLogin();
        $m = D('Admin/Users');

        $page = $m->userBankroll();
        $pager = new \Think\Page($page['total'], $page['pageSize']);
        $page['pager'] = $pager->show();

        $this->assign('userPhone', I('userPhone'));
        $this->assign('Page', $page);
        $this->display("/users/userBankroll");
    }

    /**
     * 提现申请列表
     */
    public function cashList() {
        $this->isLogin();
        $m = D('Admin/Users');

        $page = $m->cashList();
        $pager = new \Think\Page($page['total'], $page['pageSize']);
        $page['pager'] = $pager->show();

        $this->assign('userPhone', I('userPhone'));
        $this->assign('status', I('status'));
        $this->assign('Page', $page);
        $this->display("/users/cashList");
    }

    /**
     * 受理提现
     */
    public function doCash() {
        if (IS_POST) {
            $m = D('Admin/Users');
            $rs = $m->doCash();
            if ($rs) {
                $this->success('Success!', 'cashList');
            } else {
                $this->error('Error!');
            }
        } else {
            $m = M('user_cash');
            $cashId = I('cashId');
            $cash = $m->where('cashId=' . $cashId)->find();
            $this->assign('cash', $cash);
            $this->display('/users/doCash');
        }
    }

    public function  rePwd() {
        $userId = I('userId','');
        if ($userId==0) {
            die("error");
        }
        $m = M('users');   
        $res = $m->where(array('userId'=>$userId))->find();
        if (false==$res) {
             die("error");
        }
        $createLoginPwd = $this->randpw(6,'ALL');
        $createLoginPwd = strtolower($createLoginPwd);
        //检测账号，邮箱，手机是否存在
        $data["loginSecret"] = rand(1000, 9999);
        $data['loginPwd'] = md5($createLoginPwd. $data['loginSecret']);
     
        $m->where(array('userId'=>$userId))->save($data);
 
        die($createLoginPwd);

    }

    public function testsendSysMsg() {
        //$m = D('Api/Public');  //  13611112220
        //$rs = $m->sendSysMsg(2521, "test推送16:10");

        $push = D('Api/Push');
        $pushRs = $push->PushToken("test推送".date("Y-m-d H:i:s"),2521); 


          
        var_dump($pushRs);       
    }

}
?>