<?php

namespace Admin\Model;

/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 * 基础服务类
 */
use Think\Model;

class BaseModel extends Model {

    /**
     * 用来处理内容中为空的判断
     */
    public function checkEmpty($data, $isDie = false) {
        foreach ($data as $key => $v) {
            if (trim($v) == '') {
                if ($isDie)
                    die("{'status':-1,'key':'$key'}");
                return false;
            }
        }
        return true;
    }

    /**
     * 输入sql调试信息
     */
    public function logSql($m) {
        echo $m->getLastSql();
    }

    /**
     * 获取一行记录
     */
    public function queryRow($sql) {
        $plist = $this->query($sql);
        return empty($plist) ? array() : $plist[0];
    }

    /**
     * 余额日志
     */
    public function MlogBalance($userId, $price, $logType, $logContent = '') {
        $m = M('log_user_balance');
        $typeName = array(1 => '订单支付', 2 => '充值', 3 => '提现', 4 => '退款', 5 => '转售收益', 6 => '销售收益', 8 => '商家退款', 9 => '退回保证金');
        $data = array();
        $data['userId'] = $userId;
        $data['logContent'] = $logContent;
        $data['price'] = $price;
        $data['logType'] = $logType;
        $data['typeName'] = $typeName[$logType];
        $data['logTime'] = date('Y-m-d H:i:s');
        $rs = $m->add($data);
        return $rs;
    }

    /**
     * 订单日志
     */
    public function log_orders($orderId, $logUserId, $logContent = '') {
        $m = M('log_orders');
        $data = array();
        $data['orderId'] = $orderId;
        $data['logUserId'] = $logUserId;
        $data['logContent'] = $logContent;
        $data['logTime'] = date('Y-m-d H:i:s');
        $rs = $m->add($data);

        return $rs;
    }

    /**
     * 获取店铺所有相关产品
     * @param type $shopId
     * @return type
     */
    public function stopGoodsByShop($shopId) {
        $goods = M('goods')->where('shopId=' . $shopId)->select();
        $sourceGoods = array();
        $resaleGoods = array();

        foreach ($goods as $v) {
            if ($v['isSource'] == 1) {
                $sourceGoods[] = $v['goodsId'];
            } else {
                $resaleGoods[] = $v['goodsId'];
            }
        }

        $reToMygoods = array();
        $retemp = M('goods_resale')->where('sourceId in (' . implode(',', $sourceGoods) . ')')->select();
        if (!empty($retemp)) {
            foreach ($retemp as $v) {
                $reToMygoods[] = $v['goodsId'];
            }
        }

        $allGoods = implode(',', array_merge($sourceGoods, $resaleGoods, $reToMygoods));
        if (!empty($allGoods)) {
            $rs = M('goods')->where('goodsId in (' . $allGoods . ')')->save(array('goodsStatus' => 0));

            $data = array();
            $data['shopId'] = $shopId;
            $data['sourceGoods'] = implode(',', $sourceGoods);
            $data['resaleGoods'] = implode(',', $resaleGoods);
            $data['reToMygoods'] = implode(',', $reToMygoods);
            $rs = M('log_stop_shop_goods')->add($data);

            return $data;
        }
        return;
    }

}

;
?>