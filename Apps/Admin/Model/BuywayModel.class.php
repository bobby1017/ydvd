<?php

namespace Admin\Model;

/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 * 购买方式
 */
class BuywayModel extends BaseModel {

    /**
     * 新增
     */
    public function add() {
        $rd = array('status' => -1);
        $data = array();
        $data["buyCode"] = I("buyCode");
        $data["buyName"] = I("buyName");
        $data["buyDesc"] = I("buyDesc");
        ;
        if ($this->checkEmpty($data, true)) {

            $data["buyOrder"] = I("buyOrder");
            $data["buyConfig"] = I("buyConfig");
            $data["enabled"] = I("enabled");

            $m = M('Buyway');
            $rs = $m->add($data);
            if (false !== $rs) {
                $rd['status'] = 1;
            }
        }
        return $rd;
    }

    /**
     * 修改
     */
    public function edit() {
        $rd = array('status' => -1);
        $id = (int) I("id", 0);

        $m = M('Buyway');
        $data["buyName"] = I("buyName");
        $data["buyDesc"] = I("buyDesc");
        $data["buyOrder"] = I("buyOrder");
        $data["buyConfig"] = json_encode(I("buyConfig"));
        $data["enabled"] = 1;
        if ($this->checkEmpty($data)) {
            $rs = $m->where("id=" . I('id'))->save($data);
            if (false !== $rs) {
                $rd['status'] = 1;
            }
        }
        return $rd;
    }

    /**
     * 获取指定对象
     */
    public function get() {
        $m = M('Buyway');
        $buyment = $m->where("id=" . I('id'))->find();
        $buyConfig = json_decode($buyment["buyConfig"]);
        foreach ($buyConfig as $key => $value) {
            $buyment[$key] = $value;
        }
        return $buyment;
    }

    /**
     * 分页列表
     */
    public function queryByPage() {
        $m = M('Buyway');
        $sql = "select * from __PREFIX__buyway order by buyOrder asc";
        $rs = $m->pageQuery($sql);

        foreach ($rs["root"] as $key => $value) {

            $rs["root"][$key]["buyDesc"] = htmlspecialchars_decode($value["buyDesc"]);
        }
        return $rs;
    }

    /**
     * 获取列表
     */
    public function queryByList() {
        $m = M('Buyway');
        $rs = $m->select();
        return $rs;
    }

    /**
     * 删除
     */
    public function del() {
        $m = M('Buyway');
        $data["enabled"] = 0;
        $rs = $m->where("id=" . I('id'))->save($data);
        if (false !== $rs) {
            $rd['status'] = 1;
        }
        return $rd;
    }

}

;
?>