<?php

namespace Admin\Model;

class ExpressModel extends BaseModel {

    /**
     * 新增
     */
    public function insert() {
        $rd = array('status' => -1);
        $data = array();
        $data["expressName"] = I("expressName");
        $data["expressFlag"] = 1;
        if ($this->checkEmpty($data, true)) {
            $m = M('express');
            $rs = $m->add($data);
            if (false !== $rs) {
                $rd['status'] = 1;
            }
        }
        return $rd;
    }

    /**
     * 修改
     */
    public function edit() {
        $rd = array('status' => -1);
        $id = (int) I("id", 0);

        $m = M('express');
        $data["expressId"] = I("id");
        $data["expressName"] = I("expressName");
        if ($this->checkEmpty($data)) {
            $rs = $m->where("expressId=" . I('id'))->save($data);
            if (false !== $rs) {
                $rd['status'] = 1;
            }
        }
        return $rd;
    }

    /**
     * 获取指定对象
     */
    public function get() {
        $m = M('express');
        return $m->where("expressId=" . I('id'))->find();
    }

    /**
     * 分页列表
     */
    public function queryByPage() {
        $m = M('express');
        $sql = "select * from __PREFIX__express where expressFlag=1 order by expressId desc";
        $rs = $m->pageQuery($sql);
        return $rs;
    }

    /**
     * 获取列表
     */
    public function queryByList() {
        $m = M('express');
        $rs = $m->where('expressFlag=1')->select();
        return $rs;
    }

    /**
     * 删除
     */
    public function del() {
        $rd = array('status' => -1);
        $m = M('express');
        $rs = $m->delete(I('id'));
        if ($rs) {
            $rd['status'] = 1;
        }
        return $rd;
    }

}

;
?>