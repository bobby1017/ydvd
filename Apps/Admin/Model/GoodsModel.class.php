<?php

namespace Admin\Model;

/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 * 商品服务类
 */
class GoodsModel extends BaseModel {

    /**
     * 获取商品信息
     */
    public function get() {
        $m = M('goods');
        $id = I('id', 0);
        $goods = $m->where("goodsId=" . $id)->find();
        //相册
        $m = M('goods_gallerys');
        $goods['gallery'] = $m->where('goodsId=' . $id)->select();
        //款式分类
        $sql = "select c1.catName goodsName1,c2.catName goodsName2
		        from __PREFIX__goods_cats c2,__PREFIX__goods_cats c1
		        where c2.parentId=c1.catId and c2.catId=" . $goods['goodsCatId2'];
        $rs = $this->query($sql);
        $goods['goodsCats'] = $rs[0];
        //款式分类
        $sql = "select c1.className goodsName1,c2.className goodsName2
		        from __PREFIX__goods_class c2,__PREFIX__goods_class c1
		        where c2.parentId=c1.classId and c2.classId=" . $goods['goodsClassId2'];
        $rs = $this->query($sql);
        //
        $sql = "select g.goodsId,s.shopName  
                from __PREFIX__goods  g,__PREFIX__shops s
                where g.shopId=s.shopId and g.goodsId=" . $id ." limit 1" ;
        $tmpRs = $this->query($sql);     
        $goods['shopName'] = $tmpRs[0]['shopName'];
        unset($tmpRs);

        $goods['goodsClass'] = $rs[0];
        $goods['goodsContent'] = M('goods_data')->where("goodsId=" . $id)->getField('goodsContent');
        return $goods;
    }

    /**
     * 分页列表[获取待审核列表]
     */
    public function queryPenddingByPage() {
        $m = M('goods');
        $shopName = I('shopName');
        $goodsName = I('goodsName');
        $areaId1 = I('areaId1', 0);
        $areaId2 = I('areaId2', 0);
        $sql = "select g.*,p.shopName from __PREFIX__goods g 
	 	      ,__PREFIX__shops p 
	 	      where goodsStatus=0 and goodsFlag=1 and p.shopId=g.shopId and g.isSale=1 and g.isDel=0";
        if ($shopName != '')
            $sql.=" and (p.shopName like '%" . $shopName . "%' or p.shopSn like '%'" . $shopName . "%')";
        if ($goodsName != '')
            $sql.=" and (g.goodsName like '%" . $goodsName . "%' or g.goodsSn like '%" . $goodsName . "%')";
        $sql.="  order by goodsId desc";
        return $m->pageQuery($sql);
    }

    /**
     * 分页列表[获取已审核列表]
     */
    public function queryByPage() {
        $m = M('goods');
        $shopName = I('shopName');
        $goodsName = I('goodsName');
        $areaId1 = I('areaId1', 0);
        $areaId2 = I('areaId2', 0);
         
        $sql = "select g.*,p.shopName from __PREFIX__goods g 
	 	      ,__PREFIX__shops p 
	 	      where goodsStatus=1 and goodsFlag=1 and p.shopId=g.shopId  and g.isDel=0";
        if ($shopName != '')
            $sql.=" and (p.shopName like '%" . $shopName . "%' or p.shopSn like '%" . $shopName . "%')";
        if ($goodsName != '')
            $sql.=" and (g.goodsName like '%" . $goodsName . "%' or g.goodsSn like '%" . $goodsName . "%')";
        $sql.="  order by goodsId desc";
        $val =    $m->pageQuery($sql);
    
        return $m->pageQuery($sql);
    }
    
    /**
     * 分页列表[获取禁售商品列表]
     */
    public function goodsFlagList() {
        $m = M('goods');
        $shopName = I('shopName');
        $goodsName = I('goodsName');
        $areaId1 = I('areaId1', 0);
        $areaId2 = I('areaId2', 0);
        $sql = "select g.*,p.shopName from __PREFIX__goods g 
	 	      ,__PREFIX__shops p 
	 	      where goodsStatus=1 and goodsFlag=-1 and p.shopId=g.shopId and g.isSale=1 and g.isDel=0";
        if ($shopName != '')
            $sql.=" and (p.shopName like '%" . $shopName . "%' or p.shopSn like '%" . $shopName . "%')";
        if ($goodsName != '')
            $sql.=" and (g.goodsName like '%" . $goodsName . "%' or g.goodsSn like '%" . $goodsName . "%')";
        $sql.="  order by goodsId desc";
        return $m->pageQuery($sql);
    }

    /**
     * 获取列表
     */
    public function queryByList() {
        $m = M('goods');
        $sql = "select * from __PREFIX__goods order by goodsId desc";
        return $m->find($sql);
    }

    /**
     * 修改商品状态
     */
    public function changeGoodsStatus() {
        $rd = array('status' => -1);
        $m = M('goods');
        $id = (int) I('id', 0);
        $m->goodsStatus = I('status', 0);
        $rs = $m->where('goodsId=' . $id)->save();
        if (false !== $rs) {
            $sql = "select goodsName,userId from __PREFIX__goods g,__PREFIX__shops s where g.shopId=s.shopId and g.goodsId=" . $id;
            $goods = $this->query($sql);
            $msg = "";
            if (I('status', 0) == 0) {
                $msg = "商品[" . $goods[0]['goodsName'] . "]已被商城下架";
            } else {
                $msg = "商品[" . $goods[0]['goodsName'] . "]已通过审核";
            }
            $yj_data = array(
                'msgType' => 0,
                'sendUserId' => session('MXC_STAFF.staffId'),
                'receiveUserId' => $goods[0]['userId'],
                'msgContent' => $msg,
                'createTime' => date('Y-m-d H:i:s'),
                'msgStatus' => 0,
                'msgFlag' => 1,
            );
            M('messages')->add($yj_data);
            $rd['status'] = 1;
        }
        return $rd;
    }

    /**
     * 获取待审核的商品数量
     */
    public function queryPenddingGoodsNum() {
        $rd = array('status' => -1);
        $m = M('goods');
        $sql = "select count(*) counts from __PREFIX__goods where goodsStatus=0 and goodsFlag=1";
        $rs = $this->query($sql);
        $rd['num'] = $rs[0]['counts'];
        return $rd;
    }

    /**
     * 批量修改精品状态
     */
    public function changeBestStatus() {
        $rd = array('status' => -1);
        $m = M('goods');
        $id = I('id', 0);
        $m->isAdminBest = I('status', 0);
        $rs = $m->where('goodsId in(' . $id . ")")->save();
        if (false !== $rs) {
            $rd['status'] = 1;
        }
        return $rd;
    }

    /**
     * 批量修改推荐状态
     */
    public function changeRecomStatus() {
        $rd = array('status' => -1);
        $m = M('goods');
        $id = I('id', 0);
        $m->isAdminRecom = I('status', 0);
        $rs = $m->where('goodsId in(' . $id . ")")->save();
        if (false !== $rs) {
            $rd['status'] = 1;
        }
        return $rd;
    }


    /**
     * 获取款式两层ID
     * @param type $catId
     * @return type
     */
    public function getCatData($catId) {
        $m = M('goods_cats');
        $cat = $m->where('catId=' . $catId)->field('catId, parentId')->find();
        if ($cat['parentId'] == 0) {
            return array('goodsCatId1' => $catId, 'goodsCatId2' => 0);
        }
        return array('goodsCatId1' => $cat['parentId'], 'goodsCatId2' => $catId);
    }

    /**
     * 获取种质两层ID
     * @param type $classId
     * @return type
     */
    public function getClassData($classId) {
        $m = M('goods_class');
        $class = $m->where('classId=' . $classId)->field('classId, parentId')->find();
        if ($class['parentId'] == 0) {
            return array('goodsClassId1' => $classId, 'goodsClassId2' => 0);
        }
        return array('goodsClassId1' => $class['parentId'], 'goodsClassId2' => $classId);
    }

    /**
     * 后台新增商品
     */
    public function toAddGoods($shopId) {
        $data = array();
        $data['shopId'] = $shopId;
        $data['goodsName'] = I('goodsName');
        $data['marketPrice'] = I('marketPrice', 0);    //售价
        $data['shopPrice'] = I('shopPrice', 0);    //合作价
        $data['goodsStock'] = I('goodsStock', 1);
        $cat = $this->getCatData(I('catId'));
        $data['goodsCatId1'] = $cat['goodsCatId1'];
        $data['goodsCatId2'] = $cat['goodsCatId2'];
        $class = $this->getClassData(I('classId'));
        $data['goodsClassId1'] = $class['goodsClassId1'];
        $data['goodsClassId2'] = $class['goodsClassId2'];
        $data['isOffline'] = I('isOffline', 0);
        $data['createTime'] = date('Y-m-d H:i:s');
        $isGoodsVerify = M('sys_configs')->where('fieldCode = "isGoodsVerify"')->getField('fieldValue');
        if ($isGoodsVerify == 0) {
            $data['goodsStatus'] = 1;
        }
        $pics = json_decode(htmlspecialchars_decode(I('pics')), TRUE);
        $thumb = json_decode(htmlspecialchars_decode(I('thumb', '')), TRUE);
//        file_put_contents('ts.txt', print_r($thumb, TRUE));
        if ($thumb) {
            $data['goodsImg'] = $thumb['picPath'];
            $data['goodsThumbs'] = $thumb['picThumbPath'];
        } else {
            if (!empty($pics)) {
                $data['goodsImg'] = $pics[0]['picPath'];
                $data['goodsThumbs'] = $pics[0]['picThumbPath'];
            }
        }

//        show_pre_data($data, 1);
        foreach ($data as $v) {
            if ($v === '') {
                $rt['code'] = 0;
                $rt['info'] = '请将信息填写完整!';
                return $rt;
            }
        }
        $data['goodsSize'] = I('goodsSize');
        $data['goodsWeight'] = I('goodsWeight');
        $data['goodsVedio'] = I('goodsVedio', '');
        $data['vedioThumb'] = I('vedioThumb', '');
        $m = M('goods');
//        file_put_contents('tst.txt', print_r($data, TRUE));
        $goodsId = $m->add($data);
//        file_put_contents('tss.txt', print_r($this->getLastSql(), TRUE));
        if ($goodsId === FALSE) {
            $rt['code'] = 0;
            $rt['info'] = 'Error!';
            return $rt;
        }

        if (!empty($pics)) {
            $dataList = array();
            foreach ($pics as $v) {
                $data = array();
                $data['goodsId'] = $goodsId;
                $data['shopId'] = $shopId;
                $data['goodsImg'] = $v['picPath'];
                $data['goodsThumbs'] = $v['picThumbPath'];
                $dataList[] = $data;
            }
            $m = M('goods_gallerys');
            $rs = $m->addAll($dataList);
        }


        $data = array();
        $data['goodsId'] = $goodsId;
        $data['shopId'] = $shopId;
        $data['goodsContent'] = I('content');
        $m = M('goods_data');
        $rs = $m->add($data);

        return $rs;

    }

}

?>