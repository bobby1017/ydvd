<?php

namespace Admin\Model;

/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 * 社区服务类
 */
class LinestoreModel extends BaseModel {

    /**
     * 新增
     */
    public function insert() {
        $rd = array('status' => -1);
        $id = I("id", 0);
        $data = array();
        $data["areaId1"] = I("areaId1");
        $data["areaId2"] = I("areaId2");
        $data["areaId3"] = I("areaId3");
        $data["province"] = I("province");
        $data["city"] = I("city");
        $data["district"] = I("district");
        $data["storeTel"] = I("storeTel");
        $data["storeName"] = I("storeName");
        $data["storeAddress"] = I("storeAddress");
        $data["storeSort"] = I("storeSort", 0);
        $data["storeFlag"] = 1;
        if ($this->checkEmpty($data)) {
            $data["storeKey"] = I("storeKey");
            $m = M('linestore');
            $rs = $m->add($data);
            if (false !== $rs) {
                $rd['status'] = 1;
            }
        }
        return $rd;
    }

    /**
     * 修改
     */
    public function edit() {
        $rd = array('status' => -1);
        $id = I("id", 0);
        $data = array();
        $data["areaId1"] = I("areaId1");
        $data["areaId2"] = I("areaId2");
        $data["areaId3"] = I("areaId3");
        $data["province"] = I("province");
        $data["city"] = I("city");
        $data["district"] = I("district");
        $data["storeTel"] = I("storeTel");
        $data["storeName"] = I("storeName");
        $data["storeAddress"] = I("storeAddress");
        $data["storeSort"] = I("storeSort", 0);
        if ($this->checkEmpty($data)) {
            $data["storeKey"] = I("storeKey");
            $m = M('linestore');
            $rs = $m->where("storeId=" . I('id', 0))->save($data);
            if (false !== $rs) {
                $rd['status'] = 1;
            }
        }
        return $rd;
    }

    /**
     * 获取指定对象
     */
    public function get() {
        $m = M('linestore');
        return $m->where("storeId=" . I('id'))->find();
    }

    /**
     * 分页列表
     */
    public function queryByPage() {
        $m = M('linestore');
        $areaId1 = I('areaId1', 0);
        $areaId2 = I('areaId2', 0);
        $areaId3 = I('areaId3', 0);
        $sql = "select c.*,a1.areaName areaName1,a2.areaName areaName2,a3.areaName areaName3 
	 	        from __PREFIX__linestore c ,__PREFIX__areas a1 ,__PREFIX__areas a2,__PREFIX__areas a3
	 	        where a1.areaId=c.areaId1 and a2.areaId=c.areaId2 and a3.areaId=c.areaId3 and storeFlag=1";
        if ($areaId1 > 0)
            $sql.=" and c.areaId1=" . $areaId1;
        if ($areaId2 > 0)
            $sql.=" and c.areaId2=" . $areaId2;
        if ($areaId3 > 0)
            $sql.=" and c.areaId3=" . $areaId3;
        $sql.=" order by storeId desc";
        return $m->pageQuery($sql);
    }

    /**
     * 获取列表
     */
    public function queryByList() {
        $m = M('linestore');
        $sql = "select * from __PREFIX__linestore order by storeId desc";
        return $m->select($sql);
    }

    /**
     * 删除
     */
    public function del() {
        $rd = array('status' => -1);
        $m = M('linestore');
        $data = array();
        $data["storeFlag"] = -1;
        $rs = $m->where("storeId=" . I('id'))->save($data);
        if (false !== $rs) {
            $rd['status'] = 1;
        }
        return $rd;
    }

    /**
     * 显示分类是否显示/隐藏
     */
    public function editiIsShow() {
        $rd = array('status' => -1);
        if (I('id', 0) == 0)
            return $rd;
        $m = M('linestore');
        $m->isShow = (I('isShow') == 1) ? 1 : 0;
        $rs = $m->where("storeId=" . I('id', 0))->save();
        if (false !== $rs) {
            $rd['status'] = 1;
        }
        return $rd;
    }

}

;
?>