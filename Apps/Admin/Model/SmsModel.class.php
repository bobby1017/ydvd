<?php

namespace Admin\Model;

/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 * 支付类
 */
class SmsModel extends BaseModel {

    /**
     * 修改
     */
    public function edit() {
        $rd = array('status' => -1);

        $m = M('sms_config');
        $data["accountSid"] = I("accountSid");
        $data["accountToken"] = I("accountToken");
        $data["appId"] = I("appId");
        $data["codeTempId"] = I("codeTempId");
        $data["reodfinishTempId"] = I("reodfinishTempId");
        $data["resaleTempId"] = I("resaleTempId");
        $data["odfinishTempId"] = I("odfinishTempId");
        $data["userSignTempId"] = I("userSignTempId");
        $data["saleTempId"] = I("saleTempId");
        if ($this->checkEmpty($data)) {
            $rs = $m->where(array('tag' => 'sms_code_config'))->save($data);
            if (false !== $rs) {
                $rd['status'] = 1;
            }
        }
        return $rd;
    }

    /**
     * 获取指定对象
     */
    public function getConfig() {
        $m = M('sms_config');
        $smsConfig = $m->where(array('tag' => 'sms_code_config'))->find();

        return $smsConfig;
    }

}
