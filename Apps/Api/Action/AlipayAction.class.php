<?php

namespace Api\Action;

class AlipayAction extends BaseAction {

    public function notify() {
        $alipay_config = C("alipay_config");
        Vendor('Alipay.lib.alipay_notify');
        log_txt($alipay_config, 'log/log_alipay/config-' . date('His') . '.txt');
        $alipayNotify = new \AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyNotify();
        log_txt($verify_result, 'log/log_alipay/notify-' . date('His') . '.txt');
        if ($verify_result) {
            $out_trade_no = $_POST['out_trade_no'];
            //支付宝交易号
            $trade_no = $_POST['trade_no'];
            //交易状态
            $trade_status = $_POST['trade_status'];

            if ($_POST['trade_status'] == 'TRADE_FINISHED') {
                log_txt($_POST, 'log/log_alipay/TRADE_FINISHED-' . $out_trade_no . '.txt');
                //商户订单号
                $obj = array();
                $obj["out_trade_no"] = $_POST['out_trade_no'];
                $obj["total_fee"] = $_POST['total_fee'];

                //支付成功业务逻辑
                $length = strlen($obj["out_trade_no"]);
                if ($length == 8) {
                    $pay = D('Api/pay');
                    $rs = $pay->cashDepositPay($obj);
                } else {
                    $od = D('Api/Order');
                    $rs = $od->complatePay($obj);
                }
            } else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
                log_txt($_POST, 'log/log_alipay/TRADE_SUCCESS-' . $out_trade_no . '.txt');
                //商户订单号
                $obj = array();
                $obj["out_trade_no"] = $_POST['out_trade_no'];
                $obj["total_fee"] = $_POST['total_fee'];

                //支付成功业务逻辑
                $length = strlen($obj["out_trade_no"]);
                if ($length == 8) {
                    $pay = D('Api/pay');
                    $rs = $pay->cashDepositPay($obj);
                } else {
                    $od = D('Api/Order');
                    $rs = $od->complatePay($obj);
                }
            }
            echo "success";
        } else {
            //验证失败
            echo "fail";

            //调试用，写文本函数记录程序运行情况是否正常
            //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
        }
    }

}
