<?php

namespace Api\Action;

use Think\Controller\RestController;

class BaseAction extends RestController {

    public function __construct() {
        parent::__construct();
    }

    public function restApi($code, $info = '', $data = array()) {
        $newData = (!is_array($data) || count($data)==0)?array():$data;
             
        $this->response(array('code' => $code, 'info' => $info, 'data' => $newData), 'json');
    }

    public function isUserLogin() {
        $rt = array();
        $rt['code'] = 100001;
        $rt['info'] = '请登录!';

        $token = I('token');
        if (empty($token))
            $this->restApi($rt['code'], $rt['info']);
        $loginToken = M('login_token');
        $userId = $loginToken->where(array('token' => $token))->getField('userId');
        if ($userId > 0) {
            $user = M('users');
            $userInfo = $user->where(array('userId' => $userId))->field('userId, userName, userPhone, userType,userType,userStatus,rongToken')->find();
            if (empty($userInfo))
                $this->restApi($rt['code'], $rt['info']);
            if ($userInfo['userStatus'] == 0) {
                $rt['code'] = 100002;
                $rt['info'] = '你的账号已被停用，请联系客服：075582438858!';
                $this->restApi($rt['code'], $rt['info']);
            }

            return $userInfo;
        }

        $this->restApi($rt['code'], $rt['info']);
    }

    /**
     * 获取经纬度
     */
    public function getLonLat($address) {
        $address = urlencode($address);
        $ch = curl_init();
        $url = 'http://apis.baidu.com/3023/geo/code?a=' . $address;
        $header = array(
            'apikey: b1e361ecc709fa77f0f40896bf18c3ca',
        );
        // 添加apikey到header
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // 执行HTTP请求
        curl_setopt($ch, CURLOPT_URL, $url);
        $res = curl_exec($ch);
        $res = json_decode($res, TRUE);

        return $res;
    }

    public function checkAddGoodsAuth($userInfo, $type = '') {
        $m = D('Api/goods');
        $rs = $m->checkAddGoodsAuth($userInfo, $type);
        if ($rs['shopId'] > 0) {
            return $rs['shopId'];
        } else {
            $this->restApi($rs['code'], $rs['info']);
        }
    }

    /**
     * 获取所有节点商品的ID
     * @param type $goodsId
     */
    public function getNodeGoodsId($goodsId) {
        $isSource = M('goods')->where('godosId=' . $goodsId)->getField('isSource');
        if($isSource == 1) {
            $idArr = M('goods_resale')->where('sourceId=' . $goodsId)->field('goodsId')->select();
            $node = $goodsId;
        } else {
            $sourceId = M('goods_resale')->where('sourceId=' . $goodsId)->getField('sourceId');
            $idArr = M('goods_resale')->where('sourceId=' . $sourceId)->field('goodsId')->select();
            $node = $sourceId;
        }

        foreach ($idArr as $v) {
            $node .= ',' . $v['goodsId'];
        }
        $node = trim($node, ',');

        return $node;
    }

    /**
     * 单个上传图片
     */
    public function uploadImg() {
        set_time_limit(90);
        $dir = I('dir', 'uploads') ? I('dir', 'uploads') : 'uploads';
        $config = array(
            'maxSize' => 0, //上传的文件大小限制 (0-不做限制)
            'exts' => array('jpg', 'png', 'gif', 'jpeg'), //允许上传的文件后缀
            'rootPath' => './Upload/', //保存根路径
            'driver' => 'LOCAL', // 文件上传驱动
            'subName' => array('date', 'Y-m'),
            'savePath' => $dir . "/"
        );
        $upload = new \Think\Upload($config);
        $rs = $upload->upload($_FILES);
 
        $Filedata = key($_FILES);
        $width = I('width', 640) ? I('width', 640) : 640;
        $height = I('height', 640) ? I('height', 640) : 640;
        if (!$rs) {
            $this->restApi(0, $upload->getError());
        } else {
            $images = new \Think\Image(); 
            $images->open('./Upload/' . $rs[$Filedata]['savepath'] . $rs[$Filedata]['savename']);
            $newsavename = str_replace('.', '_thumb.', $rs[$Filedata]['savename']);
            $vv = $images->thumb(I('width', 180), I('height', 180))->save('./Upload/' . $rs[$Filedata]['savepath'] . $newsavename);
            $rs[$Filedata]['savepath'] = "Upload/" . $rs[$Filedata]['savepath'];
            $rs[$Filedata]['savethumbname'] = $newsavename;
            $rs['picPath'] = $rs['pic']['savepath'] . $rs['pic']['savename'];
            $rs['picThumbPath'] = $rs['pic']['savepath'] . $rs['pic']['savethumbname'];
            unset($rs['pic']);
            return $rs;
        }
    }

    /**
     * 上传视频
     */
    public function uploadVideo() {
        set_time_limit(90);
        $key = 'video';
        $dir = 'video';
        $config = array(
            'maxSize' => 0, //上传的文件大小限制 (0-不做限制)
            'exts' => array('mp4'), //允许上传的文件后缀
            'rootPath' => './Upload/', //保存根路径
            'driver' => 'LOCAL', // 文件上传驱动
            'subName' => array('date', 'Y-m'),
            'savePath' => $dir . "/"
        );

        $upload = new \Think\Upload($config);
        $rs = $upload->upload($_FILES);
        $Filedata = key($_FILES);
        if (!$rs) {
            return;
        } else {
            $path = "Upload/" . $rs[$Filedata]['savepath'] . $rs[$Filedata]['savename'];
            return $path;
        }
    }

}
