<?php

namespace Api\Action;

class CartAction extends BaseAction {

    /**
     * 添加商品到购物车
     */
    public function addToCart() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Cart');
        $rs = $m->addToCart($userInfo['userId']);

        $this->restApi($rs['code'], $rs['info']);
    }

    /**
     * 购物车列表
     */
    public function index() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Cart');
        $data = $m->getCartList($userInfo['userId']);

        if ($data === FALSE)
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 修改购物车中的商品数量
     */
    public function changeCartGoodsNum() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Cart');
        $rs = $m->changeCartGoodsNum();

        $this->restApi($rs['code'], $rs['info']);
    }

    /**
     * 删除购物车中的商品
     */
    public function delCartGoods() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Cart');
        $rs = $m->delCartGoods();

        if($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }
    
    /**
     * 清空购物车中的商品
     */
    public function cleanCartGoods() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Cart');
        $rs = $m->cleanCartGoods($userInfo['userId']);

        if($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 购物车数量
     */
    public function cartCount() {
        $userInfo = $this->isUserLogin();
        $m = M('shop_cart');
        $data = array();
        $data['cartCount'] = $m->where('type = 0 and userId=' . $userInfo['userId'])->getField('sum(goodsNumber)');
        if(empty($data['cartCount']))
            $data['cartCount'] = '';
        $this->restApi(1, 'Success!', $data);
    }
}
