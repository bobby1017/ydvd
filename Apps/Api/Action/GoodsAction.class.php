<?php

namespace Api\Action;

/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 * 商品控制器
 */
class GoodsAction extends BaseAction {

    /**
     * 上传宝贝
     */
    public function addGoods() {
        $userInfo = $this->isUserLogin();
        $shopId = $this->checkAddGoodsAuth($userInfo);

        $m = D('Api/Goods');
        $rs = $m->addGoods($shopId);
        $this->restApi($rs['code'], $rs['info'], $rs['data']);
    }

    /**
     * 转售宝贝
     */
    public function resaleGoods() {
        $userInfo = $this->isUserLogin();
        $shopId = $this->checkAddGoodsAuth($userInfo, 'resale');

        $m = D('Api/Goods');
        $rs = $m->resaleGoods($shopId);
        $this->restApi($rs['code'], $rs['info'], $rs['data']);
    }

    /**
     * 编辑宝贝
     */
    public function editGoods() {
        $userInfo = $this->isUserLogin();
        $shopId = M('shops')->where('userId=' . $userInfo['userId'])->getField('shopId');

        $m = D('Api/Goods');
        $rs = $m->editGoods($shopId);
        $this->restApi($rs['code'], $rs['info'], $rs['data']);
    }

    /**
     * 编辑宝贝
     */
    public function editResale() {
        $userInfo = $this->isUserLogin();
        $shopId = M('shops')->where('userId=' . $userInfo['userId'])->getField('shopId');

        $m = D('Api/Goods');
        $rs = $m->editResale($shopId);
        $this->restApi($rs['code'], $rs['info'], $rs['data']);
    }

    /**
     * 获取款式列表
     */
    public function getCatsTree() {
        $m = D('Api/Goods');
        $catTree = $m->getCatsTree();

        if (empty($catTree))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $catTree);
    }

    /**
     * 获取种质列表
     */
    public function getClassTree() {
        $m = D('Api/Goods');
        $classTree = $m->getClassTree();

        if (empty($classTree))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $classTree);
    }

    /**
     * 获取转售商品详情
     */
    public function getParentGoods() {
        $m = D('Api/Goods');
        $goods = $m->getParentGoods();

        if (empty($goods))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $goods);
    }

    /**
     * 删除商品相册图片
     */
    public function delGoodsPic() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Goods');
        $rs = $m->delGoodsPic();
        if ($rs == 0)
            $this->restApi($rs, 'Error!');
        $this->restApi($rs, 'Success!');
    }

    /**
     * 获取商品详细信息
     */
    public function getGoodsDetail() {
        $m = D('Api/Goods');
        $goods = $m->getGoodsDetail();
        if (empty($goods))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $goods);
    }

    /**
     * 检测转售权限
     */
    public function checkResale() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Goods');
        $rs = $m->checkResale($userInfo['userId']);

        $this->restApi($rs['code'], $rs['info']);
    }

    /**
     * 删除视频
     */
    public function delVedio() {
        $userInfo = $this->isUserLogin();
        $m = M('goods');
        $goodsId = I('goodsId', 0);

        if ($goodsId > 0) {
            $rs = $m->where('goodsId=' . $goodsId)->save(array('goodsVedio' => '', 'vedioThumb' => ''));
            $this->restApi(1, 'Success!');
        }
        $this->restApi(0, 'Error!');
    }

    /**
     * 上传编辑商品图片
     */
    public function addGoodsPic() {
        $userInfo = $this->isUserLogin();
        $shopId = $this->checkAddGoodsAuth($userInfo);
 error_log(date("Y-m-d H:i:s")." Api\Model\GoodsAction.class.php line 154 get=".json_encode($_REQUEST)."\r\n\r\n",3,LOG_PATH."/__GoodsAction".date("Ymd").".log");
         

        $goodsId = I('goodsId', 0);
        $picId = I('picId', 0);
        if($goodsId <= 0)
            $this->restApi (0, 'Error!goodsId不是数字');
        $pictype = I('picType', '');
         
        if(!empty($_FILES)) {
            $pic = $this->uploadImg();
        }
        $log = array();
        $log['userInfo'] = $userInfo;
        $log['shopId'] = $shopId;
        $log['goodsId'] = $goodsId;
        $log['pic'] = json_encode($pic);
        error_log(date("Y-m-d H:i:s")." Api\Model\GoodsAction.class.php line 166 log=".json_encode($log)."\r\n\r\n",3,LOG_PATH."/__GoodsAction".date("Ymd").".log");
        $rs = 0;
        if($goodsId > 0  && !empty($pic)) {
            $data = array();
            $data['goodsId'] = $goodsId;
            $data['shopId'] = $shopId;
            $data['goodsImg'] = $pic['picPath'];
            $data['goodsThumbs'] = $pic['picThumbPath'];
            error_log(date("Y-m-d H:i:s")." Api\Model\GoodsAction.class.php line 175 data=".json_encode($data)."\r\n",3,LOG_PATH."/__GoodsAction".date("Ymd").".log");
            if($picId > 0) {
                $rs = M('goods_gallerys')->where('id = ' . $picId)->save($data);
            } else {
                $rs = M('goods_gallerys')->add($data);
                error_log(date("Y-m-d H:i:s")." Api\Model\GoodsAction.class.php line 180 rs=".$rs."\r\n\r\n",3,LOG_PATH."/__GoodsAction".date("Ymd").".log");
            }
            error_log(date("Y-m-d H:i:s")." Api\Model\GoodsAction.class.php line 182 sql=".M('goods_gallerys')->getLastSql()."\r\n\r\n",3,LOG_PATH."/__GoodsAction".date("Ymd").".log");
             
        }

        if($rs <= 0 ) {
            $this->restApi (0, 'Error!');
        }
        $this->restApi (1, 'Success!');
    }

    /**
     * 图库上传或维护图片
     */
    public function addGallerys($param){

        // $this->addGallerys(array('shopId'=>$shopId,'goodsId'=>$goodsId ,'picId'=>$picId));
    /*
        extract($param)
        if(!empty($_FILES)) {
            $pic = $this->uploadImg();
        }
        $log = array();
        
        $log['pic'] = json_encode($pic);
        error_log(date("Y-m-d H:i:s")." Api\Model\GoodsAction.class.php line 166 param=".json_encode($param)."\r\n\r\n",3,LOG_PATH."/__GoodsAction".date("Ymd").".log");
        $rs = 0;
      */

    }
    /**
     * 转售商品时未修改图片处理
     */
    public function addResalePic() {
        $userInfo = $this->isUserLogin();
        $shopId = $this->checkAddGoodsAuth($userInfo);

        $goodsId = I('goodsId', 0);
        $picIds = I('picIds', '');
        if($goodsId > 0 && !empty($picIds)) {
            $picIds = explode(',', $picIds);
            foreach($picIds as $v) {
                $data = M('goods_gallerys')->where('id = ' . $v)->find();
                $data['goodsId'] = $goodsId;
                $data['shopId'] = $shopId;
                unset($data['id']);
                $rs = M('goods_gallerys')->add($data);
            }
            $this->restApi (1, 'Success!');
        }

        $this->restApi (0, 'Error!');
    }

    /**
     * 上传编辑商品视频
     */
    public function addGoodsVideo() {
        $userInfo = $this->isUserLogin();
        $shopId = $this->checkAddGoodsAuth($userInfo);

        $goodsId = I('goodsId', 0);
        if($goodsId <= 0)
            $this->restApi (0, 'Error!');
        if(!empty($_FILES)) {
            $video = $this->uploadVideo();
        }
        $rs = 0;
        if($goodsId > 0 && $shopId > 0 && !empty($video)) {
            $rs = M('goods')->where('goodsId = ' . $goodsId)->save(array('goodsVedio' => $video));
        }

        if($rs <= 0 )
            $this->restApi (0, 'Error!');
        $this->restApi (1, 'Success!');
    }

}
