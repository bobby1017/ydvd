<?php
namespace Api\Action;
/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 */
class IndexAction extends BaseAction {
    /**
     * 获取商品详细信息
     */
    public function getGoodsDetail() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Index');
        $goods = $m->getGoodsDetail($userInfo);
        if($goods == 0)
            $this->restApi(1, 'No Data!');
        
        $this->restApi(1, 'Success!', $goods);
    }

    /**
     * 商品评论
     */
    public function goodsComment() {
        $m = D('Api/Index');
        $comment = $m->goodsComment();
        if(empty($comment))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $comment);
    }

    /**
     * 获取款式|种质|价格区间数据
     */
    public function catsClassPrice() {
        $m = D('Api/Index');
        $data = $m->catsClassPrice();
        if(empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 宝贝列表(搜索|筛选)
     */
    public function goodsList() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Index');
        $goodsList = $m->goodsList($userInfo['userId']);
        if(empty($goodsList))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $goodsList);
    }

    /**
     * 所有宝贝列表(搜索|筛选)
     */
    public function goodsAllList() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Index');
        $goodsList = $m->goodsAllList($userInfo['userId']);
        if(empty($goodsList))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $goodsList);
    }

    /**
     * 推荐宝贝列表(搜索|筛选)
     */
    public function goodsBestList() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Index');
        $goodsList = $m->goodsBestList($userInfo['userId']);
        if(empty($goodsList))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $goodsList);
    }

    /**
     * 推荐商铺列表(搜索|筛选)
     */
    public function goodsBestShop() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Index');
        $goodsList = $m->goodsBestShop($userInfo['userId']);
        if(empty($goodsList))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $goodsList);
    }

    /**
     * 文章展示
     */
    public function article() {
        $m = D('Api/Index');
        $article = $m->getArticle();

        if(empty($article))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $article);
    }

    /**
     * 首页广告
     */
    public function indexAds() {
        $m = D('Api/Index');
        $ads = $m->indexAds();

        if(empty($ads))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $ads);
    }

    /**
     * 使用帮助
     */
    public function helpList() {
        $m = D('Api/Index');
        $help = $m->helpList();

        if(empty($help))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $help);
    }

    /**
     * 软件版本
     */
    public function softVersion() {
        $m = M('upsoft');
        $data = $m->where('id=1')->find();
        $data['downloadUrl'] = str_replace('m.yushangcn.com','yushangcn.com',$data['downloadUrl']);
 
//        $rt['version'] = '1.0';
//        $rt['content'] = '';
//        $rt['upUrl'] = '';
//        $rt['downloadUrl'] = '';
//        $rt['createTime'] = '';

        $this->restApi(1, 'Success!', $data);
    }
}