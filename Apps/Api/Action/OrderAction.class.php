<?php

namespace Api\Action;

class OrderAction extends BaseAction {
    /**
     * 订单结算
     */
    public function checkOrder() {
     
        $userInfo = $this->isUserLogin();
        $m = D('Api/Order');
        $rs = $m->checkOrder($userInfo['userId']);

        $this->restApi($rs['code'], $rs['info'], $rs['data']);
    }

    /**
     * 订单支付
     */
    public function PayOrder() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Order');
        $rs = $m->PayOrder($userInfo['userId']);

        $this->restApi($rs['code'], $rs['info'], $rs['data']);
    }

    /**
     * 门店列表
     */
    public function lineStore() {
        $m = D('Api/Order');
        $data = $m->lineStore();

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    
}