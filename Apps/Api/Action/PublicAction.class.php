<?php

namespace Api\Action;

class PublicAction extends BaseAction {

    /**
     * 获取省市区
     */
    public function getAreas() {
        $m = M('areas');
        $parentId = I('parentId');
        $areas = $m->where(array('parentId' => $parentId))->field('areaId, areaName')->select();
        if (empty($areas))
            $this->restApi(0, 'Error!', $areas);
        $this->restApi(1, 'Success!', $areas);
    }

    /**
     * 单个上传图片
     */
    public function uploadPic() {
        set_time_limit(90);
        $dir = I('dir', 'uploads') ? I('dir', 'uploads') : 'uploads';
        $config = array(
            'maxSize' => 0, //上传的文件大小限制 (0-不做限制)
            'exts' => array('jpg', 'png', 'gif', 'jpeg'), //允许上传的文件后缀
            'rootPath' => './Upload/', //保存根路径
            'driver' => 'LOCAL', // 文件上传驱动
            'subName' => array('date', 'Y-m'),
            'savePath' => $dir . "/"
        );
        $upload = new \Think\Upload($config);
        $rs = $upload->upload($_FILES);
        $Filedata = key($_FILES);
        $width = I('width', 640) ? I('width', 640) : 640;
        $height = I('height', 640) ? I('height', 640) : 640;
        if (!$rs) {
            $this->restApi(0, $upload->getError());
        } else {
            $images = new \Think\Image();
            $images->open('./Upload/' . $rs[$Filedata]['savepath'] . $rs[$Filedata]['savename']);
            $newsavename = str_replace('.', '_thumb.', $rs[$Filedata]['savename']);
            $vv = $images->thumb(I('width', 180), I('height', 180))->save('./Upload/' . $rs[$Filedata]['savepath'] . $newsavename);
            $rs[$Filedata]['savepath'] = "Upload/" . $rs[$Filedata]['savepath'];
            $rs[$Filedata]['savethumbname'] = $newsavename;
            $rs['picPath'] = $rs['pic']['savepath'] . $rs['pic']['savename'];
            $rs['picThumbPath'] = $rs['pic']['savepath'] . $rs['pic']['savethumbname'];
            unset($rs['pic']);
            $this->restApi(1, 'Success!', $rs);
        }
    }

    /**
     * 支付方式
     */
    public function payType() {
        $m = M('payments');
        $list = $m->field('id,payCode,payName')->select();

        $this->restApi(1, 'Success!', $list);
    }

    /**
     * 上传视频
     */
    public function uploadVedio() {
        set_time_limit(90);
        $key = 'vedio';
        $dir = 'vedio';
        $config = array(
            'maxSize' => 0, //上传的文件大小限制 (0-不做限制)
            'exts' => array('mp4'), //允许上传的文件后缀
            'rootPath' => './Upload/', //保存根路径
            'driver' => 'LOCAL', // 文件上传驱动
            'subName' => array('date', 'Y-m'),
            'savePath' => $dir . "/"
        );

        $upload = new \Think\Upload($config);
        $rs = $upload->upload($_FILES);
        $Filedata = key($_FILES);
        if (!$rs) {
            $this->restApi(0, $upload->getError());
        } else {
            $path = "Upload/" . $rs[$Filedata]['savepath'] . $rs[$Filedata]['savename'];
            $this->restApi(1, 'Success!', array('savePath' => $path));
        }
    }

    /**
     * 统一统计
     */
    public function allSum() {
        $userInfo = $this->isUserLogin();
        $data = array();
        $m = M('shop_cart');
        $data['cartCount'] = $m->where('type = 0 and userId=' . $userInfo['userId'])->getField('sum(goodsNumber)');
        if(empty($data['cartCount']))
            $data['cartCount'] = 0;
        $m = D('Api/UserOrder');
        $data['userOrderSum'] = $m->userOrderSum($userInfo['userId']);
        if($userInfo['userType'] == 0) {
            $data['shopOrderSum'] = array('nopay'=> 0, 'noshipping'=>0,'nosign'=>0, 'server' => 0);
        } else {
            $data['shopOrderSum'] = $m->shopOrderSum($userInfo['userId']);
        }

        $m = D('Api/Zone');
        $sumNF = $m->newFriendsSum($userInfo['userId']);
        $data['newFriendsSum'] = $sumNF[0]['sum'];
        $m = M('user_sys_msg');
        $sum = $m->where('isRead = 0 and userId=' . $userInfo['userId'])->order('id desc')->count();
        $sum = $sum > 200 ? 200 : $sum;
        $data['sysMsgListSum'] = $sum;

        $m = D('Api/UserOrder');
        $m->upAutoOrderStatus($userInfo['userId']);

        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 打开APP自动运行计划任务
     */
    public function autoRun() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/UserOrder');
        $rs = $m->upAutoOrderStatus($userInfo['userId']);
        $this->restApi(1, 'Success!');
    }
}
