<?php

namespace Api\Action;

class ShopOrderAction extends BaseAction {
    /**
     * 销售订单列表
     */
    public function orderList() {
        $userInfo = $this->isUserLogin();

        $m = D('Api/ShopOrder');
        $data = $m->orderList($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 订单详情
     */
    public function orderInfo() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/UserOrder');
        $data = $m->orderInfo($userInfo['userId']);

        if (empty($data))
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 快递列表
     */
    public function expressList() {
        $m = M('express');
        $data = $m->field('expressId,expressName')->select();

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 商家发货填写单号
     */
    public function deliverGoods() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/ShopOrder');
        $rs = $m->deliverGoods();

        if($rs == FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 订单统计
     */
    public function orderCount() {
        $userInfo = $this->isUserLogin();

        $m = D('Api/ShopOrder');
        $data = $m->orderCount($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 金额统计
     */
    public function priceCount() {
        $userInfo = $this->isUserLogin();

        $m = D('Api/ShopOrder');
        $data = $m->priceCount($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 访客统计
     */
    public function visitorCount() {
        $userInfo = $this->isUserLogin();

        $m = D('Api/ShopOrder');
        $data = $m->visitorCount($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 收藏统计
     */
    public function followCount() {
        $userInfo = $this->isUserLogin();

        $m = D('Api/ShopOrder');
        $data = $m->followCount($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }
}