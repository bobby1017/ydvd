<?php

namespace Api\Action;

class ShopsAction extends BaseAction {

    /**
     * 个人入驻申请
     */
    public function openPerShop() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Shops');
        $rs = $m->addPerShop($userInfo);

        if ($rs['code'] == 0) {
            $this->restApi(0, $rs['info']);
        } else {
            $this->restApi(1, $rs['info']);
        }
    }

    /**
     * 企业入驻申请
     */
    public function openCompShop() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Shops');
        $rs = $m->addCompShop($userInfo);

        if ($rs['code'] == 0) {
            $this->restApi(0, $rs['info']);
        } else {
            $this->restApi(1, $rs['info']);
        }
    }

    /**
     * 担保交易等级
     */
    public function deposit() {
        $userInfo = $this->isUserLogin();

        $m = M('shops_deposit');
        $lists = M('shops')->where('userId=' . $userInfo['userId'])->field('depositId,depositMoney')->find();
        $lists['deposit'] = $m->where(1)->order('level ASC')->select();
        if (empty($lists))
            $this->restApi(0, 'Error!', $lists);
        $this->restApi(1, 'Success!', $lists);
    }

    /**
     * 更新店铺保证金等级
     */
    public function upShopDeposit() {
        $userInfo = $this->isUserLogin();

        $m = M('shops');
        $shopId = $m->where('userId=' . $userInfo['userId'])->getField('shopId');
        if ($shopId > 0) {
            $rs = $m->where('shopId=' . $shopId)->save(array('depositId' => I('depositId')));
            if ($rs === FALSE)
                $this->restApi(0, 'Error!', $rs);
            $this->restApi(1, 'Success!', $rs);
        }

        $this->restApi(0, '您还没有开通微店!');
    }

    /**
     * 更新店铺邮费
     */
    public function upShopPostage() {
        $userInfo = $this->isUserLogin();

        $m = M('shops');
        $shopId = $m->where('userId=' . $userInfo['userId'])->getField('shopId');
        if ($shopId > 0) {
            $rs = $m->where('shopId=' . $shopId)->save(array('postage' => I('postage')));
            if ($rs === FALSE)
                $this->restApi(0, 'Error!', $rs);
            $this->restApi(1, 'Success!', $rs);
        }

        $this->restApi(0, '您还没有开通微店!');
    }

    /**
     * 更新店铺名称
     */
    public function upShopName() {
        $userInfo = $this->isUserLogin();
        $shopId = I('shopId');
        $m = M('Shops');
        $m->shopName = I('shopName');
        $rs = $m->where('shopId=' . $shopId)->save();

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 更新店铺地址
     */
    public function upShopAddress() {
        $userInfo = $this->isUserLogin();
        $shopId = I('shopId');

        $m = M('shops');
        if ($shopId > 0) {
            $data = array();
            $data['areaId1'] = I('provinceId');
            $data['areaId2'] = I('cityId');
            $data['areaId3'] = I('districtId');
            $data['province'] = I('province');
            $data['city'] = I('city');
            $data['district'] = I('district');
            $data['shopAddress'] = I('address');
            $address = $data['province'] . $data['city'] . $data['district'] . $data['shopAddress'];
            $ll = $this->getLonLat($address);
            $data['longitude'] = $ll['lon'];
            $data['latitude'] = $ll['lat'];

            $rs = $m->where('shopId=' . $shopId)->save($data);
            if ($rs === FALSE)
                $this->restApi(0, 'Error!', $rs);
            $this->restApi(1, 'Success!', $rs);
        }

        $this->restApi(0, '您还没有开通微店!');
    }

    /**
     * 获取店铺基本信息
     */
    public function getShopConfig() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Shops');

        $shop = $m->getShopConfig($userInfo);
        if (empty($shop))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $shop);
    }

    /**
     * 修改店铺图标
     */
    public function modifyShopImg() {
        $userInfo = $this->isUserLogin();
        $shopId = I('shopId');
        $m = M('Shops');
        $m->shopImg = I('shopImg');
        $rs = $m->where('shopId=' . $shopId)->save();

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 修改店铺运费
     */
    public function modifyPostage() {
        $userInfo = $this->isUserLogin();
        $shopId = I('shopId');
        $m = M('Shops');
        $m->postage = I('postage');
        $rs = $m->where('shopId=' . $shopId)->save();

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 修改店铺模板ID
     */
    public function modifyTemplateId() {
        $userInfo = $this->isUserLogin();
        $shopId = I('shopId');
        $m = M('Shops');
        $m->templateId = I('templateId');
        $rs = $m->where('shopId=' . $shopId)->save();

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 修改店铺展示形式
     */
    public function modifyListStyle() {
        $userInfo = $this->isUserLogin();
        $shopId = I('shopId');
        $m = M('Shops');
        $m->listStyle = I('listStyle');
        $rs = $m->where('shopId=' . $shopId)->save();

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 修改店铺联系电话
     */
    public function modifyShopTel() {
        $userInfo = $this->isUserLogin();
        $shopId = I('shopId');
        $m = M('Shops');
        $m->shopTel = I('shopTel');
        $rs = $m->where('shopId=' . $shopId)->save();

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 获取发布的商品
     */
    public function getGoodsList() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Shops');

        $goodsList = $m->getGoodsList();
        if (empty($goodsList))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $goodsList);
    }

    /**
     * 获取转售的商品
     */
    public function getResalesList() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Shops');

        $goodsList = $m->getResalesList();
        if (empty($goodsList))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $goodsList);
    }

    /**
     * 保证金支付
     */
    public function depositMoney() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Shops');
        $rs = $m->depositMoney($userInfo['userId']);

        if ($rs['code'] == 0)
            $this->restApi($rs['code'], $rs['info']);
        $this->restApi($rs['code'], $rs['info'], $rs['data']);
    }

    /**
     * 获取店铺验证信息
     */
    public function getShopAuth() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Shops');

        $data = $m->getShopAuth($userInfo);
        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 开关转售权限
     */
    public function openResale() {
        $userInfo = $this->isUserLogin();
        $m = M('Shops');
        $shopId = $m->where('userId=' . $userInfo['userId'])->getField('shopId');

        $m->openResale = I('openResale', 0);
        $rs = $m->where('shopId=' . $shopId)->save();

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 好友商家
     */
    public function shopFriend() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Shops');

        $data = $m->shopFriend($userInfo['userId']);
        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 设置是否同意转售
     */
    public function setResale() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Shops');

        $rs = $m->setResale($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 已售设置
     */
    public function setIsSelling() {
        $userInfo = $this->isUserLogin();
        $m = M('goods');
        $goodsId = I('goodsId', 0);
        $status = I('status', 0);
        $isSource = $m->where('goodsId=' . $goodsId)->getField('isSource');
        if ($isSource) {
            $node = $this->getNodeGoodsId($goodsId);
            if ($status == 1) {
                $goodsStock = $m->where('goodsId=' . $goodsId)->getField('goodsStock');
                if ($goodsStock == 0) {
                    $this->restApi(0, '此宝贝已卖光,无法再上架!');
                }

                if (empty($node)) {
                    $rs = $m->where('goodsId=' . $goodsId)->save(array('isSelling' => $status));
                } else {
                    $rs = $m->where('goodsId in (' . $node . ')')->save(array('isSelling' => $status));
                }
            } else {
                if (empty($node)) {
                    $rs = $m->where('goodsId=' . $goodsId)->save(array('isSelling' => $status));
                } else {
                    $rs = $m->where('goodsId in (' . $node . ')')->save(array('isSelling' => $status));
                }
            }
        } else {
            $rs = $m->where('goodsId=' . $goodsId)->save(array('isSelling' => $status));
        }


        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 删除宝贝
     */
    public function delGoods() {
        $userInfo = $this->isUserLogin();
        $m = M('goods');
        $goodsId = I('goodsId', 0);
        $isSource = $m->where('goodsId=' . $goodsId)->getField('isSource');
        if ($isSource) {
            $node = $this->getNodeGoodsId($goodsId);
            if (empty($node)) {
                $rs = $m->where('goodsId=' . $goodsId)->save(array('isDel' => 1));
            } else {
                $rs = $m->where('goodsId in (' . $node . ')')->save(array('isDel' => 1));
            }
        } else {
            $rs = $m->where('goodsId=' . $goodsId)->save(array('isDel' => 1));

	    //删除shops_resale表中的数据
	    $shopId = M('shops')->where(array('userId' => $userInfo['userId']))->getField('shopId');
            $map['goodsId'] = $goodsId;
            $map['shopId'] = $shopId;
            M('goods_resale')->where($map)->delete();
        }


        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

}
