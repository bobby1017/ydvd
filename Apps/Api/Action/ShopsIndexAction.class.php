<?php

namespace Api\Action;

class ShopsIndexAction extends BaseAction {

    /**
     * 店铺首页新品宝贝
     */
    public function newGoodsList() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/ShopsIndex');
        $goodsList = $m->newGoodsList();

        if(empty($goodsList))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $goodsList);
    }

     /**
     * 店铺首页在售宝贝
     */
    public function saleGoodsList() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/ShopsIndex');
        $goodsList = $m->saleGoodsList();

        if(empty($goodsList))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $goodsList);
    }

    /**
     * 店铺首页已售宝贝
     */
    public function soldGoodsList() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/ShopsIndex');
        $goodsList = $m->soldGoodsList();

        if(empty($goodsList))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $goodsList);
    }

     /**
     * 店铺基本信息
     */
    public function shopInfo() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/ShopsIndex');
        $shopInfo = $m->shopInfo($userInfo['userId']);

        if(empty($shopInfo))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $shopInfo);
    }

}