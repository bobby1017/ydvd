<?php

namespace Api\Action;

class UserOrderAction extends BaseAction {

    /**
     * 待付款订单列表
     */
    public function orderList() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/UserOrder');
        $data = $m->orderList($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 取消订单
     */
    public function cancelOrder() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/UserOrder');
        $rs = $m->cancelOrder($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 待付款订单结算
     */
    public function toCheckPay() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/UserOrder');
        $rs = $m->toCheckPay($userInfo['userId']);

        $this->restApi($rs['code'], $rs['info'], $rs['data']);
    }

    /**
     * 待付款订单支付
     */
    public function toPay() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Order');
        $rs = $m->toPay($userInfo['userId']);

        $this->restApi($rs['code'], $rs['info'], $rs['data']);
    }

    /**
     * 提醒发货
     */
    public function remind() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/UserOrder');
        $rs = $m->remind($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        if($rs === 10)
            $this->restApi(0, '今日已经提醒!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 订单详情
     */
    public function orderInfo() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/UserOrder');
        $data = $m->orderInfo($userInfo['userId']);

        if (empty($data))
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 确认收货
     */
    public function confirm() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/UserOrder');
        $rs = $m->confirm($userInfo['userId']);

        if ($rs == FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 快递信息
     */
    public function expressNo() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/UserOrder');
        $data = $m->expressNo($userInfo['userId']);

        if (empty($data))
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 商品评价
     */
    public function appraises() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/UserOrder');
        $rs = $m->appraises($userInfo['userId']);

        if ($rs == FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 售后信息
     */
    public function serviceInfo() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/UserOrder');
        $data = $m->serviceInfo($userInfo['userId']);

        if (empty($data))
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 提交退货
     */
    public function returnGoods() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/UserOrder');
        $rs = $m->returnGoods($userInfo['userId']);

        if ($rs == FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 用户订单数量统计
     */
    public function userOrderSum() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/UserOrder');
        $sum = $m->userOrderSum($userInfo['userId']);

        $this->restApi(1, 'Success!', $sum);
    }

    /**
     * 销售订单数量统计
     */
    public function shopOrderSum() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/UserOrder');
        $sum = $m->shopOrderSum($userInfo['userId']);

        $this->restApi(1, 'Success!', $sum);
    }

    /**
     * 自动更新用户订单
     */
    public function upAutoOrderStatus() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/UserOrder');
        $rs = $m->upAutoOrderStatus($userInfo['userId']);
        $this->restApi(1, 'Success!');
    }
}
