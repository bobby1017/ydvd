<?php

namespace Api\Action;
class UsersAction extends BaseAction {

    public function login() {
        $m = D('Api/Users');
        $userPhone = I('userPhone');
        $pwd = I('loginPwd');

        $rs = $m->login($userPhone, $pwd);
        if ($rs['code'] == 0)
            $this->restApi($rs['code'], $rs['info']);
        $shopStatus = $m->checkShopStauts($rs);

        $this->restApi($rs['code'], $rs['info'], array_merge($rs, $shopStatus));
    }

    public function regist() {
        $m = D('Api/Users');
        $reutrn = $m->regist();
        if ($reutrn['status'] > 0) {
            if ($reutrn['code'] == 0)
                $this->restApi($reutrn['code'], $reutrn['info']);
            $this->restApi($reutrn['status'], $reutrn['msg'], $reutrn);
        }
        $this->restApi($reutrn['status'], $reutrn['msg'], $reutrn);
    }

    public function rePwd() {
        $m = D('Api/Users');
        $reutrn = $m->rePwd();
        if ($reutrn['status'] > 0) {
            $this->restApi(1, '找回密码成功!', $reutrn);
        }
        $this->restApi(0, '找回密码失败!', $reutrn);
    }

    /**
     * 获取注册手机验证码
     */
    public function getRegPhoneVerifyCode() {
        $userPhone = I("userPhone");
        $rs = array();
        if (!preg_match("#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#", $userPhone)) {
            $this->restApi(0, '手机号码格式错误!');
        }
        $m = D('Api/Users');
        $rs = $m->checkUserPhone($userPhone);
        if ($rs["status"] != 1) {
            $this->restApi(0, '该手机已经被注册!');
        }
        $phoneVerify = rand(100000, 999999);
        $rt = D('Api/Sms')->sendSMScode($userPhone, array($phoneVerify, 5), 'codeTempId', 'registUser');
        if ($rt['status'] == 1) {
            session('VerifyCode_userPhone', $phoneVerify);
            session('VerifyCode_userPhone_Time', time());
        }
        $rt['VerifyCode'] = $phoneVerify;
        $this->restApi($rt['status'], $rt['msg'], $rt);
    }

    /**
     * 获取找回密码手机验证码
     */
    public function getPwdPhoneVerifyCode() {
        $userPhone = I("userPhone");
        $rs = array();
        if (!preg_match("#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#", $userPhone)) {
            $this->restApi(0, '手机号码格式错误!');
        }
        $m = D('Api/Users');
        $rs = $m->checkUserPhone($userPhone);
        if ($rs["status"] == 1) {
            $this->restApi(0, '该手机账号不存在!');
        }
        $phoneVerify = rand(100000, 999999);
        $rt = D('Api/Sms')->sendSMScode($userPhone, array($phoneVerify, 5), 'codeTempId', 'reUserPwd');
        if ($rt['status'] == 1) {
            session('VerifyCode_userPhone', $phoneVerify);
            session('VerifyCode_userPhone_Time', time());
        }
        $rt['VerifyCode'] = $phoneVerify;
        $this->restApi($rt['status'], $rt['msg'], $rt);
    }

    /**
     * 检查开店状态
     */
    public function checkShopStauts() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $rs = $m->checkShopStauts($userInfo);

        $this->restApi(1, 'Success!', array_merge($userInfo, $rs));
    }

    /**
     * 个人资料
     */
    public function profile() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $data = $m->getProfile($userInfo['userId']);
        $m = D('Api/UserOrder');
        $m->upAutoOrderStatus($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 个人资料
     */
    public function editProfile() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $rs = $m->editProfile($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 添加|编辑用户地址
     */
    public function editAddress() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $rs = $m->editAddress($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 地址详细信息
     */
    public function address() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $data = $m->getAddress();

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 设置默认地址
     */
    public function setDefaultAddress() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $rs = $m->setDefaultAddress($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 用户地址列表
     */
    public function addressList() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $data = $m->getAddressList($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 删除用户地址
     */
    public function delAddress() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $rs = $m->delAddress();

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 关注商品
     */
    public function followGoods() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $rs = $m->followGoods($userInfo['userId']);

        if ($rs === -1)
            $this->restApi(0, '已关注');
        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 关注店铺
     */
    public function followShop() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $rs = $m->followShop($userInfo['userId']);

        if ($rs === -1)
            $this->restApi(0, '已关注');
        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 关注列表
     */
    public function followList() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $data = $m->getFollowList($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 删除关注
     */
    public function delFollow() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $rs = $m->delFollow();

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 用户钱包
     */
    public function userBurse() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $data = $m->userBurse($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 添加银行卡
     */
    public function userBankcard() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $data = $m->userBankcard($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 银行卡列表
     */
    public function bankcardList() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $data = $m->bankcardList($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 删除银行卡
     */
    public function delBankcard() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $data = $m->delBankcard($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 银行列表
     */
    public function bankList() {
        $m = M('banks');
        $data = $m->select();

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 资金记录
     */
    public function logBalanceList() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
//        $userInfo['userId'] = 50;
        $data = $m->logBalanceList($userInfo['userId']);
        $typeName = array(
            array('logType' => 1, 'typeName' => '订单支付'),
            array('logType' => 2, 'typeName' => '充值'),
            array('logType' => 3, 'typeName' => '提现'),
            array('logType' => 4, 'typeName' => '退款'),
            array('logType' => 5, 'typeName' => '转售收益'),
            array('logType' => 6, 'typeName' => '销售收益'),
            array('logType' => 7, 'typeName' => '提现失败退回'),
            array('logType' => 8, 'typeName' => '商家退款'),
            array('logType' => 9, 'typeName' => '退回保证金')
        );

        if (empty($data))
            $data = array();
        if ($userInfo['userType'] == 0) {
            $typeName = array(
                array('logType' => 1, 'typeName' => '订单支付'),
                array('logType' => 2, 'typeName' => '充值'),
                array('logType' => 3, 'typeName' => '提现'),
                array('logType' => 4, 'typeName' => '退款'),
                array('logType' => 7, 'typeName' => '提现失败退回'),
                array('logType' => 8, 'typeName' => '商家退款'),
                array('logType' => 9, 'typeName' => '退回保证金')
            );
        }
        $rs['typeName'] = $typeName;
        $rs['data'] = $data;
        $this->restApi(1, 'Success!', $rs);
    }

    /**
     * 可提现详情
     */
    public function canTakeCash() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $data = $m->canTakeCash($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 提现
     */
    public function takeCash() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $rs = $m->takeCash($userInfo['userId']);

        $this->restApi($rs['code'], $rs['info']);
    }

    /**
     *  充值
     */
    public function depositLog() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');

        $rs = $m->depositLog($userInfo['userId']);

        if ($rs == FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!', $rs);
    }

    /**
     * 系统消息
     */
    public function sysMsgList() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $data = $m->sysMsgList($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 系统消息数量
     */
    public function sysMsgListSum() {
        $userInfo = $this->isUserLogin();
        $m = M('user_sys_msg');
        $sum = $m->where('isRead = 0 and userId=' . $userInfo['userId'])->order('id desc')->count();
        $sum = $sum > 200 ? 200 : $sum;

        $this->restApi(1, 'Success!', array('sum' => $sum));
    }

    /**
     * 系统消息详情
     */
    public function sysMsgInfo() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $data = $m->sysMsgInfo($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 意见反馈
     */
    public function feedback() {
        $userInfo = $this->isUserLogin();

        $m = M('user_feedback');
        $content = I('content', '');
        $phone = I('phone', '');
        if ($content == '')
            $this->restApi(0, '请将自信填写完整!');
        $data = array();
        $data['userId'] = $userInfo['userId'];
        $data['content'] = $content;
        $data['phone'] = $phone;
        $data['createTime'] = date('Y-m-d H:i:s');

        $rs = $m->add($data);

        if ($rs == FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!', $rs);
    }

    /**
     * 修改密码
     */
    public function modifyPassword() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Users');
        $rs = $m->modifyPassword($userInfo['userId']);

        $this->restApi($rs['code'], $rs['info']);
    }

    /**
     * 获取融云TOKEN
     * @return type
     */
    public function getRongCloudUserToke($uid, $name, $portraitUri) {
        $appKey = 'e5t4ouvptfp1a';
        $appSecret = 'LXLYdXbNnE3';

        vendor("RongCloud.ServerAPI");
        $rongCloud = new \RongServerAPI($appKey, $appSecret);

//        $uid = 1;
//        $name = '13751082273';
//        $portraitUri = C('WEB_URL') . '/Public/images/default_haeder.png';
        $token = $rongCloud->getToken($uid, $name, $portraitUri);

        return $token;
    }

    /**
     * 更新用户容云TOKEN
     */
    public function reRongCloudUserToke() {
        $m = M('users');
        $users = $m->select();

        foreach ($users as $v) {
            $uid = $v['userId'];
            $name = $v['userPhone'];
            $portraitUri = C('WEB_URL') . '/Public/images/default_haeder.png';

            $token = $this->getRongCloudUserToke($uid, $name, $portraitUri);
            $token = json_decode($token, TRUE);
            $token = $token['token'];
            $rs = $m->where('userId=' . $v['userId'])->save(array('rongToken' => $token));
        }
    }

    public function pushTest() {
        $push = D('Api/Push');
//        $rs = $push->pushSingleDeviceNotification('玉都微店', '中华人民共和国!!!!', '2a1483291f36cae1d153d8e811439abf2c737ce712e25e35ea');
        $type = I('type', 'all');
        $arg = I('arg', '');
        if ($type == 'all') {
            $rs = $push->pushAllIOS('推送测试!!!!');
            dump($rs);
            $rs = $push->PushAllAndroid('推送测试!!!!');
            dump($rs);
        } else if ($type == 'user') {
            $rs = $push->PushAccountIos('推送测试!!!!', $arg);
            dump($rs);
            $rs = $push->PushAccountAndroid('推送测试!!!!', $arg);
            dump($rs);
        } else if ($type == 'token') {
            $rs = $push->PushTokenIos('推送测试!!!!', $arg);
            dump($rs);
            $rs = $push->PushTokenAndroid('推送测试!!!!', $arg);
            dump($rs);
        }
    }

}