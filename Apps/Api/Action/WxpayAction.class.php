<?php

namespace Api\Action;

class WxpayAction extends BaseAction {

    /**
     * 统一下单 生成package
     */
    public function makePackage() {
        ini_set('date.timezone', 'Asia/Shanghai');
        vendor('Wxpay.lib.WxPay_Api');
        vendor('Wxpay.lib.Config');
        $orderSN = I('orderSN');
        $priceSum = I('priceSum') * 100;
        
        //②、统一下单
        $input = new \WxPayUnifiedOrder();
        $input->SetBody("玉都微店");
        $input->SetAppid(APPID);
        $input->SetMch_id(MCHID);
        $input->SetNonce_str(md5(time()));
        $input->SetSign();
        $input->SetOut_trade_no($orderSN);
        $input->SetTotal_fee($priceSum);
        $input->SetSpbill_create_ip(get_client_ip());
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 600));
        $input->SetGoods_tag("玉都微店");
        $input->SetNotify_url(C('WEB_URL') . "/Wxpay/example/notify.php");
        $input->SetTrade_type("APP");
        $input->SetProduct_id(time());

        $WxPayApi = new \WxPayApi;
        $order = $WxPayApi::unifiedOrder($input);

        echo json_encode($order);
    }

    public function notify() {
        vendor('Wxpay.lib.WxPay_Api');
        vendor('Wxpay.lib.Config');
        
        
    }
}
