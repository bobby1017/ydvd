<?php

namespace Api\Action;

class ZoneAction extends BaseAction {

    /**
     * 添加好友
     */
    public function addFriend() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Zone');
        if ($userInfo['userId'] == I('fuid'))
            $this->restApi(0, '不能加自己为好友!');
        $rs = $m->addFriend($userInfo['userId']);

        if ($rs === -1)
            $this->restApi(0, '已经申请,等待对方同意!');
        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 新的好友
     */
    public function newFriends() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Zone');
        $data = $m->newFriends($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 新的好友数量
     */
    public function newFriendsSum() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Zone');
        $data = $m->newFriendsSum($userInfo['userId']);

        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 忽略好友
     */
    public function ignoreFriend() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Zone');
        $rs = $m->ignoreFriend();

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 同意好友
     */
    public function agreeFriend() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Zone');
        $rs = $m->agreeFriend();

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 删除好友
     */
    public function delFriend() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Zone');
        $rs = $m->delFriend($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 好友列表
     */
    public function friends() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Zone');
        $data = $m->friends($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 搜索店铺
     */
    public function searchShop() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Zone');
        $data = $m->searchShop();

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 附近店铺
     */
    public function nearbyShop() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Zone');
        $data = $m->nearbyShop();

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 搜索好友
     */
    public function searchFriends() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Zone');
        $data = $m->searchFriends($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 记录聊天好友
     */
    public function logImStatus() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Zone');
        $rs = $m->logImStatus($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 删除聊天好友
     */
    public function delLogIm() {
        $userInfo = $this->isUserLogin();
        $m = M('log_user_imstatus');
        $logId = I('logId');
        $rs = $m->where('logId=' . $logId)->delete();

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 聊天好友列表
     */
    public function getLogImlist() {
        $userInfo = $this->isUserLogin();
        $m = D('Api/Zone');
        $data = $m->getLogImlist($userInfo['userId']);

        if (empty($data))
            $this->restApi(1, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

}
