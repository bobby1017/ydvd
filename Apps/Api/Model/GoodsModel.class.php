<?php

namespace Api\Model;
/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 * 商品服务类

 */
class GoodsModel extends BaseModel {

    /**
     * 检查上传宝贝权限

     */
    public function checkAddGoodsAuth($userInfo, $type = '') {
        $rt = array();
        $rt['code'] = 0;
        $rt['info'] = 'Error!';
        if ($userInfo['userType'] == 0) {
             $rt['info'] = '普通用户无法上传宝贝!';

            return $rt;
        }

        $m = M('shops');
        $shop = $m->where('userId=' . $userInfo['userId'])->field('shopId,shopFlag,shopStatus,depositId')->find();
        if ($shop['shopFlag'] != 1 && $shop['shopStatus'] != 1) {
             $rt['info'] = '您的店铺目前没有上传宝贝的权限!';

            return $rt;
        }
        $m = M('shops_deposit');
        $deposit = $m->where('id=' . $shop['depositId'])->find();
        $m = M('goods');
        if ($type == 'resale') {
            $sum = $m->where('shopId=' . $shop['shopId'] . ' AND isDel = 0 AND isSource = 0')->count();
            if ($sum >= $deposit['resaleNum'] && $sum>0 ) {
                $rt['info'] = '您目前最多只能转售' . intval($deposit['resaleNum']) . '个宝贝!';

                return $rt;
            }
        } else {
            $sum = $m->where('shopId=' . $shop['shopId'] . ' AND isDel = 0 AND isSource = 1')->count();
            if ($sum >= $deposit['goodsNum']) {
               $rt['info'] = '您目前最多只能发布' . intval($deposit['goodsNum']) . '个宝贝!';

                return $rt;
            }
        }


        return $shop;
    }

    /**
     * 脡脧麓芦卤娄卤麓

     */
    public function addGoods($shopId) {
        $data = array();
        $data['shopId'] = $shopId;
         $data['goodsName'] = I('goodsName');    //售价
        $data['marketPrice'] = I('marketPrice', 0);    //售价
        $data['shopPrice'] = I('shopPrice', 0);    //合作价
        $data['goodsStock'] = I('goodsStock', 1);
        $cat = $this->getCatData(I('catId'));
        $data['goodsCatId1'] = $cat['goodsCatId1'];
        $data['goodsCatId2'] = $cat['goodsCatId2'];
        $class = $this->getClassData(I('classId'));
        $data['goodsClassId1'] = $class['goodsClassId1'];
        $data['goodsClassId2'] = $class['goodsClassId2'];
        $data['isOffline'] = I('isOffline', 0);
        $data['createTime'] = date('Y-m-d H:i:s');
        $goodsSn = I('goodsSn','');
        if (false==$goodsSn) {
            $data['goodsSn'] = 'SN'.time();
        } else {
            $data['goodsSn'] = 'SN'. str_replace("SN",'',$goodsSn); //goodsSn
        }
        $isGoodsVerify = M('sys_configs')->where('fieldCode = "isGoodsVerify"')->getField('fieldValue');
        if ($isGoodsVerify == 0) {
            $data['goodsStatus'] = 1;
        }
        // $pics = json_decode(htmlspecialchars_decode(I('pics')), TRUE);
        $thumb = json_decode(htmlspecialchars_decode(I('thumb', '')), TRUE);



        if ($thumb) {
            $data['goodsImg'] = $thumb['picPath'];
            $data['goodsThumbs'] = $thumb['picThumbPath'];
            unset($thumb);
        } else {
            if (!empty($pics[0])) {
                $data['goodsImg'] = $pics[0]['picPath'];
                $data['goodsThumbs'] = $pics[0]['picThumbPath'];
                unset($pics[0]);
            }
        }
       


        foreach ($data as $v) {
            if ($v === '') {
                $rt['code'] = 0;
                 $rt['info'] = '请将信息填写完整!';

                return $rt;
            }
        }
        $data['goodsSize'] = I('goodsSize');
        $data['goodsWeight'] = I('goodsWeight');
        $data['goodsVedio'] = I('goodsVedio', '');
        $data['vedioThumb'] = I('vedioThumb', '');
      //  $m = M('goods');


        $goodsId = M('goods')->add($data);


        if ($goodsId === FALSE) {
            $rt['code'] = 0;
            $rt['info'] = 'Error!';
            return $rt;
        }
        /*
        $galleryModel = M('goods_gallerys');

        if (!empty($pics) && count($pics)>0 ) {
            $dataList = array();
            foreach ($pics as $v) {
                $data = array();
                $data['goodsId'] = $goodsId;
                $data['shopId'] = $shopId;
                $data['goodsImg'] = $v['picPath'];
                $data['goodsThumbs'] = $v['picThumbPath'];
                $dataList[] = $data;
                $galleryModel->add($data);

            }
           

           // $rs = $m->addAll($dataList);
            error_log("Api\Model\GoodsModel.class.php line 132 ".json_encode($dataList)."\r\n",3,LOG_PATH."/__GoodsModel".date("Ymd").".log");
        }
*/


        $data = array();
        $data['goodsId'] = $goodsId;
        $data['shopId'] = $shopId;
        $data['goodsContent'] = I('content');
     
        $rs = M('goods_data')->add($data);


        return array('code' => 1, 'info' => 'Success!', 'data' => array('goodsId' => $goodsId, 'wapUrl' => C('WEB_URL') . U('Wap/Index/goodsDetail/id/' . $goodsId)));
    }

    /**
    * 转售宝贝

     */
    public function resaleGoods($shopId) {
        $m = M('goods');
        $pgoods = $m->where('goodsId = ' . I('parentId'))->find();
        if ($pgoods['goodsStatus'] == 0 || $pgoods['isSale'] == 0 || $pgoods['goodsStock'] == 0) {
            $rt['code'] = 0;
            $rt['info'] = '露脭虏禄脝冒,赂脙卤娄卤麓脛驴脟掳脦脼路篓脳陋脢脹!';

            return $rt;
        }

        $data = array();
        $data['shopId'] = $shopId;
  $data['goodsName'] = I('goodsName');    //售价
        $data['marketPrice'] = I('marketPrice', 0);    //售价
        $data['shopPrice'] = I('shopPrice', 0);    //合作价
        $data['goodsCatId1'] = $pgoods['goodsCatId1'];
        $data['goodsCatId2'] = $pgoods['goodsCatId2'];
        $data['goodsClassId1'] = $pgoods['goodsClassId1'];
        $data['goodsClassId2'] = $pgoods['goodsClassId2'];
        $data['isOffline'] = $pgoods['isOffline'];
        $data['goodsSn'] = I('goodsSn', ''); 

 
        $pics = json_decode(htmlspecialchars_decode(I('pics')), TRUE);
        $thumb = json_decode(htmlspecialchars_decode(I('thumb', '')), TRUE);

    

  
        if (!empty($thumb)) {
            $data['goodsImg'] = $thumb['picPath'];
            $data['goodsThumbs'] = $thumb['picThumbPath'];
        } else {
            if (!empty($pics)) {
                $data['goodsImg'] = $pics[0]['picPath'];
                $data['goodsThumbs'] = $pics[0]['picThumbPath'];
            }
        }

//        if(empty($data['goodsImg'])) {
//            $data['goodsImg'] = $pgoods['goodsImg'];
//        }
        if ($pgoods['marketPrice'] > $data['marketPrice'] || $pgoods['shopPrice'] > $data['shopPrice']) {
            $rt['code'] = 0;
            $rt['info'] = '转售的价格不得低于原宝贝的价格!';

            return $rt;
        }
        $data['createTime'] = date('Y-m-d H:i:s');
        $data['isSource'] = 0;
        $data['parentId'] = I('parentId', 0); 
        

        foreach ($data as $v) {
            if ($v === '') {
                $rt['code'] = 0;
                $rt['info'] = '请将信息填写完整!';

                return $rt;
            }
        }
        if (empty($data['goodsImg'])) {
            $rt['code'] = 0;
            $rt['info'] = '请上传封面图!';

            return $rt;
        }
        $data['goodsSize'] = $pgoods['goodsSize'];
        $data['goodsWeight'] = $pgoods['goodsWeight'];
        $data['goodsStatus'] = $pgoods['goodsStatus'];
        $data['goodsStock'] = $pgoods['goodsStock'];
        $data['goodsVedio'] = I('goodsVedio', '');
        $data['vedioThumb'] = I('vedioThumb', '');
        $goodsId = $m->add($data);
 

        if ($goodsId === FALSE) {
            $rt['code'] = 0;
            $rt['info'] = 'Error!';
            return $rt;
        }

        $data = array();
        $data['parentId'] = I('parentId');
        $data['goodsId'] = $goodsId;
        $data['shopId'] = $shopId;
        $data['pshopId'] = $pgoods['shopId'];
        if ($pgoods['isSource'] == 1) {
            $data['sourceId'] = $pgoods['goodsId'];
            $data['sshopId'] = $pgoods['shopId'];
            $data['node'] = $pgoods['goodsId'] . ',' . $goodsId;
        } else {
            $prgoods = M('goods_resale')->where('goodsId = ' . $pgoods['goodsId'])->find();
            $data['sourceId'] = $prgoods['sourceId'];
            $data['sshopId'] = $prgoods['sshopId'];
            $data['node'] = $prgoods['node'] . ',' . $goodsId;
        }

        M('goods_resale')->add($data);
  

        if (!empty($pics)) {
            $dataList = array();
         //   $pics2 = array_unique($pics);
            foreach ($pics as $v) {
                $data = array();
                $data['goodsId'] = $goodsId;
                $data['shopId'] = $shopId;
                $data['goodsImg'] = $v['picPath'];
                $data['goodsThumbs'] = $v['picThumbPath'];
                $is_unique = 0;
                foreach ($dataList as $v2) {  
                       if ($v['picPath'] == $v2['goodsImg']) {
                            $is_unique = 1;
                       } 
                }
                if (false==$is_unique) {
                     $dataList[] = $data;
                }
            }
            $m = M('goods_gallerys');
            $rs = $m->addAll($dataList);
   
        }


        $data = array();
        $data['goodsId'] = $goodsId;
        $data['shopId'] = $shopId;
        $data['goodsContent'] = I('content');
        $m = M('goods_data');
        $rs = $m->add($data);
      

        return array('code' => 1, 'info' => 'Success!', 'data' => array('goodsId' => $goodsId, 'wapUrl' => C('WEB_URL') . U('Wap/Index/goodsDetail/id/' . $goodsId)));
    }

    /**
     * 编辑宝贝

     */
    public function editGoods($shopId) {
        error_log(date("Y-m-d H:i:s")." Api\Model\GoodsModel.class.php line 310 get=".json_encode($_REQUEST)."\r\n\r\n",3,LOG_PATH."/__GoodsAction".date("Ymd").".log");

        $goodsId = I('goodsId');
        $m = M('goods');
        $oldGoods = $m->where('goodsId=' . $goodsId)->find();
        $data = array();
        $data['shopId'] = $shopId;
        $data['goodsName'] = I('goodsName');  //售价
        $data['marketPrice'] = I('marketPrice', 0);     //售价
        $data['shopPrice'] = I('shopPrice', 0);   //合作价
        $data['goodsStock'] = I('goodsStock', 1);
        $cat = $this->getCatData(I('catId'));
        $data['goodsCatId1'] = $cat['goodsCatId1'];
        $data['goodsCatId2'] = $cat['goodsCatId2'];
        $class = $this->getClassData(I('classId'));
        $data['goodsClassId1'] = $class['goodsClassId1'];
        $data['goodsClassId2'] = $class['goodsClassId2'];
        $data['isOffline'] = I('isOffline', 0);
//        $pics = json_decode(htmlspecialchars_decode(I('pics')), TRUE);
//        $data['goodsImg'] = $pics[0]['picPath'];
//        $data['goodsThumbs'] = $pics[0]['picThumbPath'];
//        show_pre_data($data);
        foreach ($data as $v) {
            if ($v === '') {
                $rt['code'] = 0;
               $rt['info'] = '请将信息填写完整!';

                return $rt;
            }
        }
        $data['goodsSize'] = I('goodsSize');
        $data['goodsWeight'] = I('goodsWeight');
        if (I('goodsVedio', '') != '') {
            $data['goodsVedio'] = I('goodsVedio', '');
        }
        if (I('vedioThumb', '') != '') {
            $data['vedioThumb'] = I('vedioThumb', '');
        }
        
        $thumb = json_decode(htmlspecialchars_decode(I('thumb', '')), TRUE);
        if (!empty($thumb['picPath'])) {
            $data['goodsImg'] = $thumb['picPath'];
            $data['goodsThumbs'] = $thumb['picThumbPath'];
        }
        error_log(date("Y-m-d H:i:s")." Api\Model\GoodsModel.class.php line 354 data=".json_encode($data)."\r\n\r\n",3,LOG_PATH."/__GoodsAction".date("Ymd").".log");
        $rs = $m->where('goodsId=' . $goodsId)->save($data);


        if ($rs === FALSE) {
            $rt['code'] = 0;
            $rt['info'] = '编辑宝贝错误Error!'.$m->getDbError();

            return $rt;
        }

/*
        $m = M('goods_gallerys');
        foreach ($pics as $v) {
            $tmp_data = array();
            error_log("Api\Model\GoodsModel.class.php line 336 v=".json_encode($v)."\r\n",3,LOG_PATH."/__GoodsModel".date("Ymd").".log");
            if (intval($v['id']) > 0) {


                $tmp_data['goodsImg'] = $v['picPath'];
                $tmp_data['goodsThumbs'] = $v['picThumbPath'];
                $m->where('id=' . $v['id'])->save($tmp_data);

            } else {
                $tmp_data['goodsId'] = $goodsId;
                $tmp_data['shopId'] = $shopId;
                $tmp_data['goodsImg'] = $v['picPath'];
                $tmp_data['goodsThumbs'] = $v['picThumbPath'];
                $tmpRst=$m->add($tmp_data);
                error_log("Api\Model\GoodsModel.class.php line 347 sql=".($m->getLastSql())."\r\n",3,LOG_PATH."/__GoodsModel".date("Ymd").".log");
                 error_log("Api\Model\GoodsModel.class.php line 348 tmpRst=".$tmpRst."\r\n",3,LOG_PATH."/__GoodsModel".date("Ymd").".log");
                error_log("Api\Model\GoodsModel.class.php line 349 ".json_encode($tmp_data)."\r\n",3,LOG_PATH."/__GoodsModel".date("Ymd").".log");

            }


            
        }
*/
        $data_content = array();
        $data_content['goodsContent'] = I('content');
        $m = M('goods_data');
        $rs = $m->where('goodsId=' . $goodsId)->save($data_content);

        if ($data['goodsStock'] != $oldGoods['goodsStock']) {
            $up_ids = M('goods_resale')->where('sourceId=' . $goodsId)->field('goodsId')->select();
            $up_ids = $this->idsToString($up_ids, 'goodsId');
            $rs = M('goods')->where('goodsId in (' . $up_ids . ')')->save(array('goodsStock' => $data['goodsStock']));
        }

        $up_marketPrice = I('marketPrice') - $oldGoods['marketPrice'];
        $up_shopPrice = I('shopPrice') - $oldGoods['shopPrice'];
        if ($up_marketPrice != 0 || $up_shopPrice != 0) {
            $up_ids = M('goods_resale')->where('sourceId=' . $goodsId)->field('goodsId')->select();
            $up_ids = $this->idsToString($up_ids, 'goodsId');
            if ($up_marketPrice != 0) {
                $rs = M('goods')->where('goodsId in (' . $up_ids . ')')->setInc('marketPrice', $up_marketPrice);
            }
            if ($up_shopPrice != 0) {
                $rs = M('goods')->where('goodsId in (' . $up_ids . ')')->setInc('shopPrice', $up_shopPrice);
            }
        }

        return array('code' => 1, 'info' => 'Success!', 'data' => array('goodsId' => $goodsId, 'wapUrl' => C('WEB_URL') . U('Wap/Index/goodsDetail/id/' . $goodsId)));
    }

    /**
     *编辑转售宝贝

     */
    public function editResale($shopId) {
        $goodsId = I('goodsId');
        $m = M('goods');
        $sql = "select * from __PREFIX__goods g, __PREFIX__goods_resale gr where g.goodsId = gr.goodsId and g.goodsId = " . $goodsId;
        $oldGoods = $this->queryRow($sql);
//        $goods_resale = M('goods_resale')->where('goodsId = ' . $goodsId)->find();
        $pgoods = $m->where('goodsId = ' . $oldGoods['parentId'])->find();

        $data = array();
        $data['shopId'] = $shopId;
        $data['goodsName'] = I('goodsName');    //售价
        $data['marketPrice'] = I('marketPrice', 0);    //售价
        $data['shopPrice'] = I('shopPrice', 0);  //合作价
//        $pics = json_decode(htmlspecialchars_decode(I('pics')), TRUE);
//        $data['goodsImg'] = $pics[0]['picPath'];
//        $data['goodsThumbs'] = $pics[0]['picThumbPath'];
        if ($pgoods['marketPrice'] > $data['marketPrice'] || $pgoods['shopPrice'] > $data['shopPrice']) {
            $rt['code'] = 0;
            $rt['info'] = '价格不得低于原宝贝的价格!';

            return $rt;
        }

        foreach ($data as $v) {
            if ($v === '') {
                $rt['code'] = 0;
                $rt['info'] = '请将信息填写完整!';

                return $rt;
            }
        }
        if (I('goodsVedio', '') != '') {
            $data['goodsVedio'] = I('goodsVedio', '');
        }
        if (I('vedioThumb', '') != '') {
            $data['vedioThumb'] = I('vedioThumb', '');
        }
        $thumb = json_decode(htmlspecialchars_decode(I('thumb', '')), TRUE);
        if ($thumb) {
            $data['goodsImg'] = $thumb['picPath'];
            $data['goodsThumbs'] = $thumb['picThumbPath'];
        }
        $data['goodsSn'] = I('goodsSn', '');
        $rs = $m->where('goodsId=' . $goodsId)->save($data);
        if ($rs === FALSE) {
            $rt['code'] = 0;
            $rt['info'] = 'Error!';
            return $rt;
        }

        $m = M('goods_gallerys');
        foreach ($pics as $v) {
            $data = array();
            if (intval($v['id']) == 0) {
                $data['goodsId'] = $goodsId;
                $data['shopId'] = $shopId;
                $data['goodsImg'] = $v['picPath'];
                $data['goodsThumbs'] = $v['picThumbPath'];
                $m->add($data);
            } else {
                $data['goodsImg'] = $v['picPath'];
                $data['goodsThumbs'] = $v['picThumbPath'];
                $m->where('id=' . $v['id'])->save($data);
            }
        }

        $data = array();
        $data['goodsContent'] = I('content');
        $m = M('goods_data');
        $rs = $m->where('goodsId=' . $goodsId)->save($data);

        $up_marketPrice = I('marketPrice') - $oldGoods['marketPrice'];
        $up_shopPrice = I('shopPrice') - $oldGoods['shopPrice'];
        if ($up_marketPrice != 0 || $up_shopPrice != 0) {
            $up_ids = M('goods_resale')->where("node like '" . $oldGoods['node'] . ",%'")->field('goodsId')->select();
            $up_ids = $this->idsToString($up_ids, 'goodsId');
            if ($up_marketPrice != 0) {
                $rs = M('goods')->where('goodsId in (' . $up_ids . ')')->setInc('marketPrice', $up_marketPrice);
            }
            if ($up_shopPrice != 0) {
                $rs = M('goods')->where('goodsId in (' . $up_ids . ')')->setInc('shopPrice', $up_shopPrice);
            }
        }

        return array('code' => 1, 'info' => 'Success!', 'data' => array('goodsId' => $goodsId, 'wapUrl' => C('WEB_URL') . U('Wap/Index/goodsDetail/id/' . $goodsId)));
    }

    /**
     * 获取款式两层ID

     * @param type $catId
     * @return type
     */
    public function getCatData($catId) {
        $m = M('goods_cats');
        $cat = $m->where('catId=' . $catId)->field('catId, parentId')->find();
        if ($cat['parentId'] == 0) {
            return array('goodsCatId1' => $catId, 'goodsCatId2' => 0);
        }
        return array('goodsCatId1' => $cat['parentId'], 'goodsCatId2' => $catId);
    }

    /**
     * 获取种质两层ID

     * @param type $classId
     * @return type
     */
    public function getClassData($classId) {
        $m = M('goods_class');
        $class = $m->where('classId=' . $classId)->field('classId, parentId')->find();
        if ($class['parentId'] == 0) {
            return array('goodsClassId1' => $classId, 'goodsClassId2' => 0);
        }
        return array('goodsClassId1' => $class['parentId'], 'goodsClassId2' => $classId);
    }

    /**
     * 获取款式列表

     */
    public function getCatsTree() {
        $m = M('goods_cats');
        $catTree = $m->where('catFlag = 1 AND parentId = 0 AND isShow = 1')->order('catSort ASC')->field('catId, catName')->select();
        foreach ($catTree as $k => $v) {
            $child = $m->where('catFlag = 1 AND parentId = ' . $v['catId'] . ' AND isShow = 1')->order('catSort ASC')->field('catId, catName')->select();
            $catTree[$k]['child'] = $child;
        }

        return $catTree;
    }

    /**
     * 获取种质列表

     */
    public function getClassTree() {
        $m = M('goods_class');
        $classTree = $m->where('classFlag = 1 AND parentId = 0 AND isShow = 1')->order('classSort ASC')->field('classId, className')->select();
        foreach ($classTree as $k => $v) {
            $child = $m->where('classFlag = 1 AND parentId = ' . $v['classId'] . ' AND isShow = 1')->order('classSort ASC')->field('classId, className')->select();
            $classTree[$k]['child'] = $child;
        }

        return $classTree;
    }

    /**
    
     * 获取转售商品详情

     */
    public function getParentGoods() {
        $goodsId = I('goodsId');
        //$goods = M('goods')->where('goodsId=' . $goodsId)->field('goodsId,goodsSn,goodsName,shopId,marketPrice,shopPrice,goodsCatId2,goodsClassId2,goodsSize,goodsWeight,goodsVedio,vedioThumb,goodsImg,goodsThumbs')->find();
        
        $goods = M('goods')->where('goodsId=' . $goodsId)->field('goodsId,goodsSn,goodsName,shopId,marketPrice,shopPrice,goodsCatId2,goodsClassId2,goodsSize,goodsWeight,goodsVedio,vedioThumb,goodsImg,goodsThumbs')->find();

        $goods['catName'] = M('goods_cats')->where('catId=' . $goods['goodsCatId2'])->getField('catName');
        $goods['className'] = M('goods_class')->where('classId=' . $goods['goodsClassId2'])->getField('className');
        $goods['thumb']['goodsImg'] = $goods['goodsImg'];
        $goods['thumb']['goodsThumbs'] = $goods['goodsThumbs'];
        $goods['pics'] = M('goods_gallerys')->where('goodsId=' . $goodsId)->field('id,goodsImg,goodsThumbs')->select();
        if (empty($goods['pics']))
            $goods['pics'] = array();
        $goods['goodsContent'] = M('goods_data')->where('goodsId=' . $goodsId)->getField('goodsContent');
        $goods['sourceId'] = M('goods_resale')->where('goodsId=' . $goodsId)->getField('sourceId');
        unset($goods['goodsImg'],$goods['goodsThumbs']);

        return $goods;
    }

    /**
     * 删除商品相册图片

     */
    public function delGoodsPic() {
        $id = intval(I('id'));
        $m = M('goods_gallerys');
        $pic = $m->where('id = ' . $id)->find();
//        @unlink($pic['goodsImg']);
//        @unlink($pic['goodsThumbs']);
        $rs = $m->where('id = ' . $id)->delete();

        if ($rs == FALSE)
            return 0;
        return 1;
    }

    /**
     * 获取商品详细信息

     */
    public function getGoodsDetail() {
        $goodsId = I('goodsId');
        $goods = M('goods')->where('goodsId=' . $goodsId)->field('goodsId,goodsName,shopId,marketPrice,shopPrice,goodsStock,goodsCatId1,goodsCatId2,goodsClassId1,goodsClassId2,goodsSize,goodsWeight,isOffline,goodsVedio,vedioThumb')->find();
        $goods['pics'] = M('goods_gallerys')->where('goodsId=' . $goodsId)->field('shopId,goodsImg,goodsThumbs')->select();
        if (empty($goods['pics']))
            $goods['pics'] = array();
        $goods['goodsContent'] = M('goods_data')->where('goodsId=' . $goodsId)->getField('goodsContent');

        return $goods;
    }

    /**
     * 检测转售权限

     */
    public function checkResale($userId) {
        $shopId = I('shopId', 0);
        $goodsId = I('goodsId', 0);
        $rt = array();
        $rt['code'] = 0;
        $rt['info'] = 'Error!';

        //if ($shopId == 0) {
         //   $rt['info'] = 'shopId脰碌虏禄脮媒脠路!';
        //    return $rt;
       // }
        $seflShopInfo = M('shops')->where('userId=' . $userId)->find();
        $isSource = M('goods')->where('goodsId=' . $goodsId)->getField('isSource');
        $sourceGoodsId = $goodsId;
        if ($isSource == 0) {
            $goodsResaleNum = M('sys_configs')->where('fieldCode = "goodsResaleNum"')->getField('fieldValue');
            $resaleLineNum = M('sys_configs')->where('fieldCode = "resaleLineNum"')->getField('fieldValue');
            $resaleInfo = M('goods_resale')->where('goodsId=' . $goodsId)->find();
            if (count(explode(',', $resaleInfo['node'])) >= $resaleLineNum) {
                $rt['code'] = 0;
                $rt['info'] ='此宝贝无法再转售!';

                return $rt;
            }
            $sourceCount = M('goods_resale')->where('sourceId=' . $resaleInfo['sourceId'])->count();
            if ($sourceCount >= $goodsResaleNum) {
                $rt['code'] = 0;
                 $rt['info'] = '此宝贝无法再转售!';

                return $rt;
            }

            $sourceGoodsId = $resaleInfo['sourceId'];
        }

        $resaleExist = M('goods_resale')->where('sourceId=' . $goodsId . ' and shopId = ' . $seflShopInfo['shopId'])->count();
        if ($resaleExist > 0) {
            $rt['code'] = 0;
           $rt['info'] = '您已经转售过该产品!';

            return $rt;
        }

        if ($shopId == $seflShopInfo['shopId']) {
            $rt['code'] = 0;
            $rt['info'] = '无法转售自己的商品!';

            return $rt;
        }

        $shopInfo = M('shops')->where('shopId=' . $shopId)->find();
       // $isFriend = $this->isFriend($userId, $shopInfo['userId']);
       // if ($isFriend || 1) {   

            if ($shopInfo['openResale'] == 1 && 0 ) {
                $isResale = M('user_friends')->where('uid=' . $userId . ' and fuid=' . $shopInfo['userId'])->getField('isResale');
                if ($isResale == 0) {
                    $rt['code'] = 0;
                     $rt['info'] = '您无法转售该产品，请和商家联系开通转售权限!';
                    return $rt;
                }
                $rt['code'] = 1;
                $rt['info'] = 'Success!';
                return $rt;
            }
            $rt['code'] = 1;
            $rt['info'] = 'Success!';
            return $rt;
      //  } else {

       //     $rt['code'] = 0;
       //       $rt['info'] = '您无法转售该产品，请和商家联系开通转售权限!';

        //    return $rt;
       // }

    }

}