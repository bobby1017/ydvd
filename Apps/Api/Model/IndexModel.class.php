<?php

namespace Api\Model;

/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 */
class indexModel extends BaseModel {

    /**
     * 获取商品详细信息
     */
    public function getGoodsDetail($userInfo) {
        $goodsId = I('goodsId');
        $sql = "SELECT g.goodsId,g.goodsName,g.shopId,g.marketPrice,g.shopPrice,g.goodsStock,g.goodsCatId1,g.goodsCatId2,g.goodsClassId1,g.goodsClassId2,g.goodsSize,g.goodsWeight,g.isOffline,g.isResale,gc.goodsContent,g.goodsVedio,g.vedioThumb,g.isSelling,g.isDel,g.goodsStatus,g.goodsImg,g.goodsThumbs,g.goodsSn "
                . " ,s.shopName, s.shopImg, s.shopTel, u.userId, u.rongToken,u.userName,u.userPhoto,s.shopStatus "
                . " FROM __PREFIX__goods g, __PREFIX__shops s, __PREFIX__goods_data gc, __PREFIX__users u "
                . " WHERE g.goodsId = " . $goodsId . " AND g.shopId = s.shopId AND s.userId = u.userId AND g.goodsId = gc.goodsId ";
        $goods = $this->queryRow($sql);
        if (empty($goods))
            return 0;
        $goods['catName'] = $this->getCatName($goods['goodsCatId1'], $goods['goodsCatId2']);
        $goods['thumb']['goodsImg'] = $goods['goodsImg'];
        $goods['thumb']['goodsThumbs'] = $goods['goodsThumbs'];
        $goods['className'] = $this->getClassName($goods['goodsClassId1'], $goods['goodsClassId2']);
        $goods['pics'] = M('goods_gallerys')->where('goodsId=' . $goodsId)->field('id,goodsImg,goodsThumbs')->select();
        $goods['isFollow'] = $this->isGoodsFollow($goodsId, $userInfo['userId']);
        $goods['isFriend'] = $this->isFriend($userInfo['userId'], $goods['userId']);
        $goods['userType'] = $userInfo['userType'];
        $goods['wapUrl'] = C('WEB_URL') . U('Wap/Index/goodsDetail/id/' . $goodsId);
        if(empty($goods['pics'])) {
            $goods['pics'] = array();
        }

        return $goods;
    }

    /**
     * 商品评论
     * @return type
     */
    public function goodsComment() {
        $goodsId = I('goodsId');
        $num = I('num') ? I('num') : 5;
        $page = I('page') ? I('page') : 1;
        $current = $page;
        $page = ($page - 1) * $num;

        $sql = "SELECT sum(goodsScore) AS score, count( * ) AS count FROM __PREFIX__goods_appraises WHERE goodsId = " . $goodsId;
        $comment = $this->queryRow($sql);
        $comment['percent'] = round(($comment['score'] / ($comment['count'] * 5) * 100), 2) . '%';

        $sql = "select c.goodsScore,c.createTime,c.content,u.userName,u.userPhoto "
                . " from __PREFIX__goods_appraises as c left join __PREFIX__users as u on c.userId = u.userId "
                . " where c.isShow = 1 and c.goodsId = " . $goodsId
                . " order by id desc "
                . " limit " . $page . "," . $num;
        $comment['comment'] = $this->query($sql);
        $comment['goodsId'] = $goodsId;
        $comment['num'] = $num;
        $comment['current'] = $current;
        $comment['sumpage'] = ceil($comment['count'] / $num);

        return $comment;
    }

    /**
     * 获取款式|种质|价格区间数据
     */
    public function catsClassPrice() {
        $m = M('goods_cats');
        $catTree = $m->where('catFlag = 1 AND parentId = 0 AND isShow = 1')->order('catSort ASC')->field('catId, catName')->select();
        foreach ($catTree as $k => $v) {
            $child = $m->where('catFlag = 1 AND parentId = ' . $v['catId'] . ' AND isShow = 1')->order('catSort ASC')->field('catId, catName')->select();
            $catTree[$k]['child'] = $child;
        }
        $m = M('goods_class');
        $classTree = $m->where('classFlag = 1 AND parentId = 0 AND isShow = 1')->order('classSort ASC')->field('classId, className')->select();
        foreach ($classTree as $k => $v) {
            $child = $m->where('classFlag = 1 AND parentId = ' . $v['classId'] . ' AND isShow = 1')->order('classSort ASC')->field('classId, className')->select();
            $classTree[$k]['child'] = $child;
        }
        $price = array(
            array('name' => '价格区间', 'value' => 'all', 'child' => array(
                    array('name' => '500以下', 'value' => '0-500'),
                    array('name' => '501-1000', 'value' => '501-1000'),
                    array('name' => '1001-5000', 'value' => '1001-5000'),
                    array('name' => '5001-1万', 'value' => '5001-10000'),
                    array('name' => '1万-5万', 'value' => '10000-50000'),
                    array('name' => '5万以上', 'value' => '50000')
                )
            )
        );
        $data = array('catTree' => $catTree, 'classTree' => $classTree, 'price' => $price);

        return $data;
    }

    /**
     * 宝贝列表(搜索|筛选)
     */
    public function goodsList($userId) {
        $m = M('goods');
//        $userId = I('userId', 0);
        $catId = I('catId', '');
        $classId = I('classId', '');
        $price = I('price', '');
        $keyword = I('keyword', '');
        $num = I('num') ? I('num') : 10;
        $page = I('page') ? I('page') : 1;
        $page = ($page - 1) * $num;

        $shopIds = array_unique(array_merge($this->friendShopId($userId)));

        $where = '((isSale = 1 AND isDel = 0 AND goodsFlag = 1 AND goodsStatus = 1 AND isSource = 1 and isSelling  = 1 and goodsStock > 0 and (isAdminRecom = 1 or isAdminBest = 1)) or (isSale = 1 AND isDel = 0 AND goodsFlag = 1 AND goodsStatus = 1 AND isSource = 1 and isSelling  = 1 and goodsStock > 0 and shopId in (' . implode(',', $shopIds) . ')))';
        $count = $m->where($where)->count();
        if (!empty($catId)) {
            $catId = explode('-', $catId);
            $where .= ' AND goodsCatId' . $catId[1] . '=' . $catId[0];
        }
        if (!empty($classId)) {
            $classId = explode('-', $classId);
            $where .= ' AND goodsClassId' . $classId[1] . '=' . $classId[0];
        }
        if (!empty($price) && $price != 'all') {
            $classId = explode('-', $catId);
            if($price == '0-500') {
                $where .= ' AND shopPrice < 500';
            } elseif($price == '50000') {
                $where .= ' AND shopPrice > 50000';
            } else {
                $price = explode('-', $price);
                $where .= ' AND shopPrice > ' . $price[0] . ' AND shopPrice < ' . $price[1];
            }
        }
        if (preg_grep("/SN/Ui",$keyword)) {
            $where .= " AND goodsSn LIKE '%" . $keyword . "%'";
        } else if (!empty($keyword)) {
            $classId = explode('-', $catId);
            $where .= " AND goodsName LIKE '%" . $keyword . "%'";
        }
        error_log("IndexMode line 146 where=".$where."\r\n",3,LOG_PATH."/__debuger_query.log");

//        $where = ltrim($where, ' AND');
        $goodsList = $m->where($where)->field('goodsId,shopId,goodsName,shopPrice,goodsThumbs,goodsStock,goodsSn')->order('isAdminRecom DESC, isAdminBest DESC, goodsId DESC')->limit($page,$num)->select();

        return $goodsList;
    }

    public function oldgoodsList($userId) {
        $m = M('goods');
//        $userId = I('userId', 0);
        $catId = I('catId', '');
        $classId = I('classId', '');
        $price = I('price', '');
        $keyword = I('keyword', '');
        $num = I('num') ? I('num') : 10;
        $page = I('page') ? I('page') : 1;
        $page = ($page - 1) * $num;

        $shopIds = array_unique(array_merge($this->friendShopId($userId), $this->recomShopId()));

        $where = '((isSale = 1 AND isDel = 0 AND goodsFlag = 1 AND goodsStatus = 1 AND isSource = 1 and isSelling  = 1 and goodsStock > 0 and (isAdminRecom = 1 or isAdminBest = 1)) or (isSale = 1 AND isDel = 0 AND goodsFlag = 1 AND goodsStatus = 1 AND isSource = 1 and isSelling  = 1 and goodsStock > 0 and shopId in (' . implode(',', $shopIds) . ')))';
        $count = $m->where($where)->count();
        if (!empty($catId)) {
            $catId = explode('-', $catId);
            $where .= ' AND goodsCatId' . $catId[1] . '=' . $catId[0];
        }
        if (!empty($classId)) {
            $classId = explode('-', $classId);
            $where .= ' AND goodsClassId' . $classId[1] . '=' . $classId[0];
        }
        if (!empty($price) && $price != 'all') {
            $classId = explode('-', $catId);
            if($price == '0-500') {
                $where .= ' AND shopPrice < 500';
            } elseif($price == '50000') {
                $where .= ' AND shopPrice > 50000';
            } else {
                $price = explode('-', $price);
                $where .= ' AND shopPrice > ' . $price[0] . ' AND shopPrice < ' . $price[1];
            }
        }
        if (!empty($keyword)) {
            $classId = explode('-', $catId);
            $where .= " AND goodsName LIKE '%" . $keyword . "%'";
        }

//        $where = ltrim($where, ' AND');
        $goodsList = $m->where($where)->field('goodsId,shopId,goodsName,shopPrice,goodsThumbs,goodsStock')->order('isAdminRecom DESC, isAdminBest DESC, goodsId DESC')->limit($page,$num)->select();

        return $goodsList;
    }

    /**
     * 全部宝贝列表(搜索|筛选)
     */
    public function goodsAllList($userId) {
        $m = M('goods');
//        $userId = I('userId', 0);
        $catId = I('catId', '');
        $classId = I('classId', '');
        $price = I('price', '');
        $keyword = I('keyword', '');
        $num = I('num') ? I('num') : 10;
        $page = I('page') ? I('page') : 1;
        $page = ($page - 1) * $num;

        $where = 'isSale = 1 AND isDel = 0 AND goodsFlag = 1 AND goodsStatus = 1 AND isSource = 1 and isSelling  = 1 and goodsStock > 0';
        $count = $m->where($where)->count();
        if (!empty($catId)) {
            $catId = explode('-', $catId);
            $where .= ' AND goodsCatId' . $catId[1] . '=' . $catId[0];
        }
        if (!empty($classId)) {
            $classId = explode('-', $classId);
            $where .= ' AND goodsClassId' . $classId[1] . '=' . $classId[0];
        }
        if (!empty($price) && $price != 'all') {
            $classId = explode('-', $catId);
            if($price == '0-500') {
                $where .= ' AND shopPrice < 500';
            } elseif($price == '50000') {
                $where .= ' AND shopPrice > 50000';
            } else {
                $price = explode('-', $price);
                $where .= ' AND shopPrice > ' . $price[0] . ' AND shopPrice < ' . $price[1];
            }
        }
         
        if (!empty($keyword)) {
            $classId = explode('-', $catId);
            $where .= " AND (goodsName LIKE '%" . trim($keyword) . "%' or goodsSn LIKE '%". trim($keyword)."%')";
        }
         error_log("IndexMode line 240 where=".$where."\r\n",3,dirname(__FILE__)."/__debuger_query.log");
         $where .=" and shopId=1 ";
//        $where = ltrim($where, ' AND');
        $goodsList = $m->where($where)->field('goodsId,shopId,goodsName,shopPrice,goodsThumbs,goodsStock,goodsSn')->order('isAdminRecom DESC, isAdminBest  ASC, goodsId DESC')->limit($page,$num)->select();

        return $goodsList;
    }

     /**
     * 宝贝列表(搜索|筛选)
     */
    public function goodsBestList($userId) {
        $m = M('goods');
//        $userId = I('userId', 0);
        $catId = I('catId', '');
        $classId = I('classId', '');
        $price = I('price', '');
        $keyword = I('keyword', '');
        $num = I('num') ? I('num') : 10;
        $page = I('page') ? I('page') : 1;
        $page = ($page - 1) * $num;

        $where = 'isSale = 1 AND isDel = 0 AND goodsFlag = 1 AND goodsStatus = 1 AND isSource = 1 and isSelling  = 1 and goodsStock > 0 and (isAdminRecom = 1 or isAdminBest = 1)';
        $count = $m->where($where)->count();
        if (!empty($catId)) {
            $catId = explode('-', $catId);
            $where .= ' AND goodsCatId' . $catId[1] . '=' . $catId[0];
        }
        if (!empty($classId)) {
            $classId = explode('-', $classId);
            $where .= ' AND goodsClassId' . $classId[1] . '=' . $classId[0];
        }
        if (!empty($price) && $price != 'all') {
            $classId = explode('-', $catId);
            if($price == '0-500') {
                $where .= ' AND shopPrice < 500';
            } elseif($price == '50000') {
                $where .= ' AND shopPrice > 50000';
            } else {
                $price = explode('-', $price);
                $where .= ' AND shopPrice > ' . $price[0] . ' AND shopPrice < ' . $price[1];
            }
        }
        if (!empty($keyword)) {
            $classId = explode('-', $catId);
            $where .= " AND goodsName LIKE '%" . $keyword . "%'";
        }

//        $where = ltrim($where, ' AND');
        $goodsList = $m->where($where)->field('goodsId,shopId,goodsName,shopPrice,goodsThumbs,goodsStock,goodsSn')->order('isAdminRecom DESC, isAdminBest  ASC, goodsId DESC')->limit($page,$num)->select();

        return $goodsList;
    }

    /**
     * 推荐商铺列表(搜索|筛选)
     */
    public function goodsBestShop() {
        $m = M('shops');
//        $userId = I('userId', 0);
        $num = I('num') ? I('num') : 10;
        $page = I('page') ? I('page') : 1;
        $page = ($page - 1) * $num;

        $where = ' isRecom = 1 and shopFlag = 1 and shopStatus = 1';

        $shopsList = $m->where($where)->field('shopId, shopSN, userId, shopName, shopTel, shopAddress,shopImg')->order('isRecom DESC, shopId DESC')->limit($page, $num)->select();

        return $shopsList;
    }

    /**
     * 文章展示
     * @return type
     */
    public function getArticle() {
        $articleId = I('articleId');
        $m = M('articles');
        $article = $m->where('articleId=' . $articleId)->field('articleId,articleTitle,articleContent,createTime')->find();
        $article['articleContent'] = htmlspecialchars_decode($article['articleContent']);
        $article['wapUrl'] = C('WEB_URL') . U('Wap/index/article/src/app/id/' . $article['articleId']);


        return $article;
    }

    /**
     * 首页广告
     */
    public function indexAds() {
        $m = M('ads');
        $time = date('Y-m-d');
        $ads = $m->where('adStartDate <= "' . $time . '" AND adEndDate > "' . $time . '"')->field('adId,adName,adURL,adURLId,adFile')->order('adSort asc, adId desc')->select();
        $data = array();
        foreach($ads as $v) {
            if($v['adURL'] == 'article') {
                $v['wapUrl'] = C('WEB_URL') . U('Wap/index/article/src/app/id/' . $v['adURLId']);
            } else {
                $v['wapUrl'] = '';
            }
            $data[] = $v;
        }

        return $data;
    }

    public function isGoodsFollow($goodsId, $userId) {
        $m = M('favorites');

        $rs = $m->where('userId=' . $userId . ' AND goodsId=' . $goodsId)->getField('id');

        if($rs > 0)
            return 1;
        return 0;
    }

    /**
     * 使用帮助
     */
    public function helpList() {
        $m = M('articles');
        $data = $m->where('catId=7')->field('articleId,articleTitle,articleImg,createTime')->select();
        if(empty($data))
            return;

        $help = array();
        foreach ($data as $v) {
            $v['url'] = C('WEB_URL') . U('Wap/index/article/src/app/id/' . $v['articleId']);
            $help[] = $v;
        }

        return $help;
    }

    public function getCatName($catId1, $catId2) {
        $m = M('goods_cats');
        $cat1 = $m->where('catId=' . $catId1)->getField('catName');
        $cat2 = $m->where('catId=' . $catId2)->getField('catName');

        return $cat1 . '-' . $cat2;
    }

    public function getClassName($classId1, $classId2) {
        $m = M('goods_class');
        $class1 = $m->where('classId=' . $classId1)->getField('className');
        $class2 = $m->where('classId=' . $classId2)->getField('className');

        return $class1 . '-' . $class2;
    }
}
