<?php

namespace Api\Model;

class PayModel extends BaseModel {
    /**
     * 余额日志
     */
    public function logBalance($userId, $price, $logType, $logContent = '') {
        $m = M('log_user_balance');
        $typeName = array(1 => '订单支付',2 => '充值',3 => '提现',4 => '退款',5 => '转售收益',6 => '销售收益');
        $data = array();
        $data['userId'] = $userId;
        $data['logContent'] = $logContent;
        $data['price'] = $price;
        $data['logType'] = $logType;
        $data['typeName'] = $typeName[$logType];
        $data['logTime'] = date('Y-m-d H:i:s');
        $rs = $m->add($data);
        return $rs;
    }
}