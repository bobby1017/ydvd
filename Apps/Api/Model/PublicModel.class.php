<?php

namespace Api\Model;

class PublicModel extends BaseModel {

    /**
     * 发送系统消息
     * @param type $userId
     * @param type $msg
     * @param type $type
     */
    public function sendSysMsg($userId, $msg, $type = 0, $isAll = 0) {
        $m = M('user_sys_msg');
        $push = D('Api/Push');

        if (is_array($userId)) {
            
            if ($isAll) {
                $all = array();
                foreach ($userId as $v) {
                    if ($v) {
                        $data = array();
                        $data['userId'] = $v['userId'];
                        $data['msg'] = $msg;
                        $data['createTime'] = date('Y-m-d H:i:s');
                        $data['type'] = $type;
                        $pushRs = $push->PushToken($msg  , $data['userId']);
 
                      
                    }
                }
                  
                  return  1 ;
            } else {
                $all = array();
                foreach ($userId as $v) {
                    if ($v) {
                        $data = array();
                        $data['userId'] = $v;
                        $data['msg'] = $msg;
                        $data['createTime'] = date('Y-m-d H:i:s');
                        $data['type'] = $type;
                        $pushRs = $push->PushToken( $msg  , $data['userId']);
                       
                        
                    }
                }
              
                return  1 ;
            }
             
        } else {
            $data = array();
            $data['userid'] = $userId;
            $data['msg'] = $msg;
            $data['createTime'] = date('Y-m-d H:i:s');
            $data['type'] = $type;
           // $rs = $m->add($data);
            $pushRs = $push->PushToken(  $msg  , $data['userId']);
            
            return 1;
        }
    }

}
