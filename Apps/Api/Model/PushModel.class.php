<?php

namespace Api\Model;
class PushModel extends BaseModel {
    protected $push;
    protected $Message;
    private $accessId;
    private $secretKey;
    public $title;

    function __construct() {
        parent::__construct();
        vendor('Push.XingeApp');
        $this->title = '玉商微店';
    }

    public function initPush($type = 'IOS') {
        if ($type == 'IOS') {
            $this->accessId = 2200174254;
            $this->secretKey = '9685919805c32274e224a8776788eff3';
            $this->push = new \XingeApp($this->accessId, $this->secretKey);
        } else {
            $this->accessId = 2100175756;
            $this->secretKey = '7868f5d6dcb683b07cdcb9d1025f5a66';
            $this->push = new \XingeApp($this->accessId, $this->secretKey);
        }
    }

    public function pushAllIOS($content) {
        $this->initPush();
        $mess = new \MessageIOS();
        $mess->setExpireTime(86400);
        $mess->setAlert($content);
        $mess->setBadge(1);
        $mess->setSound("beep.wav");
        $acceptTime = new \TimeInterval(0, 0, 23, 59);
        $mess->addAcceptTime($acceptTime);
        $aps = array(
            "sound" => "beep.wav",
            "alert" => $content,
            "badge" => 1,
            "content-available" => 1
        );
        $raw = array("aps" => $aps);
        $mess->setRaw(json_encode($raw));
        $rs = $this->push->PushAllDevices(0, $mess, 1);
        return $rs;
    }

    /**
     * 使用默认设置推送消息给单个ios版账户
     */
    public function PushAccountIos($content, $account) {
        $this->initPush();
        $mess = new \MessageIOS();
        $mess->setAlert($content);
        $ret = $this->push->PushSingleAccount(0, $account, $mess, 1);
        return $ret;
    }

    /**
     * 使用默认设置推送消息给单个ios设备
     */
    public function PushTokenIos($content, $token) {
        $this->initPush();
        $mess = new \MessageIOS();
        $mess->setAlert($content);
        $ret = $this->push->PushSingleDevice($token, $mess, 1);
        return $ret;
    }

    /**
     * 使用默认设置推送消息给所有设备android版
     */
    public function PushAllAndroid($content) {
        $this->initPush('Android');
        $mess = new \Message();
        $mess->setTitle($this->title);
        $mess->setContent($content);
        $mess->setType(1);
        $mess->setStyle(new \Style(0, 1, 1, 1, 0));
        $action = new \ClickAction();
        $action->setActionType(1);
        $mess->setAction($action);
        $ret = $this->push->PushAllDevices(0, $mess);
        return $ret;
    }

    /**
     * 使用默认设置推送消息给单个android设备
     */
    public function PushTokenAndroid($content, $token) {
        $this->initPush('Android');
        $mess = new \Message();
        $mess->setTitle($this->title);
        $mess->setContent($content);
        $mess->setType(1);
        $mess->setStyle(new \Style(0, 1, 1, 1, 0));
        $action = new \ClickAction();
        $action->setActionType(1);
        $mess->setAction($action);
        $ret = $this->push->PushSingleDevice($token, $mess);
        return $ret;
    }

    /**
     * 使用默认设置推送消息给单个android版账户
     */
    public function PushAccountAndroid($content, $account) {
        $this->initPush('Android');
        $mess = new \Message();
        $mess->setTitle($this->title);
        $mess->setContent($content);
        $mess->setType(1);
        $mess->setStyle(new \Style(0, 1, 1, 1, 0));
        $action = new \ClickAction();
        $action->setActionType(1);
        $mess->setAction($action);
        $ret = $this->push->PushSingleAccount(0, $account, $mess);
        return $ret;
    }

    /**
     * 通过TOKE推送消息
     * @param type $content
     * @param type $token
     * @param type $uid
     * @return type
     */
    public function PushToken($content, $uid, $is_sysmsg = 1) {
        $token = M('user_pushtoken')->where('userId = ' . $uid)->order('time DESC')->getField('pushToken');
	file_put_contents('tss.txt', print_r($token, true));
	file_put_contents('tsu.txt', print_r($uid, true));
        if ($token) {
            $rt_android = $this->PushTokenAndroid($content, $token);
            $rt_ios = $this->PushTokenIos($content, $token);
        } else {
            $rt_ios = 'no token';
            $rt_android = 'no token';
        }


        if($is_sysmsg) {
            $data = array();
            $data['msg'] = $content;
            $data['userId'] = $uid;
            $data['createTime'] = date('Y-m-d H:i:s');
            $rt = M('user_sys_msg')->add($data);
        }

	file_put_contents('ts.txt', print_r(array('msg_ios' => $rt_ios, 'adroid' => $rt_android), true));
        return array('msg_ios' => $rt_ios, 'adroid' => $rt_android);
    }

}
