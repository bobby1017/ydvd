<?php

namespace Api\Model;

class ShopOrderModel extends BaseModel {

    /**
     * 销售订单列表
     * @param type $userId
     * @return type
     */
    public function orderList($userId) {
        $orderStatus = I('orderStatus', -2);
        $num = I('num') ? I('num') : 8;
        $page = I('page') ? I('page') : 1;
        $page = ($page - 1) * $num;

        $shopInfo = M('shops')->where('userId=' . $userId)->field('shopId,userId,shopName')->find();
        if ($orderStatus == 2) {
            $statusSql = ' AND orderStatus IN (2,3,4,5,6,-3) ';
        } else {
            $statusSql = ' AND orderStatus = ' . $orderStatus;
        }
        $order = M('orders')->where('orderFlag = 1 ' . $statusSql . ' AND ShopId=' . $shopInfo['shopId'])->field('orderId,orderNo,totalMoney,postage,supPrice,supPostage,payType,buyway,(totalMoney+postage+supPrice+supPostage) as priceSum,expressId,expressNo,isSource,orderStatus')->order('orderId DESC')->limit($page, $num)->select();

        if (empty($order))
            return $order;
        $data = array();
        foreach ($order as $v) {
            $v['expressName'] = M('express')->where('expressId=' . $v['expressId'])->getField('expressName');
            if (empty($v['expressName'])) {
                $v['expressName'] = '';
            }
            $v['goods'] = M('order_goods')->where('orderId=' . $v['orderId'])->select();
            $data[] = $v;
        }

        return $data;
    }

     /**
     * 订单详情
     * @return type
     */
    public function orderInfo($userId) {
        $m = M('orders');
        $orderId = I('orderId', 0);

        $order = $m->where('orderId=' . $orderId)->field('orderId,orderNo,orderStatus,shopId,totalMoney,postage,supPrice,supPostage,payType,buyway,userName,userAddress,userPhone,storeId,expressId,expressNo,createTime,isSource')->find();
        if (empty($order))
            return $order;
        if ($userId != $order['userId'] && ($order['isSource'] = 0 || $order['buyway'] == 3)) {
            $order['userName'] = M('sys_configs')->where('fieldCode = "yuduUserName"')->getField('fieldValue');
            $order['userAddress'] = M('sys_configs')->where('fieldCode = "yuduAddress"')->getField('fieldValue');
            $order['userPhone'] = M('sys_configs')->where('fieldCode = "yuduPhone"')->getField('fieldValue');
        }
        $sql = 'select u.userId,u.userName,u.userPhoto,s.shopStatus from __PREFIX__shops as s left join __PREFIX__users as u on s.userId = u.userId where s.shopId = ' . $order['shopId'];
        $order['shopInfo'] = $this->queryRow($sql);
        $order['priceSum'] = $order['totalMoney'] + $order['postage'] + $order['supPrice'] + $order['supPostage'];
        $order['payTypeName'] = M('payments')->where('id=' . $order['payType'])->getField('payName');
        $order['buywayName'] = M('buyway')->where('id=' . $order['buyway'])->getField('buyName');
        $order['goods'] = M('order_goods')->where('orderId = ' . $orderId)->select();
        $order['expressName'] = M('express')->where('expressId=' . $order['expressId'])->getField('expressName');
        if (empty($order['expressName']))
            $order['expressName'] = '';

        if ($order['buyway'] == 2) {
            $order['linestore'] = M('linestore')->where('storeId=' . $order['storeId'])->field('storeId,storeName,storeAddress,province,city,district,storeTel')->find();
            if (empty($order['linestore']))
                $order['linestore'] = array();
        }

        return $order;
    }

    /**
     * 商家发货填写单号
     * @return type
     */
    public function deliverGoods() {
        $m = M('orders');
        $orderId = I('orderId', 0);
        $expressId = I('expressId', 0);
        $expressNo = I('expressNo', '');

        if ($orderId == 0 || $expressId == 0 || $expressNo == '')
            return FALSE;

        $orderNo = $m->where('orderId = ' . $orderId)->getField('orderNo');
        $m->orderStatus = 1;
        $m->expressId = $expressId;
        $m->expressNo = $expressNo;
        $rs = $m->where('orderNo=' . $orderNo)->save();

        return $rs;
    }

    /**
     * 订单统计
     * @param type $userId
     * @return type
     */
    public function orderCount($userId) {
        $m = M('orders');
        $shopId = M('shops')->where('userId=' . $userId)->getField('shopId');
        $where = ' orderFlag = 1 and orderStatus >= 0  and shopId = ' . $shopId;
        $startTime = I('startTime') ? I('startTime') : date("Y-m-d", strtotime("-15 day"));
        $endTime = I('endTime') ? I('endTime') : date("Y-m-d");
        $where .= " and createTime >= '" . $startTime . "'";
        $where .= " and createTime <= '" . $endTime . "'";

        $sql = "SELECT date_format(`createTime`, '%Y-%m-%d') AS time, count( * ) AS count FROM __PREFIX__orders WHERE " . $where . " GROUP BY Time ORDER BY createTime DESC";
        $data = $this->query($sql);

        return $data;
    }

    /**
     * 金额统计
     * @param type $userId
     * @return type
     */
    public function priceCount($userId) {
        $m = M('orders');
        $shopId = M('shops')->where('userId=' . $userId)->getField('shopId');
        $where = ' orderFlag = 1 and orderStatus = 6 and shopId = ' . $shopId;
//        $where = ' orderFlag = 1 ';
        $startTime = I('startTime') ? I('startTime') : date("Y-m-d", strtotime("-15 day"));
        $endTime = I('endTime') ? I('endTime') : date("Y-m-d");
        $where .= " and createTime > '" . $startTime . "'";
        $where .= " and createTime < '" . $endTime . "'";

        $sql = "SELECT date_format(`createTime`, '%Y-%m-%d') AS time, sum( `totalMoney` ) AS totalMoney FROM __PREFIX__orders WHERE " . $where . " GROUP BY Time ORDER BY createTime DESC";
        $data = $this->query($sql);
        return $data;
    }

    /**
     * 访客统计
     * @param type $userId
     * @return type
     */
    public function visitorCount($userId) {
        $m = M('log_shop_hits');
        $shopId = M('shops')->where('userId=' . $userId)->getField('shopId');

        $where = 'shopId=' . $shopId;
        $startTime = I('startTime') ? I('startTime') : date("Y-m-d", strtotime("-15 day"));
        $endTime = I('endTime') ? I('endTime') : date("Y-m-d");
        $where .= " and createTime > '" . $startTime . "'";
        $where .= " and createTime < '" . $endTime . "'";

        $data = $m->where($where)->group('createTime')->order('createTime DESC')->field('createTime as time, count("hits") as count')->select();
        return $data;
    }

    /**
     * 收藏统计
     * @param type $userId
     * @return type
     */
    public function followCount($userId) {
        $m = M('favorites');

        $shopId = M('shops')->where('userId=' . $userId)->getField('shopId');
        $startTime = I('startTime') ? I('startTime') : date("Y-m-d", strtotime("-15 day"));
        $endTime = I('endTime') ? I('endTime') : date("Y-m-d");
        $where = 'shopId=' . $shopId;
        $where .= " and createTime >= '" . $startTime . "'";
        $where .= " and createTime <= '" . $endTime . "'";

        $data = $m->where($where)->group('createTime')->order('time DESC')->field('createTime AS time, count("sum") as count')->select();
        return $data;
    }

}
