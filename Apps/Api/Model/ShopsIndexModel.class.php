<?php

namespace Api\Model;
class ShopsIndexModel extends BaseModel {

    /**
     * 店铺首页新品宝贝
     */
    public function newGoodsList() {
        $m = M('goods');
        $shopId = I('shopId');
        $rs = $m->where('shopId=' . $shopId . ' and isSale = 1 and isDel = 0 and goodsFlag = 1 and goodsStatus = 1 and isSelling = 1 and goodsStock > 0 and isSource = 1')->field('goodsId,goodsName,goodsName,marketPrice, shopPrice,goodsSize,goodsWeight,goodsStatus,goodsThumbs,DATE_FORMAT(createTime, "%m月%d日") time, isSource')->order('goodsId DESC')->limit(30)->select();
        error_log("\r\n\r\n".date("Y-m-d H:i:s")." Api\Model\ShopsIndexModel.class.php line 13 ".$m->getLastSql(),3,LOG_PATH.'logs_'.date("Y-m-d").".log");
        $goodsList = array();
        foreach ($rs as $v) {
            $goodsList[$v['time']][] = $v;
        }
        $list = array();
        foreach ($goodsList as $k => $v) {
            $temp = array();
            $temp['time'] = $k;
            $temp['data'] = $v;
            $list[] = $temp;
        }

        return $list;
    }

    /**
     * 店铺首页在售宝贝
     */
    public function saleGoodsList() {
        $m = M('goods');
        $shopId = I('shopId');
        $num = I('num') ? I('num') : 5;
        $page = I('page') ? I('page') : 1;
        $page = ($page - 1) * $num;
        $goodsList = $m->where('shopId=' . $shopId . ' and isSale = 1 and isDel = 0 and goodsFlag = 1 and goodsStatus = 1 and isSelling = 1 and goodsStock > 0 and isSource = 1')->field('goodsId,goodsName,goodsName,marketPrice, shopPrice,goodsSize,goodsWeight,goodsStatus,goodsThumbs,DATE_FORMAT(createTime, "%m月%d日") time, isSource')->order('goodsId DESC')->limit($page, $num)->select();

        return $goodsList;
    }

    /**
     * 店铺首页已售宝贝
     */
    public function soldGoodsList() {
        $m = M('goods');
        $shopId = I('shopId');
        $num = I('num') ? I('num') : 5;
        $page = I('page') ? I('page') : 1;
        $page = ($page - 1) * $num;

//        $sql = "select g.goodsId,g.goodsName,g.goodsName,g.marketPrice, g.shopPrice,g.goodsSize,g.goodsWeight,g.goodsStatus,g.goodsThumbs,DATE_FORMAT(g.createTime, '%m月%d日') time, g.isSource "
//                . " from __PREFIX__goods g, __PREFIX__order_goods og "
//                . " where g.goodsId = og.goodsId and g.isSale = 1 and g.isDel = 0 and g.goodsFlag = 1 and g.shopId =" . $shopId
//                . " order by goodsId desc"
//                . " limit " . $page . "," . $num;
//        $goodsList = $this->query($sql);

        $goodsList = $m->where('shopId=' . $shopId . ' and isSource = 1 and ((isSale = 1 and isDel = 0 and goodsFlag = 1 and goodsStatus = 1 and isSelling = 0) or goodsStock = 0)')->field('goodsId,goodsName,goodsName,marketPrice, shopPrice,goodsSize,goodsWeight,goodsStatus,goodsThumbs,DATE_FORMAT(createTime, "%m月%d日") time, isSource,goodsStock,isSelling')->group('goodsId')->order('goodsId DESC')->limit($page, $num)->select();

        return $goodsList;
    }

    /**
     * 店铺基本信息
     * @return type
     */
    public function shopInfo($userId) {
         error_log("\r\n\r\n".date("Y-m-d H:i:s")." Api\Model\ShopsIndexModel.class.php line 70 ".json_encode($_REQUEST),3,LOG_PATH.'logs_'.date("Y-m-d").".log");
        $m = M('shops');
        $shopId = I('shopId');
        $shop = $m->where('shopId=' . $shopId)->field('shopId,(shopId+100000) shopSn,userId,shopName,shopImg,shopTel,shopAddress,province,city,district,depositId,depositMoney,listStyle,templateId,shopStatus')->find();
        error_log("\r\n\r\n".date("Y-m-d H:i:s")." Api\Model\ShopsIndexModel.class.php line 74 ".$m->getLastSql(),3,LOG_PATH.'logs_'.date("Y-m-d").".log");
        $shopUser = M('users')->where('userId=' . $shop['userId'])->find();
        error_log("\r\n\r\n".date("Y-m-d H:i:s")." Api\Model\ShopsIndexModel.class.php line 76 ".M('users')->getLastSql(),3,LOG_PATH.'logs_'.date("Y-m-d").".log");
        $shop['userName'] = $shopUser['userName'];
        $shop['userPhoto'] = $shopUser['userPhoto'];
        if ($shop['depositId'] > 0) {
            $shop['deposit'] = M('shops_deposit')->where('id=' . $shop['depositId'])->field('level,icon,title')->find();
            error_log("\r\n\r\n".date("Y-m-d H:i:s")." Api\Model\ShopsIndexModel.class.php line 81 ".M('shops_deposit')->getLastSql(),3,LOG_PATH.'logs_'.date("Y-m-d").".log");
        } else {
            $shop['deposit'] = null;
            
        }
        $shop['shopUrl'] = C('WEB_URL') . U('Wap/shopsIndex/newGoodsList/shopId/' . $shop['shopId']);
        $shop['isFriend'] = $this->isFriend($userId, $shop['userId']);
        $shop['isFollow'] = $this->isFollow($userId, $shopId);
//        $shop['goods'] = M('goods_gallerys')->where('shopId=' . $shopId)->order('id DESC')->limit(4)->select();
//        $shop['goods'] = M('goods')->where('shopId=' . $shopId . ' and goodsStatus = 1 and isSale = 1 and isDel = 0 and goodsFlag = 1 and goodsStock > 0 and createTime > "' . date("Y-m-d H:i:s", strtotime("-32 day")) . '"')->field('goodsId,goodsName,goodsThumbs')->order('goodsId DESC')->limit(4)->select();
        $shop['goods'] = M('goods')->where('shopId=' . $shopId . ' and goodsStatus = 1 and isSale = 1 and isDel = 0 and goodsFlag = 1 and goodsStock > 0 and isSource = 1')->field('goodsId,goodsName,goodsThumbs')->order('goodsId DESC')->limit(4)->select();
        if (empty($shop['goods'])) {
            $shop['goods'] = array();
        }
        //  $shop['newSum']  新品
        $newSum = M('goods')->where('shopId=' . $shopId . ' and isSource = 1 and goodsStatus = 1 and isSale = 1 and isSelling = 1 and isDel = 0 and goodsFlag = 1 and goodsStock > 0 and createTime > "' . date("Y-m-d H:i:s", strtotime("-32 day")) . '"')->limit(30)->select();
        $shop['newSum'] = count($newSum);
       
        error_log("\r\n\r\n".date("Y-m-d H:i:s")." Api\Model\ShopsIndexModel.class.php line 97 ".M('goods')->getLastSql(),3,LOG_PATH.'logs_'.date("Y-m-d").".log");

        $shop['saleSum'] = M('goods')->where('shopId=' . $shopId . ' and isSource = 1 and isSale = 1 and isDel = 0 and goodsFlag = 1 and goodsStatus = 1 and isSelling = 1 and goodsStock > 0')->count();
        error_log("\r\n\r\n".date("Y-m-d H:i:s")." Api\Model\ShopsIndexModel.class.php line 100 ".M('goods')->getLastSql(),3,LOG_PATH.'logs_'.date("Y-m-d").".log");
        $shop['soldSum'] = M('goods')->where('shopId=' . $shopId . ' and isSource = 1 and ((isSale = 1 and isDel = 0 and goodsFlag = 1 and goodsStatus = 1 and isSelling = 0) or goodsStock = 0)')->count();
        error_log("\r\n\r\n".date("Y-m-d H:i:s")." Api\Model\ShopsIndexModel.class.php line 102 ".M('goods')->getLastSql(),3,LOG_PATH.'logs_'.date("Y-m-d").".log");

        $this->logShopHits($shopId, $userId);
        error_log("\r\n\r\n".date("Y-m-d H:i:s")." Api\Model\ShopsIndexModel.class.php line 105 ".json_encode($shop),3,LOG_PATH.'logs_'.date("Y-m-d").".log");  
        return $shop;
    }

}