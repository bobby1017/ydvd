<?php

namespace Api\Model;

class ZoneModel extends BaseModel {

    /**
     * 添加好友
     */
    public function addFriend($uid) {
        $m = M('user_friends');
        $fuid = I('fuid');

        if ($fuid > 0) {
            $rs = $m->where('uid=' . $uid . ' AND fuid=' . $fuid)->count();
            if ($rs > 0)
                return -1;
            $m->uid = $uid;
            $m->fuid = $fuid;
            $m->createtime = time();
            $rs = $m->add();
            D('Api/Push')->PushToken('你有新的好友请求添加', $fuid);

            return $rs;
        }
        return FALSE;
    }

    /**
     * 新的好友
     */
    public function newFriends($userId) {
        $sql = 'SELECT f.id,u.userName,u.userId,u.rongToken,ifnull(u.userPhoto,"") userPhoto,ifnull(s.shopName, "") shopName,ifnull(s.shopId, 0) shopId,ifnull((s.shopId + 100000), 0) shopSn, ifnull(s.shopImg,"") shopImg,ifnull(s.shopStatus,0) as shopStatus '
                . ' FROM __PREFIX__user_friends as f '
                . ' LEFT JOIN __PREFIX__users as u ON f.uid = u.userId '
                . ' LEFT JOIN __PREFIX__shops as s ON f.uid = s.userId '
                . ' WHERE f.type = 0 AND f.fuid = ' . $userId;
        $data = $this->query($sql);

        return $data;
    }

    /**
     * 新的好友数量
     */
    public function newFriendsSum($userId) {
        $sql = 'SELECT count(*) as sum '
                . ' FROM __PREFIX__user_friends as f '
                . ' LEFT JOIN __PREFIX__users as u ON f.uid = u.userId '
                . ' LEFT JOIN __PREFIX__shops as s ON f.uid = s.userId '
                . ' WHERE f.type = 0 AND f.fuid = ' . $userId;
        $data = $this->query($sql);

        return $data;
    }

    /**
     * 忽略好友
     */
    public function ignoreFriend() {
        $m = M('user_friends');
        $id = I('id');
        $rs = $m->where('id=' . $id)->delete();

        return $rs;
    }

    /**
     * 同意好友
     */
    public function agreeFriend() {
        $m = M('user_friends');
        $id = I('id');
        $friend = $m->where('id=' . $id)->find();
        $isfriend = $m->where('uid=' . $friend['fuid'] . ' AND fuid=' . $friend['uid'])->getField('id');
        if ($isfriend > 0) {
            $rs = $m->where('id=' . $isfriend)->save(array('type' => 2));
        } else {
            $rs = $m->add(array(
                'uid' => $friend['fuid'],
                'fuid' => $friend['uid'],
                'type' => 2,
                'createtime' => time(),
            ));
        }


        $rs = $m->where('id=' . $id)->save(array('type' => 2));

        return $rs;
    }

     /**
     * 删除好友
     */
    public function delFriend() {
        $m = M('user_friends');
        $id = I('id');
        $friend = $m->where('id=' . $id)->find();
        if(empty($friend))
            return FALSE;

        $rs = $m->where('uid=' . $friend['uid'] . ' AND fuid=' . $friend['fuid'])->delete();
        $rs = $m->where('uid=' . $friend['fuid'] . ' AND fuid=' . $friend['uid'])->delete();


        return $rs;
    }

    /**
     * 好友列表
     */
    public function friends($userId) {
        $sql = 'SELECT f.id,u.userName,u.userId,u.rongToken,ifnull(u.userPhoto,"") userPhoto,ifnull(s.shopName,"") shopName,ifnull(s.shopId, 0) shopId,ifnull((s.shopId + 100000), 0) shopSn, ifnull(s.shopImg,"") shopImg,ifnull(s.shopStatus,0) as shopStatus '
                . ' FROM __PREFIX__user_friends as f '
                . ' LEFT JOIN __PREFIX__users as u ON f.uid = u.userId '
                . ' LEFT JOIN __PREFIX__shops as s ON f.uid = s.userId '
                . ' WHERE s.shopStatus = 1 and f.type = 2 AND f.fuid = ' . $userId;
        $data = $this->query($sql);

        return $data;
    }

    /**
     * 搜索店铺
     */
    public function searchShop() {
        $m = M('shops');
        $keyword = I('keyword');
        if(empty($keyword))
            return;

        $num = I('num') ? I('num') : 2;
        $page = I('page') ? I('page') : 1;
        $page = ($page - 1) * $num;

        $where = 'shopFlag = 1 AND shopStatus = 1 ';
        if(intval($keyword) > 0) {
            $keyword = intval($keyword) - 100000;
            $where .= ' AND shopId = ' . $keyword;
        } else {
            $where .= ' AND shopName LIKE "%' . $keyword . '%"';
        }
        $data = $m->where($where)->field('shopId,ifnull((shopId + 100000), 0) shopSn,userId,shopName,ifnull(shopImg,"") shopImg, shopStatus')->order('shopId DESC')->limit($page, $num)->select();

        return $data;
    }

    /**
     * 附近店铺
     */
    public function nearbyShop() {
        $lon = I('lon');
        $lat = I('lat');

        $num = I('num') ? I('num') : 200;
        $page = I('page') ? I('page') : 1;
        $page = ($page - 1) * $num;

        $sql = "SELECT "
                . " shopId,ifnull((shopId + 100000), 0) shopSn,userId,ifnull(shopName,'') shopName,ifnull(shopImg,'') shopImg, "
                . " sqrt((((" . $lon . "-longitude)*PI()*12656*cos(((" . $lat . "+latitude)/2)*PI()/180)/180) * ((" . $lon . "-longitude)*PI()*12656*cos (((" . $lat . "+latitude)/2)*PI()/180)/180) ) + ( ((" . $lat . "-latitude)*PI()*12656/180) * ((" . $lat . "-latitude)*PI()*12656/180))) AS distance "
                . " FROM __PREFIX__shops "
                . " WHERE shopFlag = 1 AND shopStatus = 1 AND `longitude` > 0 AND `latitude` > 0 "
                . " AND sqrt((((" . $lon . "-longitude)*PI()*12656*cos(((" . $lat . "+latitude)/2)*PI()/180)/180) * ((" . $lon . "-longitude)*PI()*12656*cos (((" . $lat . "+latitude)/2)*PI()/180)/180) ) + ( ((" . $lat . "-latitude)*PI()*12656/180) * ((" . $lat . "-latitude)*PI()*12656/180))) < 20 "
                . " ORDER BY distance ASC "
                . " LIMIT " . $page . "," . $num;
        $shop = $this->query($sql);

        $data = array();
        foreach($shop as $v) {
            $v['distance'] = round($v['distance'], 2) . '公里';
            $data[] = $v;
        }
        unset($shop);

        return $data;
    }

    /**
     * 搜索好友
     */
    public function searchFriends($userId) {
        $keyword = I('keyword');
        $where = '';
        if(intval($keyword) > 0) { //输入的关键词为数字
            $keyword = intval($keyword) - 100000;
            $where .= ' AND shopId = ' . $keyword;
        } else {
            $where .= ' AND shopName LIKE "%' . $keyword . '%"';
        }
        $sql = 'SELECT f.id,u.userName,u.userId,u.rongToken,ifnull(u.userPhoto,"") userPhoto,ifnull(s.shopName,"") shopName,ifnull(s.shopId, 0) shopId,ifnull((s.shopId + 100000), 0) shopSn, ifnull(s.shopImg,"") shopImg,s.shopStatus '
                . ' FROM __PREFIX__user_friends as f '
                . ' LEFT JOIN __PREFIX__users as u ON f.uid = u.userId '
                . ' LEFT JOIN __PREFIX__shops as s ON f.uid = s.userId '
                . ' WHERE s.shopStatus = 1 and f.type = 2 AND f.fuid = ' . $userId . $where;
        $data = $this->query($sql);

        return $data;
    }

     /**
     * 记录聊天好友
     */
    public function logImStatus($userId) {
        $fuid = I('fuid', 0);
        if($fuid > 0) {
            $m = M('log_user_imstatus');
            $ulogId = $m->where('uid=' . $userId . ' and fuid=' . $fuid)->getField('logId');
            $flogId = $m->where('uid=' . $fuid . ' and fuid=' . $userId)->getField('logId');
            if($ulogId > 0) {
                $m->logTime = date('Y-m-d H:i:s');
                $rs = $m->where('logId=' . $ulogId)->save();

            } else {
                $m->uid = $userId;
                $m->fuid = $fuid;
                $m->logTime = date('Y-m-d H:i:s');
                $rs = $m->add();
            }
            if($flogId > 0) {
                $m->logTime = date('Y-m-d H:i:s');
                $rs = $m->where('logId=' . $flogId)->save();
            } else {
                $m->uid = $fuid;
                $m->fuid = $userId;
                $m->logTime = date('Y-m-d H:i:s');
                $rs = $m->add();
            }
            return TRUE;
        } else {
            return FALSE;
        }
    }


    /**
     * 聊天好友列表
     */
    public function getLogImlist($userId) {
        $sql = 'select l.logId,l.logTime,u.userId,ifnull(u.userName,"") userName,ifnull(u.userPhoto,"") userPhoto,s.shopId,s.shopStatus   '
                . ' from __PREFIX__log_user_imstatus as l left join __PREFIX__users as u on l.fuid = u.userId '
                . ' left join __PREFIX__shops as s on l.fuid = s.userId '
                . ' where l.uid = ' . $userId
                . ' order by l.logTime desc';
        $data = $this->query($sql);

        return $data;
    }
}
