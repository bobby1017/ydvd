<?php
namespace Home\Model;
/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 * 友情连接服务类
 */
class FriendlinksModel extends BaseModel {
	/**
     * 获取友情链接
     */
	public function getFriendlinks(){
		return $this->cache('MXC_CACHE_FRIENDLINK_000',areaId3,31536000)->order('friendlinkSort asc')->select();
	}
}