<?php

namespace Wap\Action;

class AlipayAction extends BaseAction {

    public function notify() {
        $alipay_config = C("alipay_config");
        Vendor('Alipay.lib.alipay_notify');

        $alipayNotify = new \AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyNotify();

        if ($verify_result) {
            $out_trade_no = $_POST['out_trade_no'];
            //支付宝交易号
            $trade_no = $_POST['trade_no'];
            //交易状态
            $trade_status = $_POST['trade_status'];

            if ($_POST['trade_status'] == 'TRADE_FINISHED') {
                log_txt($_POST, 'log/log_alipay/TRADE_FINISHED-' . $out_trade_no . '.txt');
                //商户订单号
                $obj = array();
                $obj["out_trade_no"] = $_POST['out_trade_no'];
                $obj["total_fee"] = $_POST['total_fee'];

                //支付成功业务逻辑
                $length = strlen($obj["out_trade_no"]);
                if ($length == 8) {
                    $pay = D('Wap/pay');
                    $rs = $pay->cashDepositPay($obj);
                } else {
                    $od = D('Wap/Order');
                    $rs = $od->complatePay($obj);
                }
            } else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
                log_txt($_POST, 'log/log_alipay/TRADE_SUCCESS-' . $out_trade_no . '.txt');
                //商户订单号
                $obj = array();
                $obj["out_trade_no"] = $_POST['out_trade_no'];
                $obj["total_fee"] = $_POST['total_fee'];
                //支付成功业务逻辑
                $length = strlen($obj["out_trade_no"]);
                if ($length == 8) {
                    $pay = D('Wap/pay');
                    $rs = $pay->cashDepositPay($obj);
                } else {
                    $od = D('Wap/Order');
                    $rs = $od->complatePay($obj);
                }
            }
            echo "success";
        } else {
            //验证失败
            echo "fail";

            //调试用，写文本函数记录程序运行情况是否正常
            //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
        }
    }

    public function return_url() {
        $alipay_config = C("alipay_config");
        Vendor('Alipay.lib.alipay_notify');

        $alipayNotify = new \AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyReturn();
        if ($verify_result) {
            //商户订单号
            $out_trade_no = $_GET['out_trade_no'];

            //支付宝交易号
            $trade_no = $_GET['trade_no'];

            //交易状态
            $trade_status = $_GET['trade_status'];

            if ($_GET['trade_status'] == 'TRADE_FINISHED' || $_GET['trade_status'] == 'TRADE_SUCCESS') {
                redirect(U('Wap/Order/wapToPay') . '?orderSN=' . $out_trade_no);
            } else {
                redirect(U('Wap/Order/wapToPay') . '?orderSN=' . $out_trade_no);
            }

            redirect(U('Wap/Order/wapToPay') . '?orderSN=' . $out_trade_no);
        } else {
            //验证失败
            //如要调试，请看alipay_notify.php页面的verifyReturn函数
            redirect(U('Wap/Order/wapToPay') . '?orderSN=' . $out_trade_no);
        }
    }

}
