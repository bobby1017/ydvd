<?php

namespace Wap\Action;

class CartAction extends BaseAction {

    /**
     * 添加商品到购物车
     */
    public function addToCart() {
        $userInfo = $this->isUserLogin();
        if (I('goodsId', 0) > 0) {
            session('login_goodsId', I('goodsId', 0));
        }
        $m = D('Wap/Cart');
        $rs = $m->addToWapCart($userInfo['userId']);

        $this->restApi($rs['code'], $rs['info'], $rs['data']);
    }

    /**
     * 购物车列表
     */
    public function index() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Cart');
        $data = $m->getCartList($userInfo['userId']);

        if ($data === FALSE)
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 修改购物车中的商品数量
     */
    public function changeCartGoodsNum() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Cart');
        $rs = $m->changeCartGoodsNum();

        $this->restApi($rs['code'], $rs['info']);
    }

    /**
     * 删除购物车中的商品
     */
    public function delCartGoods() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Cart');
        $rs = $m->delCartGoods();

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 清空购物车中的商品
     */
    public function cleanCartGoods() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Cart');
        $rs = $m->cleanCartGoods($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

}
