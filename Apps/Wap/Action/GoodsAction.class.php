<?php
namespace Wap\Action;
/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 * 商品控制器
 */
class GoodsAction extends BaseAction {
    /**
     * 上传宝贝
     */
    public function addGoods() {
        $userInfo = $this->checkLogin();
        $shopId = $this->checkAddGoodsAuth($userInfo);

        $m = D('Wap/Goods');
        $rs = $m->addGoods($shopId);
        $this->restApi($rs['code'], $rs['info']);
    }
    
    /**
     * 传售宝贝
     */
    public function resaleGoods() {
        $userInfo = $this->checkLogin();
        $shopId = $this->checkAddGoodsAuth($userInfo, 'resale');
        
        $m = D('Wap/Goods');
        $rs = $m->resaleGoods($shopId);
        $this->restApi($rs['code'], $rs['info']);
    }
    
    /**
     * 编辑宝贝
     */
    public function editGoods() {
        $userInfo = $this->checkLogin();
        $shopId = $this->checkAddGoodsAuth($userInfo);

        $m = D('Wap/Goods');
        $rs = $m->editGoods($shopId);
        $this->restApi($rs['code'], $rs['info']);
    }
    
    /**
     * 编辑宝贝
     */
    public function editResale() {
        $userInfo = $this->checkLogin();
        $shopId = $this->checkAddGoodsAuth($userInfo);

        $m = D('Wap/Goods');
        $rs = $m->editResale($shopId);
        $this->restApi($rs['code'], $rs['info']);
    }
    
    /**
     * 获取款式列表
     */
    public function getCatsTree() {
        $m = D('Wap/Goods');
        $catTree = $m->getCatsTree();
        
        if(empty($catTree))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $catTree);
    }
    
    /**
     * 获取种质列表
     */
    public function getClassTree() {
        $m = D('Wap/Goods');
        $classTree = $m->getClassTree();
        
        if(empty($classTree))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $classTree);
    }
    
    /**
     * 获取转售商品详情
     */
    public function getParentGoods() {
        $m = D('Wap/Goods');
        $goods = $m->getParentGoods();
        
        if(empty($goods))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $goods);
    }
    
    /**
     * 删除商品相册图片
     */
    public function delGoodsPic() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Goods');
        $rs = $m->delGoodsPic();
        if($rs == 0)
            $this->restApi($rs, 'Error!');
        $this->restApi($rs, 'Success!');
    }
    
    /**
     * 获取商品详细信息
     */
    public function getGoodsDetail() {
        $m = D('Wap/Goods');
        $goods = $m->getGoodsDetail();
        if(empty($goods))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $goods);
    }
}