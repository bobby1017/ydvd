<?php

namespace Wap\Action;

/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 */
class IndexAction extends BaseAction {

    public function index() {
        redirect(U('Users/login'));
    }

    /**
     * 获取商品详细信息
     */
    public function goodsDetail() {
        $userinfo = $this->isLogin();
        $m = D('Wap/Index');
        $goods = $m->getGoodsDetail($userinfo['userId']);

        $this->assign('goods', $goods);
        $this->assign('header_title', '商品详情');
        $this->display("default/goods/goodsDetail");
    }

    /**
     * 商品评论
     */
    public function goodsComment() {
        $m = D('Wap/Index');
        $comment = $m->goodsComment();

        $this->assign('comment', $comment);
//        $this->display("default/goods/comment_list");
        $html = $this->fetch("default/goods/comment_list");
        $this->ajaxReturn($html);
    }

    /**
     * 获取款式|种质|价格区间数据
     */
    public function catsClassPrice() {
        $m = D('Wap/Index');
        $data = $m->catsClassPrice();
        if (empty($data))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 宝贝列表(搜索|筛选)
     */
    public function goodsList() {
        $m = D('Wap/Index');
        $goodsList = $m->goodsList();
        if (empty($goodsList))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $goodsList);
    }

    /**
     * 文章展示
     */
    public function article() {
        $m = D('Wap/Index');
        $article = $m->getArticle();

        $this->assign('header_title', mb_substr($article['articleTitle'], 0, 10, 'utf8') . '…');
        $this->assign('src', I('src'));
        $this->assign('article', $article);
        $this->display('default/article/view');
    }

    /**
     * 首页广告
     */
    public function indexAds() {
        $m = D('Wap/Index');
        $ads = $m->indexAds();

        if (empty($ads))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $ads);

        cookie();
    }

    public function getAreasList() {
        $parentId = I('parentId', 0);
        $m = M('areas');
        $list = $m->where('areaFlag=1 and parentId=' . $parentId)->select();

        $rs = array();
        $rs['status'] = 1;
        $rs['list'] = $list;
        $this->ajaxReturn($rs);
    }

    /**
     * 使用帮助
     */
    public function helpList() {
        $m = D('Wap/Index');
        $helplist = $m->helpList();

        $this->assign('helplist', $helplist);
        $this->assign('header_title', '使用帮助');
        $this->display("default/article/helplist");
    }

}
