<?php

namespace Wap\Action;

class OrderAction extends BaseAction {

    /**
     * 订单结算
     */
    public function checkOrder() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Order');
        $order = $m->checkOrder($userInfo['userId']);

        $this->assign('order', $order['data']);
        $this->assign('cartId', I('cartId', 0));
        $areas = M('areas')->where('parentId = 0')->select();
        $this->assign('areas', $areas);
        $this->assign('user', $userInfo);
        $this->assign('header_title', '确认订单');
        $this->display('default/order/checkOrder');
    }

    /**
     * 订单支付
     */
    public function PayOrder() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Order');
        $rs = $m->PayOrder($userInfo['userId']);

        $this->restApi($rs['code'], $rs['info'], $rs['data']);
    }

    /**
     * 页面支付状态
     */
    public function wapToPay() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Order');
        $order = $m->wapToPay();
        $payType = I('payType', 0);
        if($payType > 0) {
            $order['payType'] = $payType;
        }

        if ($order['payType'] == 2 && $order['isPay'] == 0) {
            $alipay = D('Wap/Alipay');
            $alipay_url = $alipay->createAlipayUrl($order);
            echo $alipay_url;
            exit;
        }
        if($order['payType'] == 3 && $order['isPay'] == 0) {
            $jsApiParameters = $this->wxJsApiPay($order);
            $this->assign('jsApiParameters', $jsApiParameters);
        }

        $orderSN = I('orderSN', 0);
        $this->assign('orderSN', $orderSN);
        $this->assign('user', $userInfo);
        $this->assign('order', $order);
        $this->assign('alipay_url', $alipay_url);
        $this->assign('header_title', '支付结果');
        $this->display('default/order/wapToPay');
    }

    public function toAlipay() {
        $m = D('Wap/Order');
        $order = $m->wapToPay();

        $alipay = D('Wap/Alipay');
        $alipay_url = $alipay->createAlipayUrl($order);
        echo $alipay_url;
        exit;
    }

    /**
     * 门店列表
     */
    public function lineStore() {
        $m = D('Wap/Order');
        $data = $m->lineStore();

        if (empty($data))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

}
