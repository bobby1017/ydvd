<?php

namespace Wap\Action;

class PublicAction extends BaseAction {

    /**
     * 获取省市区
     */
    public function getAreas() {
        $m = M('areas');
        $parentId = I('parentId');
        $areas = $m->where(array('parentId' => $parentId))->field('areaId, areaName')->select();
        $html = '<option value="0">请选择</option>';
        foreach ($areas as $v) {
            $html .= '<option value="' . $v['areaId'] . '">' . $v['areaName'] . '</option>';
        }
        $this->restApi(1, 'Success!', $html);
    }

    /**
     * 单个上传图片
     */
    public function uploadPic() {
        set_time_limit(90);
        $dir = I('dir', 'uploads') ? I('dir', 'uploads') : 'uploads';
        $config = array(
            'maxSize' => 0, //上传的文件大小限制 (0-不做限制)
            'exts' => array('jpg', 'png', 'gif', 'jpeg'), //允许上传的文件后缀
            'rootPath' => './Upload/', //保存根路径
            'driver' => 'LOCAL', // 文件上传驱动
            'subName' => array('date', 'Y-m'),
            'savePath' => $dir . "/"
        );
        $upload = new \Think\Upload($config);
        $rs = $upload->upload($_FILES);
        $Filedata = key($_FILES);
        $width = I('width', 480) ? I('width', 480) : 480;
        $height = I('height', 480) ? I('height', 480) : 480;
        if (!$rs) {
            $this->restApi(0, $upload->getError());
//            echo json_encode($upload->getError());
        } else {
            $images = new \Think\Image();
            $images->open('./Upload/' . $rs[$Filedata]['savepath'] . $rs[$Filedata]['savename']);
            $newsavename = str_replace('.', '_thumb.', $rs[$Filedata]['savename']);
            $vv = $images->thumb(I('width', 480), I('height', 480))->save('./Upload/' . $rs[$Filedata]['savepath'] . $newsavename);
            $rs[$Filedata]['savepath'] = "Upload/" . $rs[$Filedata]['savepath'];
            $rs[$Filedata]['savethumbname'] = $newsavename;
            $rs['picPath'] = $rs['file']['savepath'] . $rs['file']['savename'];
            $rs['picThumbPath'] = $rs['file']['savepath'] . $rs['file']['savethumbname'];
            unset($rs['file']);
            $this->restApi(1, 'Success!', $rs);
//            echo json_encode($rs);
        }
    }

}
