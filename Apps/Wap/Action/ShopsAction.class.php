<?php

namespace Wap\Action;

class ShopsAction extends BaseAction {

    /**
     * 个人入驻申请
     */
    public function openPerShop() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Shops');
        $rs = $m->addPerShop($userInfo);

        if ($rs['code'] == 0) {
            $this->restApi(0, $rs['info']);
        } else {
            $this->restApi(1, $rs['info']);
        }
    }

    /**
     * 企业入驻申请
     */
    public function openCompShop() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Shops');
        $rs = $m->addCompShop($userInfo);

        if ($rs['code'] == 0) {
            $this->restApi(0, $rs['info']);
        } else {
            $this->restApi(1, $rs['info']);
        }
    }

    /**
     * 担保交易等级
     */
    public function deposit() {
        $userInfo = $this->checkLogin();

        $m = M('shops_deposit');
        $lists = $m->where(1)->order('level ASC')->select();
        if (empty($lists))
            $this->restApi(0, 'Error!', $lists);
        $this->restApi(1, 'Success!', $lists);
    }

    /**
     * 更新店铺保证金等级
     */
    public function upShopDeposit() {
        $userInfo = $this->checkLogin();

        $m = M('shops');
        $shopId = $m->where('userId=' . $userInfo['userId'])->getField('shopId');
        if ($shopId > 0) {
            $rs = $m->where('shopId=' . $shopId)->save(array('depositId' => I('depositId')));
            if ($rs === FALSE)
                $this->restApi(0, 'Error!', $rs);
            $this->restApi(1, 'Success!', $rs);
        }

        $this->restApi(0, '您还没有开通微店!');
    }

    /**
     * 更新店铺邮费
     */
    public function upShopPostage() {
        $userInfo = $this->checkLogin();

        $m = M('shops');
        $shopId = $m->where('userId=' . $userInfo['userId'])->getField('shopId');
        if ($shopId > 0) {
            $rs = $m->where('shopId=' . $shopId)->save(array('postage' => I('postage')));
            if ($rs === FALSE)
                $this->restApi(0, 'Error!', $rs);
            $this->restApi(1, 'Success!', $rs);
        }

        $this->restApi(0, '您还没有开通微店!');
    }

    /**
     * 更新店铺名称
     */
    public function upShopName() {
        $userInfo = $this->checkLogin();
        $shopId = I('shopId');
        $m = M('Shops');
        $m->shopName = I('shopName');
        $rs = $m->where('shopId=' . $shopId)->save();
 
        if($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 更新店铺地址
     */
    public function upShopAddress() {
        $userInfo = $this->checkLogin();
        $shopId = I('shopId');
        
        $m = M('shops');
        if ($shopId > 0) {
            $data = array();
            $data['areaId1'] = I('provinceId');
            $data['areaId2'] = I('cityId');
            $data['areaId3'] = I('districtId');
            $data['shopAddress'] = I('address');
            $ll = $this->getLonLat($data['shopAddress']);
            $data['longitude'] = $ll['lon'];
            $data['latitude'] = $ll['lat'];
            
            $rs = $m->where('shopId=' . $shopId)->save($data);
            if ($rs === FALSE)
                $this->restApi(0, 'Error!', $rs);
            $this->restApi(1, 'Success!', $rs);
        }

        $this->restApi(0, '您还没有开通微店!');
    }

    /**
     * 获取店铺基本信息
     */
    public function getShopConfig() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Shops');
        
        $shop = $m->getShopConfig($userInfo);
        if(empty($shop))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $shop);
    }
    
    /**
     * 修改店铺图标
     */
    public function modifyShopImg() {
        $userInfo = $this->checkLogin();
        $shopId = I('shopId');
        $m = M('Shops');
        $m->shopImg = I('shopImg');
        $rs = $m->where('shopId=' . $shopId)->save();
 
        if($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }
    
    /**
     * 修改店铺运费
     */
    public function modifyPostage() {
        $userInfo = $this->checkLogin();
        $shopId = I('shopId');
        $m = M('Shops');
        $m->postage = I('postage');
        $rs = $m->where('shopId=' . $shopId)->save();
 
        if($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }
    
    /**
     * 修改店铺模板ID
     */
    public function modifyTemplateId() {
        $userInfo = $this->checkLogin();
        $shopId = I('shopId');
        $m = M('Shops');
        $m->templateId = I('templateId');
        $rs = $m->where('shopId=' . $shopId)->save();
 
        if($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }
    
    /**
     * 修改店铺展示形式
     */
    public function modifyListStyle() {
        $userInfo = $this->checkLogin();
        $shopId = I('shopId');
        $m = M('Shops');
        $m->listStyle = I('listStyle');
        $rs = $m->where('shopId=' . $shopId)->save();
 
        if($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }
    
    /**
     * 修改店铺联系电话
     */
    public function modifyShopTel() {
        $userInfo = $this->checkLogin();
        $shopId = I('shopId');
        $m = M('Shops');
        $m->shopTel = I('shopTel');
        $rs = $m->where('shopId=' . $shopId)->save();
 
        if($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }
    
    /**
     * 获取发布的商品
     */
    public function getGoodsList() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Shops');
        
        $goodsList = $m->getGoodsList();
        if(empty($goodsList))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $goodsList);
    }
    
    /**
     * 获取转售的商品
     */
    public function getResalesList() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Shops');
        
        $goodsList = $m->getResalesList();
        if(empty($goodsList))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $goodsList);
    }
}
