<?php

namespace Wap\Action;

class ShopsIndexAction extends BaseAction {
    
    /**
     * 店铺首页新品宝贝
     */
    public function newGoodsList() {
        $userinfo = $this->isLogin();
        $m = D('Wap/ShopsIndex');
        $shopId = I('shopId');
        $goodsList = $m->newGoodsList();
        $shop = $m->shopInfo($userinfo['userId']);
        
        $this->assign('shopId',$shopId);
        $this->assign('goods',$goodsList);
        $this->assign('shop',$shop);
        $this->assign('header_title', $shop['shopName']);
        $this->display("default/shop/newGoodsList");
    }
    
     /**
     * 店铺首页在售宝贝
     */
    public function saleGoodsList() {
        $userinfo = $this->isLogin();
        $m = D('Wap/ShopsIndex');
        $shopId = I('shopId');
        $goodsList = $m->saleGoodsList();
        $shop = $m->shopInfo($userinfo['userId']);
        
        $this->assign('shopId',$shopId);
        $this->assign('goods',$goodsList);
        $this->assign('shop',$shop);
        $this->assign('header_title', $shop['shopName']);
        $this->display("default/shop/saleGoodsList");
    }
    
    /**
     * 店铺首页已售宝贝
     */
    public function soldGoodsList() {
        $userinfo = $this->isLogin();
        $m = D('Wap/ShopsIndex');
        $shopId = I('shopId');
        $goodsList = $m->soldGoodsList();
        $shop = $m->shopInfo($userinfo['userId']);
        
        $this->assign('shopId',$shopId);
        $this->assign('goods',$goodsList);
        $this->assign('shop',$shop);
        $this->assign('header_title', $shop['shopName']);
        $this->display("default/shop/soldGoodsList");
    }
    
     /**
     * 店铺基本信息
     */
    public function shopInfo() {
        $m = D('Wap/ShopsIndex');
        $shopId = I('shopId');
        $shopInfo = $m->shopInfo();
        
        if(empty($shopInfo))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $shopInfo);
    }

}