<?php

namespace Wap\Action;

class UserOrderAction extends BaseAction {

    /**
     * 待付款订单列表
     */
    public function orderList() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/UserOrder');
        $data = $m->orderList($userInfo['userId']);

        if (empty($data))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    public function nopayList() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/UserOrder');
        $orderStatus = -2;
        $data = $m->orderList($userInfo['userId'], $orderStatus);

        $this->assign('order', $data);
        $this->assign('header_title', '我的订单');
        $this->display('default/order/list_nopay');
    }

    public function noShipping() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/UserOrder');
        $orderStatus = 0;
        $data = $m->orderList($userInfo['userId'], $orderStatus);

        $this->assign('order', $data);
        $this->assign('header_title', '我的订单');
        $this->display('default/order/list_noshipping');
    }

    public function noSign() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/UserOrder');
        $orderStatus = 1;
        $data = $m->orderList($userInfo['userId'], $orderStatus);

        $this->assign('order', $data);
        $this->assign('header_title', '我的订单');
        $this->display('default/order/list_nosign');
    }

    public function serviceList() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/UserOrder');
        $orderStatus = 2;
        $data = $m->orderList($userInfo['userId'], $orderStatus);

        $this->assign('order', $data);
        $this->assign('header_title', '我的订单');
        $this->display('default/order/list_service');
    }

    public function allOrderList() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/UserOrder');
        $data = $m->allOrderList($userInfo['userId']);

        $this->assign('order', $data);
        $this->assign('header_title', '我的订单');
        $this->display('default/order/list_order');
    }

    /**
     * 取消订单
     */
    public function cancelOrder() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/UserOrder');
        $rs = $m->cancelOrder($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 待付款订单结算
     */
    public function toCheckPay() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/UserOrder');
        $rs = $m->toCheckPay($userInfo['userId']);
        foreach($rs['data']['goods'] as $k=>$v){
            if ($v['isSource'] == 0) {
                $rs['data']['newgoods'][] = $v;
            }
        }
        unset($rs['data']['goods'] );
        $rs['data']['goods']  = $rs['data']['newgoods'];
        unset($rs['data']['newgoods'] );
 
        $order = $rs['data'];
//        show_pre_data($order);
        $this->assign('order', $order);
        $this->display('default/order/order_topay');
    }

    /**
     * 待付款订单支付
     */
    public function toPay() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Order');
        $rs = $m->toPay($userInfo['userId']);

        $this->restApi($rs['code'], $rs['info'], $rs['data']);
    }

    /**
     * 提醒发货
     */
    public function remind() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/UserOrder');
        $rs = $m->remind($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 订单详情
     */
    public function orderInfo() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/UserOrder');
        $data = $m->orderInfo($userInfo['userId']);

        if (empty($data))
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 确认收货
     */
    public function confirm() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/UserOrder');
        $rs = $m->confirm($userInfo['userId']);

        if ($rs == FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 快递信息
     */
    public function expressNo() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/UserOrder');
        $data = $m->expressNo($userInfo['userId']);

        if (empty($data))
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 商品评价
     */
    public function appraises() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/UserOrder');
        $rs = $m->appraises($userInfo['userId']);

        if ($rs == FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 售后信息
     */
    public function serviceInfo() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/UserOrder');
        $data = $m->serviceInfo($userInfo['userId']);

        if (empty($data))
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 提交退货
     */
    public function returnGoods() {
        $userInfo = $this->isUserLogin();
        $m = D('Wap/UserOrder');
        $rs = $m->returnGoods($userInfo['userId']);
        if ($rs == FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 待付款订单结算
     */
    public function toServer() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/UserOrder');
        $rs = $m->toServer($userInfo['userId']);

        $order = $rs['data'];
//        show_pre_data($order);
        $this->assign('order', $order);
        $this->assign('header_title', '申请售后');
        $this->display('default/order/order_server');
    }

}
