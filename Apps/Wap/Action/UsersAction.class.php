<?php

namespace Wap\Action;

class UsersAction extends BaseAction {

    public function login() {
        $this->assign('header_title', '用户登录');
        $this->display('default/user/login');
    }

    public function toLogin() {
        $m = D('Wap/Users');
        $userPhone = I('loginName');
        $pwd = I('loginPwd');

        $rs = $m->toLogin($userPhone, $pwd);
        $this->restApi($rs['code'], $rs['info'],$rs['data']);
    }

    public function regist() {
        $this->assign('header_title', '用户注册');
        $this->display('default/user/reg');
    }

    public function phoneCode() {
        $this->assign('header_title', '找回密码');
        $this->display('default/user/phone_code');
    }

    public function repassword() {
        $this->assign('header_title', '找回密码');
        $this->display('default/user/repassword');
    }

    public function index() {
        $user = $this->checkLogin();

        $this->assign('user', $user);
        $this->assign('header_title', '管理中心');
        $this->display('default/user/index');
    }

    public function upHead() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Users');
        if (IS_POST) {
            $userPhoto = I('userPhoto');
            if (!empty($userPhoto)) {
                $rs = $m->where('userId=' . $userInfo['userId'])->save(array('userPhoto' => $userPhoto));
                if ($rs == FALSE)
                    $this->restApi(0, '头像更新失败');
                $this->restApi(1, '头像更新成功');
            }
            $this->restApi(0, '头像更新失败');
        } else {
            $data = $m->getProfile($userInfo['userId']);

            $this->assign('user', $userInfo);
            $this->assign('userInfo', $data);
            $this->assign('header_title', '个人资料');
            $this->display('default/user/up_head');
        }
    }

    public function upHeadPic() {
        $img = $this->uploadImg('file');

        if (empty($img['file']['savename'])) {
            die(json_encode(array('code' => 0)));
        }
        die(json_encode(array('code' => 1, 'path' => $img['picPath'])));
    }

    public function setCode() {
        $data = array();
        $data['userPhone'] = I('userPhone');
        $data['mobileCode'] = I('mobileCode');

        $verify = session('VerifyCode_userPhone');
        $startTime = (int) session('VerifyCode_userPhone_Time');
        if ((time() - $startTime) > 300) {
            $this->restApi(0, '验证码已超过有效期!');
        }
        if ($data['mobileCode'] == "" || $verify != $data['mobileCode']) {
            $this->restApi(0, '验证码错误!');
        }

        session('MX_CHECKCODE', $data);
        $this->restApi(1, '成功!');
    }

    public function toRegist() {
        $m = D('Wap/Users');
        $reutrn = $m->regist();

        $this->restApi($reutrn['status'], $reutrn['msg']);
    }

    public function rePwd() {
        $m = D('Wap/Users');
        $reutrn = $m->rePwd();
        if ($reutrn['status'] > 0) {
            $this->restApi(1, '找回密码成功!', $reutrn);
        }
        $this->restApi(0, $reutrn['msg'], $reutrn);
    }

    public function loginOut() {
        cookie("MX_USER", null);
        redirect(U('Wap/Users/login'));
    }

    /**
     * 获取注册手机验证码
     */
    public function getRegPhoneVerifyCode() {
        $userPhone = I("userPhone");
        $rs = array();
        if (!preg_match("#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#", $userPhone)) {
            $this->restApi(0, '手机号码格式错误!');
        }
        $m = D('Wap/Users');
        $rs = $m->checkUserPhone($userPhone);
        if ($rs["status"] != 1) {
            $this->restApi(0, '该手机已经被注册!');
        }
        $phoneVerify = rand(100000, 999999);
        $rt = D('Wap/Sms')->sendSMScode($userPhone, array($phoneVerify, 5), 'codeTempId', 'registUser');
        if ($rt['status'] == 1) {
            session('VerifyCode_userPhone', $phoneVerify);
            session('VerifyCode_userPhone_Time', time());
        }
        $rt['VerifyCode'] = $phoneVerify;

        $this->restApi($rt['status'], $rt['msg'], $rt);
    }

    /**
     * 获取找回密码手机验证码
     */
    public function getPwdPhoneVerifyCode() {
        $userPhone = I("userPhone");
        $rs = array();
        if (!preg_match("#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#", $userPhone)) {
            $this->restApi(0, '手机号码格式错误!');
        }
        $m = D('Wap/Users');
        $rs = $m->checkUserPhone($userPhone);
        if ($rs["status"] == 1) {
            $this->restApi(0, '该手机账号不存在!');
        }
        $phoneVerify = rand(100000, 999999);
        $rt = D('Wap/Sms')->sendSMScode($userPhone, array($phoneVerify, 5), 'codeTempId', 'reUserPwd');
        if ($rt['status'] == 1) {
            session('VerifyCode_userPhone', $phoneVerify);
            session('VerifyCode_userPhone_Time', time());
        }
        $rt['VerifyCode'] = $phoneVerify;
        $this->restApi($rt['status'], $rt['msg'], $rt);
    }

    /**
     * 检查开店状态
     */
    public function checkShopStauts() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Users');
        $rs = $m->checkShopStauts($userInfo['userId']);

        $this->restApi(1, 'Success!', $rs);
    }

    /**
     * 个人资料
     */
    public function profile() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Users');
        $data = $m->getProfile($userInfo['userId']);

        $this->assign('user', $userInfo);
        $this->assign('userInfo', $data);
        $this->assign('header_title', '个人资料');
        $this->display('default/user/profile');
    }

    /**
     * 个人资料
     */
    public function editProfile() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Users');
        $rs = $m->editProfile($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 更新会员资料
     */
    public function upProfile() {
        $userInfo = $this->checkLogin();
        $m = M('users');
        if (IS_POST) {
            log_txt($_POST, 'ts.txt');
            $m->userName = I('userName');
            $m->userPhone = I('userPhone');
            $m->userSex = I('userSex');

            $rs = $m->where('userId=' . $userInfo['userId'])->save();
            if ($rs == FALSE)
                $this->restApi(0, '更新失败');
            $this->restApi(1, '更新成功');
        } else {
            $user = $m->where('userId=' . $userInfo['userId'])->find();
            $this->assign('user', $user);
            $this->assign('header_title', '个人资料');
            $this->assign('src', I('src'));
            $this->display('default/user/up_profile');
        }
    }

    /**
     * 添加|编辑用户地址
     */
    public function editAddress() {
        $userInfo = $this->checkLogin();
        if (IS_POST) {
            $m = D('Wap/Users');
            $rs = $m->editAddress($userInfo['userId']);

            if ($rs === FALSE)
                $this->restApi(0, 'Error!');
            $this->restApi(1, 'Success!');
        } else {
            $areas = M('areas')->where('parentId = 0')->select();
            $this->assign('areas', $areas);
            $id = I('id', 0);
            if ($id > 0) {
                $address = M('user_address')->where('addressId=' . $id)->find();
                $this->assign('address', $address);
            }
            $this->assign('header_title', '地址管理');
            $this->assign('cartId', I('cartId', 0));
            $this->display('default/user/add_address');
        }
    }

    /**
     * 地址详细信息
     */
    public function address() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Users');
        $data = $m->getAddress();

        if (empty($data))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 设置默认地址
     */
    public function setDefaultAddress() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Users');
        $rs = $m->setDefaultAddress($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 用户地址列表
     */
    public function addressList() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Users');
        $data = $m->getAddressList($userInfo['userId']);

        $this->assign('header_title', '地址管理');
        $this->assign('cartId', I('cartId', 0));
        $this->assign('lists', $data);
        $this->display('default/user/list_address');
    }

    /**
     * 删除用户地址
     */
    public function delAddress() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Users');
        $rs = $m->delAddress();
        
        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 关注商品
     */
    public function followGoods() {
        $userInfo = $this->isLogin();
        if($userInfo['userId'] === 0)
            $this->restApi(100001, '请登录!');
        $m = D('Wap/Users');
        $rs = $m->followGoods($userInfo['userId']);

        if ($rs === -1)
            $this->restApi(0, '已关注');
        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 关注店铺
     */
    public function followShop() {
        $userInfo = $this->isLogin();
        if($userInfo['userId'] === 0)
            $this->restApi(100001, '请登录!');
        $m = D('Wap/Users');
        $rs = $m->followShop($userInfo['userId']);

        if ($rs === -1)
            $this->restApi(0, '已关注');
        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 关注列表
     */
    public function followList() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Users');
        $data = $m->getFollowList($userInfo['userId']);

        $type = I('type');
        $this->assign('user', $userInfo);
        $this->assign('list', $data);
        $this->assign('type', $type);
        $this->assign('header_title', '我的收藏');
        if ($type == 'goods') {
            $this->display('default/user/follow_goods');
        } else {
            $this->display('default/user/follow_shop');
        }
    }

    /**
     * 删除关注
     */
    public function delFollow() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Users');
        $rs = $m->delFollow();

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 用户钱包
     */
    public function userBurse() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Users');
        $data = $m->userBurse($userInfo['userId']);

        if (empty($data))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 添加银行卡
     */
    public function userBankcard() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Users');
        $data = $m->userBankcard($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }

    /**
     * 银行卡列表
     */
    public function bankcardList() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Users');
        $data = $m->bankcardList($userInfo['userId']);

        if (empty($data))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }

    /**
     * 删除银行卡
     */
    public function delBankcard() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Users');
        $data = $m->delBankcard($userInfo['userId']);

        if ($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }
    
    

}
