<?php

namespace Wap\Action;

class WxpayAction extends BaseAction {

    public function native_url() {
        vendor("Wxpay.WxPayHelper");
        $orderSN = I('orderSN');
        
        $wxPayHelper = new \WxPayHelper();
        $native_url = $wxPayHelper->create_native_url($orderSN);
        echo $native_url;
        $native_url = parse_url($native_url);
        parse_str($native_url['query'], $native_url);
        $this->restApi(1, 'Success!', array('native_url' => $native_url));
    }

    public function nativecall() {
        vendor("Wxpay.WxPayHelper");

        $orderSN = I('orderSN');
        $commonUtil = new \CommonUtil();
        $wxPayHelper = new \WxPayHelper();

        log_txt($commonUtil->create_noncestr(), 'log/log_wxpay/out_trade_no-' . time());

        $wxPayHelper->setParameter("bank_type", "WX");
        $wxPayHelper->setParameter("body", "订单" . $orderSN . "支付");
        $wxPayHelper->setParameter("partner", "1900000109");
        $wxPayHelper->setParameter("out_trade_no", $commonUtil->create_noncestr());
        $wxPayHelper->setParameter("total_fee", "0.01");
        $wxPayHelper->setParameter("fee_type", "0.01");
        $wxPayHelper->setParameter("notify_url", 'http://khapi.mooxun.com' . U("Wxpay/notify_url"));
        $wxPayHelper->setParameter("spbill_create_ip", "127.0.0.1");
        $wxPayHelper->setParameter("input_charset", "GBK");
        $package = $wxPayHelper->create_native_package();
        echo $package;
//        $this->restApi(1, 'Success!' , array('package' => $package));
    }

    public function notify_url() {
        vendor('WxPayPubHelper.WxPayPubHelper');
        //使用通用通知接口
        $notify = new \Notify_pub();

        //存储微信的回调
        $xml = $GLOBALS['HTTP_RAW_POST_DATA'];
        $notify->saveData($xml);

        //验证签名，并回应微信。
        //对后台通知交互时，如果微信收到商户的应答不是成功或超时，微信认为通知失败，
        //微信会通过一定的策略（如30分钟共8次）定期重新发起通知，
        //尽可能提高通知的成功率，但微信不保证通知最终能成功。
        // $this->log_result("【checkSign】:\n".$notify->checkSign()."\n");
        if ($notify->checkSign() == FALSE) {
            $notify->setReturnParameter("return_code", "FAIL"); //返回状态码
            $notify->setReturnParameter("return_msg", "签名失败"); //返回信息
        } else {
            $notify->setReturnParameter("return_code", "SUCCESS"); //设置返回码
        }
        $returnXml = $notify->returnXml();
        log_txt($returnXml, './log/log_wxpay/returnXml-' . time());
        $parameter = $notify->xmlToArray($xml);
        log_txt($parameter, './log/log_wxpay/xmlToArray-' . time());
        // $this->log_result("【返回回调信息】:\n".$returnXml."\n");
        //==商户根据实际情况设置相应的处理流程，此处仅作举例=======
        //以log文件形式记录回调信息
        $this->log_result("【接收到的notify通知】:\n" . $xml . "\n");

        if ($notify->checkSign() == TRUE) {
            if ($notify->data["return_code"] == "FAIL") {
                //此处应该更新一下订单状态，商户自行增删操作
                $this->log_result("【通信出错】:\n" . $xml . "\n");
            } elseif ($notify->data["result_code"] == "FAIL") {
                //此处应该更新一下订单状态，商户自行增删操作
                $this->log_result("【业务出错】:\n" . $xml . "\n");
            } else {
                //此处应该更新一下订单状态，商户自行增删操作
                $this->log_result("【支付成功】:\n" . $xml . "\n");
            }

            //商户自行增加处理流程,
            //例如：更新订单状态
            //例如：数据库操作
            //例如：推送支付完成信息
            if ($this->process($parameter)) {

                //处理成功后输出success，微信就不会再下发请求了
                echo 'success';
            } else {

                //没有处理成功，微信会间隔的发送请求
                echo 'error';
            }
        }
    }

    //订单处理

    private function process($parameter) {

        //此处应该更新一下订单状态，商户自行增删操作

        /*
         * 返回的数据最少有以下几个
         * $parameter = array(
          'out_trade_no' => xxx,//商户订单号
          'total_fee' => XXXX,//支付金额
          'openid' => XXxxx,//付款的用户ID
          );
         */

        return true;
    }

}
