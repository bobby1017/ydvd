<?php

namespace Wap\Action;

class ZoneAction extends BaseAction {
    
    /**
     * 添加好友
     */
    public function addFriend() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Zone');
        $rs = $m->addFriend($userInfo['userId']);
        
        if($rs === -1)
            $this->restApi(0, '已经加为好友!');
        if($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }
    
    /**
     * 新的好友
     */
    public function newFriends() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Zone');
        $data = $m->newFriends($userInfo['userId']);
        
        if(empty($data))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }
    
    /**
     * 忽略好友
     */
    public function ignoreFriend() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Zone');
        $rs = $m->ignoreFriend();
        
        if($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }
    
    /**
     * 同意好友
     */
    public function agreeFriend() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Zone');
        $rs = $m->agreeFriend();
        
        if($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }
    
    /**
     * 删除好友
     */
    public function delFriend() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Zone');
        $rs = $m->delFriend($userInfo['userId']);
        
        if($rs === FALSE)
            $this->restApi(0, 'Error!');
        $this->restApi(1, 'Success!');
    }
    
    /**
     * 好友列表
     */
    public function friends() {
        $userInfo = $this->checkLogin();
        $m = D('Wap/Zone');
        $data = $m->friends($userInfo['userId']);
        
        if(empty($data))
            $this->restApi(0, 'No Data!');
        $this->restApi(1, 'Success!', $data);
    }
}