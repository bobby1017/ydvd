<?php

namespace Wap\Model;

/**
 * ============================================================================
 * MXCMall开源商城
 * 官网地址:http://www.mothink.cn
 * 联系QQ:510858687
 * ============================================================================
 * 商品服务类
 */
class GoodsModel extends BaseModel {

    /**
     * 检查上传宝贝权限
     */
    public function checkAddGoodsAuth($userInfo, $type = '') {
        $rt = array();
        $rt['code'] = 0;
        $rt['info'] = 'Error!';
        if ($userInfo['userType'] == 0) {
            $rt['info'] = '普通用户无法上传宝贝!';
            return $rt;
        }

        $m = M('shops');
        $shop = $m->where('userId=' . $userInfo['userId'])->field('shopId,shopFlag,shopStatus,depositId')->find();
        if ($shop['shopFlag'] != 1 && $shop['shopStatus'] != 1) {
            $rt['info'] = '您的店铺目前没有上传宝贝的权限!';
            return $rt;
        }
        $m = M('shops_deposit');
        $deposit = $m->where('id=' . $shop['depositId'])->find();
        $m = M('goods');
        if ($type == 'resale') {
            $sum = $m->where('shopId=' . $shop['shopId'] . ' AND isDel = 0 AND isSource = 0')->count();
            if ($sum >= $deposit['resaleNum']) {
                $rt['info'] = '您目前最多只能转售' . intval($deposit['resaleNum']) . '个宝贝!';
                return $rt;
            }
        } else {
            $sum = $m->where('shopId=' . $shop['shopId'] . ' AND isDel = 0 AND isSource = 1')->count();
            if ($sum >= $deposit['goodsNum']) {
                $rt['info'] = '您目前最多只能发布' . intval($deposit['goodsNum']) . '个宝贝!';
                return $rt;
            }
        }

        return $shop;
    }

    /**
     * 上传宝贝
     */
    public function addGoods($shopId) {
        $data = array();
        $data['shopId'] = $shopId;
        $data['goodsName'] = I('goodsName');    //售价
        $data['marketPrice'] = I('marketPrice', 0);    //售价
        $data['shopPrice'] = I('shopPrice', 0);    //合作价
        $data['goodsStock'] = I('goodsStock', 1);
        $cat = $this->getCatData(I('catId'));
        $data['goodsCatId1'] = $cat['goodsCatId1'];
        $data['goodsCatId2'] = $cat['goodsCatId2'];
        $class = $this->getClassData(I('classId'));
        $data['goodsClassId1'] = $class['goodsClassId1'];
        $data['goodsClassId2'] = $class['goodsClassId2'];
        $data['goodsSize'] = I('goodsSize');
        $data['goodsWeight'] = I('goodsWeight');
        $data['isOffline'] = I('isOffline', 0);
        $data['createTime'] = date('Y-m-d H:i:s');
        $pics = json_decode(htmlspecialchars_decode(I('pics')), TRUE);
        if (!empty($pics)) {
            $data['goodsImg'] = $pics[0]['picThumbPath'];
            $data['goodsThumbs'] = $pics[0]['picThumbPath'];
        }

//        show_pre_data($data, 1);
        foreach ($data as $v) {
            if ($v === '') {
                $rt['code'] = 0;
                $rt['info'] = '请将信息填写完整!';
                return $rt;
            }
        }
        $m = M('goods');
        $goodsId = $m->add($data);
        if ($goodsId === FALSE) {
            $rt['code'] = 0;
            $rt['info'] = 'Error!';
            return $rt;
        }

        if (!empty($pics)) {
            $dataList = array();
            foreach ($pics as $v) {
                $data = array();
                $data['goodsId'] = $goodsId;
                $data['shopId'] = $shopId;
                $data['goodsImg'] = $v['picPath'];
                $data['goodsThumbs'] = $v['picThumbPath'];
                $dataList[] = $data;
            }
            $m = M('goods_gallerys');
            $rs = $m->addAll($dataList);
        }


        $data = array();
        $data['goodsId'] = $goodsId;
        $data['shopId'] = $shopId;
        $data['goodsContent'] = I('content');
        $m = M('goods_data');
        $rs = $m->add($data);

        return array('code' => 1, 'info' => 'Success!');
    }

    /**
     * 转售宝贝
     */
    public function resaleGoods($shopId) {
        $m = M('goods');
        $pgoods = $m->where('goodsId = ' . I('parentId'))->find();
        if ($pgoods['goodsStatus'] == 0 || $pgoods['isSale'] == 0 || $pgoods['goodsStock'] == 0) {
            $rt['code'] = 0;
            $rt['info'] = '对不起,该宝贝目前无法转售!';
            return $rt;
        }

        $data = array();
        $data['shopId'] = $shopId;
        $data['goodsName'] = I('goodsName');    //售价
        $data['marketPrice'] = I('marketPrice', 0);    //售价
        $data['shopPrice'] = I('shopPrice', 0);    //合作价
        $data['goodsCatId1'] = $pgoods['goodsCatId1'];
        $data['goodsCatId2'] = $pgoods['goodsCatId2'];
        $data['goodsClassId1'] = $pgoods['goodsClassId1'];
        $data['goodsClassId2'] = $pgoods['goodsClassId2'];
        $data['goodsSize'] = $pgoods['goodsSize'];
        $data['goodsWeight'] = $pgoods['goodsWeight'];
        $data['isOffline'] = $pgoods['isOffline'];
        $pics = json_decode(htmlspecialchars_decode(I('pics')), TRUE);
        if (!empty($pics)) {
            $data['goodsImg'] = $pics[0]['picPath'];
            $data['goodsThumbs'] = $pics[0]['picThumbPath'];
        }
        if ($pgoods['marketPrice'] > $data['marketPrice'] || $pgoods['shopPrice'] > $data['shopPrice']) {
            $rt['code'] = 0;
            $rt['info'] = '转售的价格不得低于原宝贝的价格!';
            return $rt;
        }
        $data['createTime'] = date('Y-m-d H:i:s');
        $data['isSource'] = 0;

        foreach ($data as $v) {
            if ($v === '') {
                $rt['code'] = 0;
                $rt['info'] = '请将信息填写完整!';
                return $rt;
            }
        }
        $goodsId = $m->add($data);
        if ($goodsId === FALSE) {
            $rt['code'] = 0;
            $rt['info'] = 'Error!';
            return $rt;
        }

        $data = array();
        $data['parentId'] = I('parentId');
        $data['goodsId'] = $goodsId;
        $data['shopId'] = $shopId;
        $data['pshopId'] = $pgoods['shopId'];
        if ($pgoods['isSource'] == 1) {
            $data['sourceId'] = $pgoods['goodsId'];
            $data['node'] = $pgoods['goodsId'] . ',' . $goodsId;
        } else {
            $prgoods = M('goods_resale')->where('goodsId = ' . $pgoods['goodsId'])->find();
            $data['sourceId'] = $prgoods['sourceId'];
            $data['node'] = $prgoods['node'] . ',' . $goodsId;
        }

        M('goods_resale')->add($data);

        if (!empty($pics)) {
            $dataList = array();
            foreach ($pics as $v) {
                $data = array();
                $data['goodsId'] = $goodsId;
                $data['shopId'] = $shopId;
                $data['goodsImg'] = $v['picPath'];
                $data['goodsThumbs'] = $v['picThumbPath'];
                $dataList[] = $data;
            }
            $m = M('goods_gallerys');
            $rs = $m->addAll($dataList);
        }


        $data = array();
        $data['goodsId'] = $goodsId;
        $data['shopId'] = $shopId;
        $data['goodsContent'] = I('content');
        $m = M('goods_data');
        $rs = $m->add($data);

        return array('code' => 1, 'info' => 'Success!');
    }

    /**
     * 编辑宝贝
     */
    public function editGoods($shopId) {
        $goodsId = I('goodsId');
        $m = M('goods');
        $oldGoods = $m->where('goodsId=' . $goodsId)->find();
        $data = array();
        $data['shopId'] = $shopId;
        $data['goodsName'] = I('goodsName');    //售价
        $data['marketPrice'] = I('marketPrice', 0);    //售价
        $data['shopPrice'] = I('shopPrice', 0);    //合作价
        $data['goodsStock'] = I('goodsStock', 1);
        $cat = $this->getCatData(I('catId'));
        $data['goodsCatId1'] = $cat['goodsCatId1'];
        $data['goodsCatId2'] = $cat['goodsCatId2'];
        $class = $this->getClassData(I('classId'));
        $data['goodsClassId1'] = $class['goodsClassId1'];
        $data['goodsClassId2'] = $class['goodsClassId2'];
        $data['goodsSize'] = I('goodsSize');
        $data['goodsWeight'] = I('goodsWeight');
        $data['isOffline'] = I('isOffline', 0);
        $pics = json_decode(htmlspecialchars_decode(I('pics')), TRUE);
        $data['goodsImg'] = $pics[0]['picPath'];
        $data['goodsThumbs'] = $pics[0]['picThumbPath'];
//        show_pre_data($data);
        foreach ($data as $v) {
            if ($v === '') {
                $rt['code'] = 0;
                $rt['info'] = '请将信息填写完整!';
                return $rt;
            }
        }
        $rs = $m->where('goodsId=' . $goodsId)->save($data);
        if ($rs === FALSE) {
            $rt['code'] = 0;
            $rt['info'] = 'Error!';
            return $rt;
        }

        $m = M('goods_gallerys');
        foreach ($pics as $v) {
            $data = array();
            if (intval($v['id']) == 0) {
                $data['goodsId'] = $goodsId;
                $data['shopId'] = $shopId;
                $data['goodsImg'] = $v['picPath'];
                $data['goodsThumbs'] = $v['picThumbPath'];
                $m->add($data);
            } else {
                $data['goodsImg'] = $v['picPath'];
                $data['goodsThumbs'] = $v['picThumbPath'];
                $m->where('id=' . $v['id'])->save($data);
            }
        }

        $data = array();
        $data['goodsContent'] = I('content');
        $m = M('goods_data');
        $rs = $m->where('goodsId=' . $goodsId)->save($data);

        $up_marketPrice = I('marketPrice') - $oldGoods['marketPrice'];
        $up_shopPrice = I('shopPrice') - $oldGoods['shopPrice'];
        if ($up_marketPrice != 0 || $up_shopPrice != 0) {
            $up_ids = M('goods_resale')->where('sourceId=' . $goodsId)->field('goodsId')->select();
            $up_ids = $this->idsToString($up_ids, 'goodsId');
            if ($up_marketPrice != 0) {
                $rs = M('goods')->where('goodsId in (' . $up_ids . ')')->setInc('marketPrice', $up_marketPrice);
            }
            if ($up_shopPrice != 0) {
                $rs = M('goods')->where('goodsId in (' . $up_ids . ')')->setInc('shopPrice', $up_shopPrice);
            }
        }

        return array('code' => 1, 'info' => 'Success!');
    }

    /**
     * 编辑转售宝贝
     */
    public function editResale($shopId) {
        $goodsId = I('goodsId');
        $m = M('goods');
        $sql = "select * from __PREFIX__goods g, __PREFIX__goods_resale gr where g.goodsId = gr.goodsId and g.goodsId = " . $goodsId;
        $oldGoods = $this->queryRow($sql);
//        $goods_resale = M('goods_resale')->where('goodsId = ' . $goodsId)->find();
        $pgoods = $m->where('goodsId = ' . $oldGoods['parentId'])->find();

        $data = array();
        $data['shopId'] = $shopId;
        $data['goodsName'] = I('goodsName');    //售价
        $data['marketPrice'] = I('marketPrice', 0);    //售价
        $data['shopPrice'] = I('shopPrice', 0);    //合作价
        $pics = json_decode(htmlspecialchars_decode(I('pics')), TRUE);
        $data['goodsImg'] = $pics[0]['picPath'];
        $data['goodsThumbs'] = $pics[0]['picThumbPath'];
        if ($pgoods['marketPrice'] > $data['marketPrice'] || $pgoods['shopPrice'] > $data['shopPrice']) {
            $rt['code'] = 0;
            $rt['info'] = '价格不得低于原宝贝的价格!';
            return $rt;
        }

        foreach ($data as $v) {
            if ($v === '') {
                $rt['code'] = 0;
                $rt['info'] = '请将信息填写完整!';
                return $rt;
            }
        }
        $rs = $m->where('goodsId=' . $goodsId)->save($data);
        if ($rs === FALSE) {
            $rt['code'] = 0;
            $rt['info'] = 'Error!';
            return $rt;
        }

        $m = M('goods_gallerys');
        foreach ($pics as $v) {
            $data = array();
            if (intval($v['id']) == 0) {
                $data['goodsId'] = $goodsId;
                $data['shopId'] = $shopId;
                $data['goodsImg'] = $v['picPath'];
                $data['goodsThumbs'] = $v['picThumbPath'];
                $m->add($data);
            } else {
                $data['goodsImg'] = $v['picPath'];
                $data['goodsThumbs'] = $v['picThumbPath'];
                $m->where('id=' . $v['id'])->save($data);
            }
        }

        $data = array();
        $data['goodsContent'] = I('content');
        $m = M('goods_data');
        $rs = $m->where('goodsId=' . $goodsId)->save($data);

        $up_marketPrice = I('marketPrice') - $oldGoods['marketPrice'];
        $up_shopPrice = I('shopPrice') - $oldGoods['shopPrice'];
        if ($up_marketPrice != 0 || $up_shopPrice != 0) {
            $up_ids = M('goods_resale')->where("node like '" . $oldGoods['node'] . ",%'")->field('goodsId')->select();
            $up_ids = $this->idsToString($up_ids, 'goodsId');
            if ($up_marketPrice != 0) {
                $rs = M('goods')->where('goodsId in (' . $up_ids . ')')->setInc('marketPrice', $up_marketPrice);
            }
            if ($up_shopPrice != 0) {
                $rs = M('goods')->where('goodsId in (' . $up_ids . ')')->setInc('shopPrice', $up_shopPrice);
            }
        }

        return array('code' => 1, 'info' => 'Success!');
    }

    /**
     * 获取款式两层ID
     * @param type $catId
     * @return type
     */
    public function getCatData($catId) {
        $m = M('goods_cats');
        $cat = $m->where('catId=' . $catId)->field('catId, parentId')->find();
        if ($cat['parentId'] == 0) {
            return array('goodsCatId1' => $catId, 'goodsCatId2' => 0);
        }
        return array('goodsCatId1' => $cat['parentId'], 'goodsCatId2' => $catId);
    }

    /**
     * 获取种质两层ID
     * @param type $classId
     * @return type
     */
    public function getClassData($classId) {
        $m = M('goods_class');
        $class = $m->where('classId=' . $classId)->field('classId, parentId')->find();
        if ($class['parentId'] == 0) {
            return array('goodsClassId1' => $classId, 'goodsClassId2' => 0);
        }
        return array('goodsClassId1' => $class['parentId'], 'goodsClassId2' => $classId);
    }

    /**
     * 获取款式列表
     */
    public function getCatsTree() {
        $m = M('goods_cats');
        $catTree = $m->where('catFlag = 1 AND parentId = 0 AND isShow = 1')->order('catSort ASC')->field('catId, catName')->select();
        foreach ($catTree as $k => $v) {
            $child = $m->where('catFlag = 1 AND parentId = ' . $v['catId'] . ' AND isShow = 1')->order('catSort ASC')->field('catId, catName')->select();
            $catTree[$k]['child'] = $child;
        }

        return $catTree;
    }

    /**
     * 获取种质列表
     */
    public function getClassTree() {
        $m = M('goods_class');
        $classTree = $m->where('classFlag = 1 AND parentId = 0 AND isShow = 1')->order('classSort ASC')->field('classId, className')->select();
        foreach ($classTree as $k => $v) {
            $child = $m->where('classFlag = 1 AND parentId = ' . $v['classId'] . ' AND isShow = 1')->order('classSort ASC')->field('classId, className')->select();
            $classTree[$k]['child'] = $child;
        }

        return $classTree;
    }

    /**
     * 获取转售商品详情
     */
    public function getParentGoods() {
        $goodsId = I('goodsId');
        $goods = M('goods')->where('goodsId=' . $goodsId)->field('goodsId,goodsName,shopId,marketPrice,shopPrice,goodsCatId2,goodsClassId2,goodsSize,goodsWeight')->find();
        $goods['catName'] = M('goods_cats')->where('catId=' . $goods['goodsCatId2'])->getField('catName');
        $goods['className'] = M('goods_class')->where('classId=' . $goods['goodsClassId2'])->getField('className');
        $goods['pics'] = M('goods_gallerys')->where('goodsId=' . $goodsId)->field('id,goodsImg,goodsThumbs')->select();
        $goods['goodsContent'] = M('goods_data')->where('goodsId=' . $goodsId)->getField('goodsContent');

        return $goods;
    }

    /**
     * 删除商品相册图片
     */
    public function delGoodsPic() {
        $id = intval(I('id'));
        $m = M('goods_gallerys');
        $pic = $m->where('id = ' . $id)->find();
//        @unlink($pic['goodsImg']);
//        @unlink($pic['goodsThumbs']);
        $rs = $m->where('id = ' . $id)->delete();

        if ($rs == FALSE)
            return 0;
        return 1;
    }

    /**
     * 获取商品详细信息
     */
    public function getGoodsDetail($param) {
        $goodsId = I('goodsId');
        $goods = M('goods')->where('goodsId=' . $goodsId)->field('goodsId,goodsName,shopId,marketPrice,shopPrice,goodsStock,goodsCatId1,goodsCatId2,goodsClassId1,goodsClassId2,goodsSize,goodsWeight,isOffline')->find();
        $goods['pics'] = M('goods_gallerys')->where('goodsId=' . $goodsId)->field('shopId,goodsImg,goodsThumbs')->select();
        $goods['goodsContent'] = M('goods_data')->where('goodsId=' . $goodsId)->getField('goodsContent');

        return $goods;
    }

}
