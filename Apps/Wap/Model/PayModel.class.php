<?php

namespace Wap\Model;

class PayModel extends BaseModel {
    /**
     * 完成支付订单
     */
    public function cashDepositPay($obj) {

        $orderSN = $obj["out_trade_no"];
        $total_fee = $obj["total_fee"];
        log_txt($_POST, 'log/log_alipay/cashDepositPay-Model-' . $out_trade_no . '.txt');
        $log = M('user_deposit_log')->where('orderSN=' . $orderSN)->find();
        if($log['type'] == 2) {
            $m = M('shops');
            $rs = $m->where('userId=' . $log['userId'])->save(array('depositId' => $log['depositId']));
            $rs = $m->where('userId=' . $log['userId'])->setInc('depositMoney', $total_fee);
            M('user_deposit_log')->where('orderSN=' . $orderSN)->save(array('status' => 1));
            $this->MlogBalance($log['userId'], $total_fee, 1, date('Y-m-d H:i:s') . '保证金支付');
            return $rs;
        } else {
            $m = M('users');
            $rs = $m->where('userId=' . $log['userId'])->setInc('userBalance', $total_fee);
            M('user_deposit_log')->where('orderSN=' . $orderSN)->save(array('status' => 1));
            $this->MlogBalance($log['userId'], $total_fee, 2, '钱包充值');
            return $rs;
        }

        return;
    }
    
}