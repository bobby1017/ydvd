<?php

namespace Wap\Model;

class ShopOrderModel extends BaseModel {

    /**
     * 销售订单列表
     * @param type $userId
     * @return type
     */
    public function orderList($userId) {
        $orderStatus = I('orderStatus', -2);
        $num = I('num') ? I('num') : 8;
        $page = I('page') ? I('page') : 1;
        $page = ($page - 1) * $num;

        $shopInfo = M('shops')->where('userId=' . $userId)->field('shopId,userId,shopName')->find();
        if ($orderStatus == 2) {
            $statusSql = ' AND orderStatus IN (2,3,4,5) ';
        } else {
            $statusSql = ' AND orderStatus = ' . $orderStatus;
        }
        $order = M('orders')->where('orderFlag = 1 ' . $statusSql . ' AND ShopId=' . $shopInfo['shopId'])->field('orderId,orderNo,totalMoney,postage,supPrice,supPostage,payType,buyway,(totalMoney+postage+supPrice+supPostage) as priceSum,expressId,expressNo,isSource')->order('orderId DESC')->limit($page, $num)->select();

        if (empty($order))
            return $order;
        $data = array();
        foreach ($order as $v) {
            $v['expressName'] = M('express')->where('expressId=' . $v['expressId'])->getField('expressName');
            if (empty($v['expressName'])) {
                $v['expressName'] = '';
            }
            $v['goods'] = M('order_goods')->where('orderNo=' . $v['orderNo'])->select();
            $data[] = $v;
        }

        return $data;
    }

    /**
     * 商家发货填写单号
     * @return type
     */
    public function deliverGoods() {
        $m = M('orders');
        $orderId = I('orderId', 0);
        $expressId = I('expressId', 0);
        $expressNo = I('expressNo', '');

        if ($orderId == 0 || $expressId == 0 || $expressNo == '')
            return FALSE;

        $m->orderStatus = 1;
        $m->expressId = $expressId;
        $m->expressNo = $expressNo;
        $rs = $m->where('orderStatus = 0 and orderId=' . $orderId)->save();

        return $rs;
    }

    /**
     * 订单统计
     * @param type $userId
     * @return type
     */
    public function orderCount($userId) {
        $m = M('orders');
        $shopId = M('shops')->where('userId=' . $userId)->getField('shopId');
        $where = ' orderFlag = 1 and orderStatus = 2  and shopId = ' . $shopId;
        $startTime = I('startTime') ? I('startTime') : date("Y-m-d", strtotime("-15 day"));
        $endTime = I('endTime') ? I('endTime') : date("Y-m-d");
        $where .= " and createTime > '" . $startTime . "'";
        $where .= " and createTime < '" . $endTime . "'";

        $sql = "SELECT date_format(`createTime`, '%Y-%m-%d') AS time, count( * ) AS count FROM __PREFIX__orders WHERE " . $where . " GROUP BY Time ORDER BY createTime DESC";
        $data = $this->query($sql);

        return $data;
    }

    /**
     * 金额统计
     * @param type $userId
     * @return type
     */
    public function priceCount($userId) {
        $m = M('orders');
        $shopId = M('shops')->where('userId=' . $userId)->getField('shopId');
        $where = ' orderFlag = 1 and orderStatus = 6  and shopId = ' . $shopId;
//        $where = ' orderFlag = 1 ';
        $startTime = I('startTime') ? I('startTime') : date("Y-m-d", strtotime("-15 day"));
        $endTime = I('endTime') ? I('endTime') : date("Y-m-d");
        $where .= " and createTime > '" . $startTime . "'";
        $where .= " and createTime < '" . $endTime . "'";

        $sql = "SELECT date_format(`createTime`, '%Y-%m-%d') AS time, sum( `totalMoney` ) AS totalMoney FROM __PREFIX__orders WHERE " . $where . " GROUP BY Time ORDER BY createTime DESC";
        $data = $this->query($sql);
        return $data;
    }

    /**
     * 访客统计
     * @param type $userId
     * @return type
     */
    public function visitorCount($userId) {
        $data = array(
            array(
                "time" => "2015-11-20",
                "count" => "63784.00"
            ),
            array(
                "time" => "2015-11-19",
                "count" => "7477.00",
            ),
            array(
                "time" => "2015-11-18",
                "count" => "281800.00",
            )
        );
        return $data;
    }
    
    /**
     * 收藏统计
     * @param type $userId
     * @return type
     */
    public function followCount($userId) {
        $data = array(
            array(
                "time" => "2015-11-20",
                "count" => "63784.00"
            ),
            array(
                "time" => "2015-11-19",
                "count" => "7477.00",
            ),
            array(
                "time" => "2015-11-18",
                "count" => "281800.00",
            )
        );
        return $data;
    }

}
