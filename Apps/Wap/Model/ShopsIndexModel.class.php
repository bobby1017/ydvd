<?php

namespace Wap\Model;

class ShopsIndexModel extends BaseModel {
    /**
     * 店铺首页新品宝贝
     */
    public function newGoodsList() {
        $m = M('goods');
        $shopId = I('shopId');
        $rs = $m->where('shopId=' . $shopId . ' and isSale = 1 and isDel = 0 and goodsFlag = 1 and goodsStatus = 1 and isSelling = 1 and goodsStock > 0')->field('goodsId,goodsName,goodsName,marketPrice, shopPrice,goodsSize,goodsWeight,goodsStatus,goodsThumbs,DATE_FORMAT(createTime, "%m月%d日") time, isSource')->order('goodsId DESC')->limit(30)->select();

        $goodsList = array();
        foreach($rs as $v) {
            $goodsList[$v['time']][] = $v;
        }
        $list = array();
        foreach($goodsList as $k => $v) {
            $temp = array();
            $temp['time'] = $k;
            $temp['data'] = $v;
            $list[] = $temp;
        }

        return $list;
    }

    /**
     * 店铺首页在售宝贝
     */
    public function saleGoodsList() {
        $m = M('goods');
        $shopId = I('shopId');
        $num = 6;
        $page = I('page') ? I('page') : 1;
        $current = $page;
        $page = ($page - 1) * $num;
        $goodsList['count'] = $m->where('shopId=' . $shopId . ' and isSale = 1 and isDel = 0 and goodsFlag = 1 and goodsStatus = 1 and isSelling = 1 and goodsStock > 0')->count();
        $goodsList['shopId'] = $shopId;
        $goodsList['num'] = $num;
        $goodsList['current'] = $current;
        $goodsList['sumpage'] = ceil($goodsList['count'] / $num);

        $goodsList['goods'] = $m->where('shopId=' . $shopId . ' and isSale = 1 and isDel = 0 and goodsFlag = 1 and goodsStatus = 1 and isSelling = 1 and goodsStock > 0')->field('goodsId,goodsName,goodsName,marketPrice, shopPrice,goodsSize,goodsWeight,goodsStatus,goodsThumbs,DATE_FORMAT(createTime, "%m月%d日") time, isSource')->order('goodsId DESC')->limit($page, $num)->select();

        return $goodsList;
    }

    /**
     * 店铺首页已售宝贝
     */
    public function soldGoodsList() {
        $m = M('goods');
        $shopId = I('shopId');
        $num = 6;
        $page = I('page') ? I('page') : 1;
        $current = $page;
        $page = ($page - 1) * $num;
        $goodsList['count'] = $m->where('shopId=' . $shopId . ' and ((isSale = 1 and isDel = 0 and goodsFlag = 1 and goodsStatus = 1 and isSelling = 0) or goodsStock = 0)')->count();
        $goodsList['shopId'] = $shopId;
        $goodsList['num'] = $num;
        $goodsList['current'] = $current;
        $goodsList['sumpage'] = ceil($goodsList['count'] / $num);

        $goodsList['goods'] = $m->where('shopId=' . $shopId . ' and ((isSale = 1 and isDel = 0 and goodsFlag = 1 and goodsStatus = 1 and isSelling = 0) or goodsStock = 0)')->field('goodsId,goodsName,goodsName,marketPrice, shopPrice,goodsSize,goodsWeight,goodsStatus,goodsThumbs,DATE_FORMAT(createTime, "%m月%d日") time, isSource,goodsStock,isSelling')->group('goodsId')->order('goodsId DESC')->limit($page, $num)->select();

        return $goodsList;
    }

    /**
     * 店铺基本信息
     * @return type
     */
    public function shopInfo($userId = 0) {
        $m = M('shops');
        $shopId = I('shopId');
        $shop = $m->where('shopId=' . $shopId)->field('shopId,(shopId+100000) shopSn,userId,shopName,shopImg,shopTel,shopAddress')->find();
        $areaId1 = M('areas')->where('areaId=' . $shop['areaId1'])->getField('areaName');
        $areaId2 = M('areas')->where('areaId=' . $shop['areaId2'])->getField('areaName');
        $areaId3 = M('areas')->where('areaId=' . $shop['areaId3'])->getField('areaName');
        $shop['areas'] = $areaId1 . $areaId2 . $areaId3;
        $shop['goods'] = M('goods')->where('shopId=' . $shopId . ' and goodsStatus = 1 and isSale = 1 and isDel = 0 and goodsFlag = 1 and goodsStock > 0 and createTime > "' . date("Y-m-d H:i:s",strtotime("-32 day")) .  '"')->order('goodsId DESC')->limit(4)->select();
        $shop['newSum'] = M('goods')->where('shopId=' . $shopId . ' and goodsStatus = 1 and isSale = 1 and isDel = 0 and goodsFlag = 1 and goodsStock > 0 and createTime > "' . date("Y-m-d H:i:s",strtotime("-32 day")) .  '"')->count();
        $shop['saleSum'] = M('goods')->where('shopId=' . $shopId . ' and isSale = 1 and isDel = 0 and goodsFlag = 1 and goodsStatus = 1 and isSelling = 1 and goodsStock > 0')->count();
        $shop['soldSum'] = M('goods')->where('shopId=' . $shopId . ' and ((isSale = 1 and isDel = 0 and goodsFlag = 1 and goodsStatus = 1 and isSelling = 0) or goodsStock = 0)')->count();
        if($userId == 0) {
            $shop['isFollow'] = 0;
        } else {
            $shop['isFollow'] = $this->isFollow($userId, $shopId);
        }

        return $shop;
    }
}