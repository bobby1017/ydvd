<?php

namespace Wap\Model;

class ShopsModel extends BaseModel {

    /**
     * 添加个人入驻微店
     * @return int
     */
    public function addPerShop($userInfo) {
        $rt = array();
        $rt['code'] = 0;
        $rt['info'] = '数据添加失败!';


        $data = array();
        $data['userId'] = $userInfo['userId'];
        $data['shopTel'] = I('phone');
        $data['areaId1'] = I('provinceId');
        $data['areaId2'] = I('cityId');
        $data['areaId3'] = I('districtId');
        $data['shopAddress'] = I('address');
        $data['fullName'] = I('fullName');

        $m = M('shops');
        $isShop = $m->where(array('userId' => $userInfo['userId']))->getField('shopId');
        if ($isShop > 0) {
            $rt["code"] = 0;
            $rt['info'] = '您已经申请过微店!';
            return $rt;
        }
        if (!preg_match("#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#", $data['shopTel'])) {
            $rt["code"] = 0;
            $rt['info'] = '手机号码格式错误!';
            return $rt;
        }
        foreach ($data as $v) {
            if ($v == '') {
                $rt['code'] = 0;
                $rt['info'] = '申请信息不完整!';
                return $rt;
            }
        }

        $shopId = $m->add($data);
        if (false !== $shopId) {
            $data['shopId'] = $shopId;
            $data['shopType'] = 2;
            $data['address'] = I('address');
            $data['phone'] = I('phone');
            $data['cardNum'] = I('cardNum');
            $data['cardImg'] = I('cardImg');
            $data['userCardImg'] = I('userCardImg');

            unset($data['shopTel']);
            unset($data['userId']);
            unset($data['shopAddress']);
            $m = M('shops_auth');
            $authId = $m->add($data);
            if ($authId !== FALSE) {
                $m = M('users');
                $m->where(array('userId' => $userInfo['userId']))->save(array('userType' => 2));
                
                $rt['code'] = 1;
                $rt['info'] = '数据添加成功!';
            }

            return $rt;
        }

        return $rt;
    }

    /**
     * 添加商家入驻微店
     * @return int
     */
    public function addCompShop($userInfo) {
        $rt = array();
        $rt['code'] = 0;
        $rt['info'] = '数据添加失败!';


        $data = array();
        $data['userId'] = $userInfo['userId'];
        $data['shopTel'] = I('phone');
        $data['areaId1'] = I('provinceId');
        $data['areaId2'] = I('cityId');
        $data['areaId3'] = I('districtId');
        $data['shopAddress'] = I('address');
        $data['fullName'] = I('fullName');

        $m = M('shops');
        $isShop = $m->where(array('userId' => $userInfo['userId']))->getField('shopId');
        if ($isShop > 0) {
            $rt["code"] = 0;
            $rt['info'] = '您已经申请过微店!';
            return $rt;
        }
        if (!preg_match("#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#", $data['shopTel'])) {
            $rt["code"] = 0;
            $rt['info'] = '手机号码格式错误!';
            return $rt;
        }
        foreach ($data as $v) {
            if ($v == '') {
                $rt['code'] = 0;
                $rt['info'] = '申请信息不完整!';
                return $rt;
            }
        }

        $shopId = $m->add($data);
        if (false !== $shopId) {
            $data['shopId'] = $shopId;
            $data['shopType'] = 1;
            $data['address'] = I('address');
            $data['phone'] = I('phone');
            $data['company'] = I('company');
            $data['companyNum'] = I('companyNum');
            $data['companyImg'] = I('companyImg');
            $data['companyAddress'] = I('companyAddress');

            unset($data['shopTel']);
            unset($data['userId']);
            unset($data['shopAddress']);

            foreach ($data as $v) {
                if ($v == '') {
                    $rt['code'] = 0;
                    $rt['info'] = '申请信息不完整!';
                    return $rt;
                }
            }
            $m = M('shops_auth');
            $authId = $m->add($data);
            if ($authId !== FALSE) {
                $m = M('users');
                $m->where(array('userId' => $userInfo['userId']))->save(array('userType' => 1));
                
                $rt['code'] = 1;
                $rt['info'] = '数据添加成功!';
            }

            return $rt;
        }

        return $rt;
    }
    
    /**
     * 获取店铺基本信息
     */
    public function getShopConfig($userInfo) {
        $m = M('shops');
        $shop = $m->where('userId=' . $userInfo['userId'])->field('shopId,userId,shopName,shopImg,shopTel,shopAddress,templateId,depositId,depositMoney,listStyle,postage')->find();
        $shop['shopSN'] = 100000 + $shop['shopId'];
        $shop['deposit'] = M('shops_deposit')->where('id=' . $shop['depositId'])->field('level,money,title,description')->find();

        return $shop;
    }
    
    /**
     * 获取发布的商品
     */
    public function getGoodsList() {
        $m = M('goods');
        $shopId = I('shopId');
        $num = I('num') ? I('num') : 5;
        $page = I('page') ? I('page') : 1;
        $page = ($page - 1) * $num;
        $goodsList = $m->where('shopId=' . $shopId . ' and isSource = 1 and isDel = 0 and goodsFlag = 1')->field('goodsId,goodsName,goodsName,marketPrice, 	shopPrice,goodsSize,goodsWeight,goodsStatus,goodsThumbs')->order('goodsId DESC')->limit($page, $num)->select();
        
        return $goodsList;
    }
    
    /**
     * 获取转售的商品
     */
    public function getResalesList() {
        $m = M('goods');
        $shopId = I('shopId');
        $num = I('num') ? I('num') : 5;
        $page = I('page') ? I('page') : 1;
        $page = ($page - 1) * $num;
        $goodsList = $m->where('shopId=' . $shopId . ' and isSource = 0 and isDel = 0 and goodsFlag = 1')->field('goodsId,goodsName,goodsName,marketPrice, 	shopPrice,goodsSize,goodsWeight,goodsStatus,goodsThumbs')->order('goodsId DESC')->limit($page, $num)->select();
        
        return $goodsList;
    }

}
