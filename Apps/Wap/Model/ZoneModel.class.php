<?php

namespace Wap\Model;

class ZoneModel extends BaseModel {

    /**
     * 添加好友
     */
    public function addFriend($uid) {
        $m = M('user_friends');
        $fuid = I('fuid');

        if ($fuid > 0) {
            $rs = $m->where('uid=' . $uid . ' AND fuid=' . $fuid)->count();
            if ($rs > 0)
                return -1;
            $m->uid = $uid;
            $m->fuid = $fuid;
            $m->createtime = time();
            $rs = $m->add();

            return $rs;
        }
        return FALSE;
    }

    /**
     * 新的好友
     */
    public function newFriends($userId) {
        $sql = 'SELECT f.id,u.userName,ifnull(u.userPhoto,"") userPhoto,ifnull(s.shopName, "") shopName,ifnull(s.shopId, 0) shopId,ifnull((s.shopId + 100000), 0), ifnull(s.shopImg,"") shopImg '
                . ' FROM __PREFIX__user_friends as f '
                . ' LEFT JOIN __PREFIX__users as u ON f.uid = u.userId '
                . ' LEFT JOIN __PREFIX__shops as s ON f.uid = s.userId '
                . ' WHERE f.type = 0 AND f.fuid = ' . $userId;
        $data = $this->query($sql);

        return $data;
    }

    /**
     * 忽略好友
     */
    public function ignoreFriend() {
        $m = M('user_friends');
        $id = I('id');
        $rs = $m->where('id=' . $id)->delete();

        return $rs;
    }

    /**
     * 同意好友
     */
    public function agreeFriend() {
        $m = M('user_friends');
        $id = I('id');
        $friend = $m->where('id=' . $id)->find();
        $isfriend = $m->where('uid=' . $friend['fuid'] . ' AND fuid=' . $friend['uid'])->getField('id');
        if ($isfriend > 0) {
            $rs = $m->where('id=' . $isfriend)->save(array('type' => 2));
        } else {
            $rs = $m->add(array(
                'uid' => $friend['fuid'],
                'fuid' => $friend['uid'],
                'type' => 2,
                'createtime' => time(),
            ));
        }


        $rs = $m->where('id=' . $id)->save(array('type' => 2));

        return $rs;
    }
    
     /**
     * 删除好友
     */
    public function delFriend() {
        $m = M('user_friends');
        $id = I('id');
        $friend = $m->where('id=' . $id)->find();
        if(empty($friend))
            return FALSE;
        
        $rs = $m->where('uid=' . $friend['uid'] . ' AND fuid=' . $friend['fuid'])->delete();
        $rs = $m->where('uid=' . $friend['fuid'] . ' AND fuid=' . $friend['uid'])->delete();
        

        return $rs;
    }
    
    /**
     * 好友列表
     */
    public function friends($userId) {
        $sql = 'SELECT f.id,u.userName,ifnull(u.userPhoto,"") userPhoto,ifnull(s.shopName,"") shopName,ifnull(s.shopId, 0) shopId,ifnull((s.shopId + 100000), 0) shopSN, ifnull(s.shopImg,"") shopImg '
                . ' FROM __PREFIX__user_friends as f '
                . ' LEFT JOIN __PREFIX__users as u ON f.uid = u.userId '
                . ' LEFT JOIN __PREFIX__shops as s ON f.uid = s.userId '
                . ' WHERE f.type = 2 AND f.fuid = ' . $userId;
        $data = $this->query($sql);

        return $data;
    }

}
