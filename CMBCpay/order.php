<?php
// www.yushangcn.com/CMBCpay/order.php?act=makeform
class order {
	function __construct() {
        $this->sadkInitialize();
  	}

  	function lajpTest()
  	{
  		
		require_once("php_java.php");//引用LAJP提供的PHP脚本

		$name ="LAJP";  //定义一个名称

		try
		{
		  //调用Java的hello.HelloClass类中的hello方法
		  $ret = lajp_call("hello.HelloClass::hello", $name);
		  echo "{$ret}<br>";
		}
		catch(Exception $e)
		{
		  echo "Err:{$ret}<br>";
		  echo "$e";
		}

  	}
  	/*
  		订单信息要求按照顺序拼装，一共19，并且每项以“|”分隔：
			版本号|订单号|交易金额|币种|交易日期|交易时间|商户代码|商户名称|二级商户号|通知地址|跳转地址|银行卡卡号|交易详细内容|商户预留信息|支付通道|借贷标识|商品类型|商品名称|备注
			订单格式示例：
			1.0.0|66002201506261037231111111111111|10.01|156|20150626|103723|66002|商户名称|11313|http://tongzhi.com|http://tiaozhuan.com||交易详细内容|hh|0|1|01|豆子|备注

  	 */
    
	function makeform() {
  		$payUrl = "http://111.205.207.118:55003/epay/cmbcpay.do";
  
  		$memberCode = "09025";

  		if (false==($billNo=$_POST['billNo'])) {
  			die (json_encode(array("code"=>1,"info"=>false,"data"=>'billNo字段未定义')));
  		}

  		if (false==($txAmt=$_POST['txAmt'])) {
  			die (json_encode(array("code"=>1,"info"=>false,"data"=>'txAmt字段未定义')));
  		}

  		if (false==is_numeric($txAmt)) {
  			die (json_encode(array("code"=>1,"info"=>false,"data"=>'txAmt一定为数字类型')));
  		}

  		$post['version'] = '1.0.0'; //默认1.0.0
  		$post['billNo'] =  $memberCode .date("YmdHis"). $billNo; //订单号:最长支持32位，订单号必须以5位商户号开始，参考格式如下：商户号（5位）＋YYYYMMDDHHMMSS（14位）+商户自定义序号（13位）。订单号总长可小于32位，且不可重复。
  		$post['txAmt'] = $txAmt; //交易金额
  		$post['PayerCurr'] = '156'; //目前仅支持人民币，默认为156
  		$post['txDate'] = date("Ymd"); //交易日期
  		$post['txTime'] = date("His"); //交易时间
  		$post['corpID'] = $memberCode; //商户代码
  		$post['corpName'] = '玉都商城'; //商户名称,字母数字汉字不能
  		$post['subCorpID'] = ""; //二级商户号,只能字母数字.可以为空
  		$post['NotifyUrl'] = "http://www.yushangcn.com/CMBCpay/order.php?act=notify"; //支付成功后我行异步方式通知商户的地址
  		$post['JumpUrl'] = "http://www.yushangcn.com/CMBCpay/order.php?act=JumpUrl"; //支付成功后页面方式通知商户的地址
  		$post['Account'] = $_POST['Account']?$_POST['Account']:''; //输入卡号时为指定此卡进行支付
  		$post['TransInfo'] = $_POST['TransInfo']?$_POST['TransInfo']:"玩具水枪详细内容"; //交易详细内容,订单关联的商品信息，字母数字汉字不能有,可以为空
  		$post['Message'] = $_POST['Message']?$_POST['Message']:''; //商户自定义信息，原值返回
  		$post['Channel'] = "2"; //支付通道 
  		$post['LoanFlag'] = ""; //借贷标示，不输表示为都支持
  		$post['ProductType'] = "1"; //商品类型，不输表示为都支持,1=综合商城
  		$post['ProductName'] = $_POST['ProductName']?$_POST['ProductName']:"商品名称玩具水枪"; //商品名称，不输表示为都支持 ，字母数字汉字不能，可以为空
  		$post['Remark'] = $_POST['Remark']?$_POST['Remark']:''; //备注，字母数字汉字不能，可以为空

  		foreach ($post as $key => $value) {
  			$newpost[] =  $value ;
  		}
  		$str = implode("|",$newpost);
  		//echo $str; die;
  		$encodeStr =  $this->sadkSign($str);

  		//$_html = "<form id='payfrm' name='orderinfo' action='{$payUrl}' method='post' accept-charset='utf-8'>";
		//$_html .= "<input name=\"orderinfo\" type=\"hidden\" value=\"{$encodeStr}\" /><input type='button' value='提交'   style='height:5em;width:5em;font-size: 2em;' />";
		//$_html .= "</form>";
    //
		  die (json_encode(array("code"=>1,"info"=>true,
              "data"=>array(
                  "payUrl"=>$payUrl
                  ,"orderinfo"=>$encodeStr
               )
          )));

		error_log("\r\n时间 ".date("Y-m-d H:i:s")." html=".json_encode($_html),3,dirname(__FILE__)."/postfrom_".date("Ymd").".txt");	
		echo $_html;
  		 
  	}
  			

  	function makeform_del() {
  		$payUrl = "http://111.205.207.118:55003/epay/cmbcpay.do";
  		 
  		$memberCode = "09025";

  		$post['version'] = '1.0.0'; //默认1.0.0
  		$post['billNo'] = $memberCode.date("YmdHis").rand(100000,999999); //订单号:最长支持32位，订单号必须以5位商户号开始，参考格式如下：商户号（5位）＋YYYYMMDDHHMMSS（14位）+商户自定义序号（13位）。订单号总长可小于32位，且不可重复。
  		$post['txAmt'] = '0.01'; //交易金额
  		$post['PayerCurr'] = '156'; //目前仅支持人民币，默认为156
  		$post['txDate'] = date("Ymd"); //交易日期
  		$post['txTime'] = date("His"); //交易时间
  		$post['corpID'] = $memberCode; //商户代码
  		$post['corpName'] = '玉都商城'; //商户名称,字母数字汉字不能
  		$post['subCorpID'] = ""; //二级商户号,只能字母数字.可以为空
  		$post['NotifyUrl'] = "http://www.yushangcn.com/CMBCpay/order.php?act=notify"; //支付成功后我行异步方式通知商户的地址
  		$post['JumpUrl'] = "http://www.yushangcn.com/CMBCpay/order.php?act=JumpUrl"; //支付成功后页面方式通知商户的地址
  		$post['Account'] = ""; //输入卡号时为指定此卡进行支付
  		$post['TransInfo'] = "玩具水枪详细内容"; //交易详细内容,订单关联的商品信息，字母数字汉字不能有,可以为空
  		$post['Message'] = ""; //商户自定义信息，原值返回
  		$post['Channel'] = "2"; //支付通道 
  		$post['LoanFlag'] = ""; //借贷标示，不输表示为都支持
  		$post['ProductType'] = "1"; //商品类型，不输表示为都支持,1=综合商城
  		$post['ProductName'] = "玩具水枪"; //商品名称，不输表示为都支持 ，字母数字汉字不能，可以为空
  		$post['Remark'] = ""; //备注，字母数字汉字不能，可以为空

  		foreach ($post as $key => $value) {
  			$newpost[] =  $value ;
  		}
  		$str = implode("|",$newpost);
  		//echo $str; die;
  		$encodeStr =  $this->sadkSign($str);

  		$_html = "<form id='payfrm' name='orderinfo' action='{$payUrl}' method='post' accept-charset='utf-8'>";
		$_html .= "<input name=\"orderinfo\" type=\"hidden\" value=\"{$encodeStr}\" /><input type='button' value='提交' onclick='jsubmit();' style='height:5em;width:5em;font-size: 2em;' />";
		$_html .= "</form>";
		$_html .=  "<script>function jsubmit() { document.getElementById('payfrm').submit(); }</script>";
		error_log("\r\n时间 ".date("Y-m-d H:i:s")." html=".json_encode($_html),3,dirname(__FILE__)."/postfrom_".date("Ymd").".txt");	
		echo $_html;
  		 
  	}

  	function sadkInitialize() {
  		require_once("php_java.php");//引用LAJP提供的PHP脚本
		try
		{
			//初始化， 
		   $ret = lajp_call("cfca.sadk.cmbc.tools.php.PHPDecryptKit::Initialize", "config/demo.properties");
		    //   echo "{$ret}<br>";
		}
		catch(Exception $e)
		{
		  echo "Err:{$e}<br>";
		}

  	}

  	function sadkSign($str) {
  		$base64Plain = base64_encode($str);
 
  		require_once("php_java.php");//引用LAJP提供的PHP脚本
		//需要签名的数据，base64格式
 
		try
		{
		 	//对数据进行PKCS#7带原文签名
			$ret = lajp_call("cfca.sadk.cmbc.tools.php.PHPDecryptKit::SignAndEncryptMessage", $base64Plain);
				//echo "PKCS:{$ret}<br>";   //0x70010001	工具包未初始化
			return $ret;
		  
		}
		catch(Exception $e)
		{
		  echo "Err:{$e}<br>";
		}

  	}

  	function JumpUrl(){
  		//支付成功后页面方式通知商户的地址
  		error_log("\r\n时间 ".date("Y-m-d H:i:s").":已经支付成功，正在转到商户地址",3,dirname(__FILE__)."/JumpUrl_".date("Ymd").".txt");	
  	}

  	function notify() {
  		//支付成功后异步方式通知商户的地址
  		error_log("\r\n时间 ".date("Y-m-d H:i:s")." html=".json_encode($_REQUEST['payresult']),3,dirname(__FILE__)."/notify_".date("Ymd").".txt");	
  	}

}


$order = new order();
$order->{$_REQUEST['act']}();


/*

 
2.	从商户的网站上点击民生卡支付后报错“加密出错或非法订单信息”？
a）在提交加密后的订单信息时，从安全考虑，推荐商户使用POST方式，而不是直接把加密后的订单信息放在action中，如：
action="https://per.cmbc.com.cn/pweb/b2cprelogin.do? <%=data%>"这样不推荐，应该把加密后的订单信息做为隐藏域进行提交，即<input name="orderinfo" type="hidden" value="<%=data%>">；
b）FORM表单名字不正确，确保name="orderinfo"；
c）订单信息格式不正确，请按照该文档中“商户银行接口参数”的定义规则核实上送的订单信息格式或内容是否正确。
3.	在生产环境上出现“不存在帐户信息”的错误提示
这是由于分行业务人员没有将商户的状态置为“开通”，请与分行相关人员进行联系，要求其修改。
4.	商户通知（异步）说明
商户通知通过get请求，通知前已对密文做了urlencode处理，通知格式如：
http://127.0.0.1:8080/notify.do? payresult=密文


 */
?>