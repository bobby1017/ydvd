<?php 
ini_set('date.timezone','Asia/Shanghai');
//error_reporting(E_ERROR);
require_once "../lib/WxPay.Api.php";
require_once 'log.php';

//初始化日志
$logHandler= new CLogFileHandler("../logs/".date('Y-m-d').'.log');
$log = Log::Init($logHandler, 15);


$body = $_REQUEST['body'];
$total_fee = intval($_REQUEST['total_fee']);
$order_no = intval($_REQUEST['order_no']);
$order_no = "7735".time();
//echo "<pre>";
//var_dump($_REQUEST);
//die;
//②、统一下单
$input = new WxPayUnifiedOrder();
$input->SetBody("玉都商品描述");
//$input->SetBody($body);
$input->SetAppid(WxPayConfig::APPID);
$input->SetMch_id(WxPayConfig::MCHID);
$input->SetNonce_str(md5(time()));

$input->SetOut_trade_no(WxPayConfig::MCHID.$order_no);
//$input->SetTotal_fee("1");
$input->SetTotal_fee("1");

$input->SetSpbill_create_ip(get_client_ip());
$input->SetTime_start(date("YmdHis"));
$input->SetTime_expire(date("YmdHis", time() + 600));
$input->SetGoods_tag("test");
//$input->SetNotify_url("http://khapi.mooxun.com/Wxpay/example/notify.php");
$input->SetNotify_url("http://yushangcn.com/Wxpay/example/notify.php");

$input->SetTrade_type("APP");
$input->SetProduct_id(time());
$input->SetSign();
$order = WxPayApi::unifiedOrder($input);

$orderArr = object_to_array($order);

//重新生成签名
//参考：https://cnodejs.org/topic/550b8f223135610a365b03a0
//参考：http://www.jianshu.com/p/972ffa8a7a40
//获取到 prepay_id 后,将参数 appid、noncestr、package(注意:此处应置为 Sign=WXPay)、partnerid、prepayid、timestamp 签名后返回给 APP。

$order_data = $orderArr;
$order_data['appid'] = $orderArr['appid'];
$order_data['prepay_id'] = $orderArr['prepay_id'];
$order_data['nonce_str'] = $orderArr['nonce_str'];
 
Log::DEBUG("example\appPay.php 57 line:" . json_encode($order_data));

$str = 'appid='.$order_data['appid'].'&noncestr='.$order_data['nonce_str'].'&package=Sign=WXPay&partnerid='.WxPayConfig::MCHID.'&prepayid='.$order_data['prepay_id'].'&timestamp='.time();
//重新生成签名
$order_data['sign'] = strtoupper(md5($str.'&key='.WxPayConfig::KEY));
//将$order_data数据返回给APP端调用
echo json_encode($order_data);

 

/**
 * 获取客户端IP地址
 * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
 * @return mixed
 */
function get_client_ip($type = 0) {
    $type       =  $type ? 1 : 0;
    static $ip  =   NULL;
    if ($ip !== NULL) return $ip[$type];
    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $arr    =   explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
        $pos    =   array_search('unknown',$arr);
        if(false !== $pos) unset($arr[$pos]);
        $ip     =   trim($arr[0]);
    }elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ip     =   $_SERVER['HTTP_CLIENT_IP'];
    }elseif (isset($_SERVER['REMOTE_ADDR'])) {
        $ip     =   $_SERVER['REMOTE_ADDR'];
    }
    // IP地址合法验证
    $long = sprintf("%u",ip2long($ip));
    $ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
    return $ip[$type];
}

function object_to_array($obj){  
    if(is_array($obj)){  
        return $obj;  
    }  
    $_arr = is_object($obj)? get_object_vars($obj) :$obj;  
    foreach ($_arr as $key => $val){  
    $val=(is_array($val)) || is_object($val) ? object_to_array($val) :$val;  
    $arr[$key] = $val;  
    }  
  
    return $arr;  
       
}  

?>