<?php
/**
 * @Filename mots.php
 *
 * @Copyright       Copyright (c) 2015~2020 http://www.mocode.cn All rights reserved.
 * @License         Licensed (http://www.mocode.cn/licenses/)
 * @Author          moocde (mo@mocode.cn)
 */
ini_set('date.timezone', 'Asia/Shanghai');
error_reporting(E_ERROR);

require_once "../lib/WxPay.Api.php";
require_once '../lib/WxPay.Notify.php';
require_once '../lib/WxPay.OrderModel.php';
require_once 'log.php';

$od = new OrderPay();
$obj["out_trade_no"] = '1451839459';
$obj["total_fee"] = 0.01;
$rs = $od->logUserProfit(1271);
