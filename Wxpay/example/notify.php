<?php
ini_set('date.timezone', 'Asia/Shanghai');
error_reporting(E_ERROR);

require_once "../lib/WxPay.Api.php";
require_once '../lib/WxPay.Notify.php';
require_once '../lib/WxPay.OrderModel.php';
require_once 'log.php';

//初始化日志
$logHandler = new CLogFileHandler("../logs/" . date('Y-m-d') . '.log');
$log = Log::Init($logHandler, 15);

class PayNotifyCallBack extends WxPayNotify {

    //查询订单
    public function Queryorder($transaction_id) {
        $input = new WxPayOrderQuery();
        $input->SetTransaction_id($transaction_id);
        $result = WxPayApi::orderQuery($input);
        Log::DEBUG("query:" . json_encode($result));
//        log_txt($result, '../logs/query-' . date('Y-m-d-H-i-s'));
        if (array_key_exists("return_code", $result) && array_key_exists("result_code", $result) && $result["return_code"] == "SUCCESS" && $result["result_code"] == "SUCCESS") {
            return true;
        }
        return false;
    }

    //重写回调处理函数
    public function NotifyProcess($data, &$msg) {

        Log::DEBUG("notify.php 32 line:" . json_encode($data));
//        log_txt($data, '../logs/call-back-' . date('Y-m-d-H-i-s'));
        $notfiyOutput = array();

        if (!array_key_exists("transaction_id", $data)) {
            $msg = "输入参数不正确";
            return false;
        }
        //查询订单，判断订单真实性
        if (!$this->Queryorder($data["transaction_id"])) {
            $msg = "订单查询失败";
            return false;
        }

//        支付成功业务逻辑
        $obj['out_trade_no'] = $data['out_trade_no'];
        $obj['total_fee'] = $data['total_fee'] / 100;

        $length = strlen($obj["out_trade_no"]);
        $od = new OrderPay();
        if ($length == 8) {
            $rs = $od->cashDepositPay($obj);
        } else {
            $rs = $od->complatePay($obj);
//            log_txt($data, '../logs/order-' . $out_trade_no . date('Y-m-d-H-i-s'));
        }

        return true;
    }

}

//log_txt('begin notify', '../logs/init-' . date('Y-m-d-H:i:s'));
Log::DEBUG("begin notify");
$notify = new PayNotifyCallBack();
$notify->Handle(false);

/**
 * 记录日志
 * @param type $data
 * @param type $filename
 */
function log_txt($data, $filename) {
    file_put_contents($filename, print_r($data, TRUE));
}
